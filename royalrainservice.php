    <!DOCTYPE html>
    <html lang="en">

    <head>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>สถิติการขอรับบริการฝนหลวง</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
            integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous">
        </script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-3KLMGLD350"></script>
        <script>
            window.dataLayer = window.dataLayer || [];

            function gtag() {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());
            gtag('config', 'G-3KLMGLD350');
        </script>
        <style>
            .table td,
            .table th {
                padding: 5px .75rem;
            }

            .chart_pie {
                font-size: 12px;
                position: absolute;
                margin-top: 100px;
                margin-left: 360px;
            }

            .layout_center {
                display: flex;
                justify-content: center;
                align-items: center;
                font-size: 1.5rem;
            }

            .upper_north_area {
                background-color: #ff0d0d99;
                border-radius: 3px;
                margin-right: 8px;
                padding: 0 8px;
            }

            .lower_north_area {
                background-color: #f59723;
                border-radius: 3px;
                margin-right: 8px;
                padding: 0 8px;
            }

            .central_area {
                background-color: #f3ed2b99;
                border-radius: 3px;
                margin-right: 8px;
                padding: 0 8px;
            }

            .upper_northeast_area {
                background-color: #00FF7F;
                border-radius: 3px;
                margin-right: 8px;
                padding: 0 8px;
            }

            .lower_northeast_area {
                background-color: #41e1d2;
                border-radius: 3px;
                margin-right: 8px;
                padding: 0 8px;
            }

            .eastern_area {
                background-color: #6495EDB9;
                border-radius: 3px;
                margin-right: 8px;
                padding: 0 8px;
            }

            .southern_area {
                background-color: #8A2BE299;
                border-radius: 3px;
                margin-right: 8px;
                padding: 0 8px;
            }

            .invisible {
                visibility: hidden;
            }

            .font_update_date {
                font-size: 0.8rem;
                padding-top: 20px;
                margin-right: 3.8rem;
            }

            .number_all_reques {
                background-color: #fff0;
                border-radius: 3px;
                margin-right: 8px;
                padding: 0 8px;
            }

            .number_reques_sueecss {
                background-color: #4cd137;
                border-radius: 3px;
                margin-right: 8px;
                padding: 0 8px;
            }

            .number_reques_progress {
                background-color: #fbc531;
                border-radius: 3px;
                margin-right: 8px;
                padding: 0 8px;
            }
        </style>
    </head>

    <body>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
        </script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous">
        </script>

        <?php 
        if (isset($_GET['year'])) {
            if($_GET['year'] < 2558 || $_GET['year'] > 2564){
                $year = 2564; 
            }else{
                $year = $_GET['year'];
            }
        }else{
            $year = 2564;
        }
        ?>
        <div class="container">
            <div class="card card-default">
                <div class="card-header">
                    <h2 class="text-center mt-4"> สถิติการขอรับบริการฝนหลวง <?php echo 'ปีงบประมาณ ' . $year;?></h2>
                </div>

                <div class="mt-4">
                    <div class="layout_center ">
                        <div class="">
                            <form action="#" method="GET" class="form-inline">
                                <label class="pr-3"> เลือกปีงบประมาณ</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <select id="year" name="year" class="custom-select">
                                            <option value="2558"
                                                <?php echo (isset($_GET['year']) ? ($_GET['year'] == '2558') ? 'selected' : '' :'')?>>
                                                2558
                                            </option>
                                            <option value="2559"
                                                <?php echo (isset($_GET['year']) ? ($_GET['year'] == '2559') ? 'selected' : '' :'')?>>
                                                2559
                                            </option>
                                            <option value="2560"
                                                <?php echo (isset($_GET['year']) ? ($_GET['year'] == '2560') ? 'selected' : '' :'')?>>
                                                2560
                                            </option>
                                            <option value="2561"
                                                <?php echo (isset($_GET['year']) ? ($_GET['year'] == '2561') ? 'selected' : '' :'')?>>
                                                2561
                                            </option>
                                            <option value="2562"
                                                <?php echo (isset($_GET['year']) ? ($_GET['year'] == '2562') ? 'selected' : '' :'')?>>
                                                2562
                                            </option>
                                            <option value="2563"
                                                <?php echo (isset($_GET['year']) ? ($_GET['year'] == '2563') ? 'selected' : '' :'')?>>
                                                2563
                                            </option>
                                            <option value="2564"
                                                <?php echo (isset($_GET['year']) ? ($_GET['year'] >= '2564' || $_GET['year'] < '2558') ? 'selected' : '' :'selected')?>>
                                                2564
                                            </option>
                                        </select>
                                    </div>
                                    <div class="invisible">
                                        <div class="input-group-append">
                                            <input type="submit" value="ตกลง" class="btn btn-primary" id="submit_year">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>


                </div>
                <!-- <pre> -->
                <?php
        
        $curlSession = curl_init();
        curl_setopt($curlSession, CURLOPT_URL, 'https://opendata.royalrain.go.th/request/all');
        curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);

        $jsonData = json_decode(curl_exec($curlSession));
        curl_close($curlSession);
        //  print_r($jsonData);

        if ($year == 2564) {
            $year_begin = 2020;
            $year_end = 2021;
        }elseif($year == 2563){
            $year_begin = 2019;
            $year_end = 2020;
        } elseif ($year == 2562) {
            $year_begin = 2018;
            $year_end = 2019;
        }
        elseif ($year == 2561) {
            $year_begin = 2017;
            $year_end = 2018;
        }
        elseif ($year == 2560) {
            $year_begin = 2016;
            $year_end = 2017;
        }
        elseif ($year == 2559) {
            $year_begin = 2015;
            $year_end = 2016;
        }
        elseif ($year == 2558) {
            $year_begin = 2014;
            $year_end = 2015;
        }else{
            $year_begin = 2020;
            $year_end = 2021;
        }

        $begin_day = date('Y-m-d', strtotime("10/1/".$year_begin));
        $end_day = date('Y-m-d', strtotime("09/30/".$year_end));

        $january_begin = date('Y-m-d', strtotime("01/01/".$year_end));
        $january_end = date('Y-m-d', strtotime("01/31/".$year_end));

        $february_begin = date('Y-m-d', strtotime("02/01/".$year_end));
        $february_end = date('Y-m-d', strtotime("02/29/".$year_end));

        $march_begin = date('Y-m-d', strtotime("03/01/".$year_end));
        $march_end = date('Y-m-d', strtotime("03/31/".$year_end));

        $april_begin = date('Y-m-d', strtotime("04/01/".$year_end));
        $april_end = date('Y-m-d', strtotime("04/30/".$year_end));

        $may_begin = date('Y-m-d', strtotime("05/01/".$year_end));
        $may_end = date('Y-m-d', strtotime("05/31/".$year_end));

        $june_begin = date('Y-m-d', strtotime("06/01/".$year_end));
        $june_end = date('Y-m-d', strtotime("06/30/".$year_end));

        $july_begin = date('Y-m-d', strtotime("07/01/".$year_end));
        $july_end = date('Y-m-d', strtotime("07/31/".$year_end));

        $august_begin = date('Y-m-d', strtotime("08/01/".$year_end));
        $august_end = date('Y-m-d', strtotime("08/31/".$year_end));

        $september_begin = date('Y-m-d', strtotime("09/01/".$year_end));
        $september_end = date('Y-m-d', strtotime("09/30/".$year_end));

        $october_begin = date('Y-m-d', strtotime("10/01/".$year_begin));
        $october_end = date('Y-m-d', strtotime("10/31/".$year_begin));

        $november_begin = date('Y-m-d', strtotime("11/01/".$year_begin));
        $november_end = date('Y-m-d', strtotime("11/30/".$year_begin));

        $december_begin = date('Y-m-d', strtotime("12/01/".$year_begin));
        $december_end = date('Y-m-d', strtotime("12/31/".$year_begin));

        $all_reques = 0;
        $reques_sueecss = 0;

        // Month
        $january = 0;
        $february = 0;
        $march = 0;
        $april = 0;
        $may = 0;
        $june = 0;
        $july = 0;
        $august = 0;
        $september = 0;
        $october = 0;
        $november = 0;
        $december = 0;

        // set array area
        $arrey_upper_north_area = array();
        $arrey_lower_north_area = array();
        $arrey_central_area = array();
        $arrey_upper_northeast_area = array();
        $arrey_lower_northeast_area = array();
        $arrey_eastern_area = array();
        $arrey_southern_area = array();

        foreach ($jsonData->data as $key => $value) {
            $day = date('Y-m-d', strtotime($value->request_date));
        if ($day >= $begin_day && $day <= $end_day) {
            //    echo '<br>';
            //    print_r($value);
                $all_reques++;
                if ($value->status_id == 6 ) {
                    $reques_sueecss++; 
                }
        
                if ($day >= $january_begin && $day <= $january_end) {
                    $january++;
                }

                if ($day >= $february_begin && $day <= $february_end) {
                    $february++;
                }

                if ($day >= $march_begin && $day <= $march_end) {
                    $march++;
                }

                if ($day >= $april_begin && $day <= $april_end) {
                    $april++;
                }

                if ($day >= $may_begin && $day <= $may_end) {
                    $may++;
                }

                if ($day >= $june_begin && $day <= $june_end) {
                    $june++;
                }

                if ($day >= $july_begin && $day <= $july_end) {
                    $july++;
                }

                if ($day >= $august_begin && $day <= $august_end) {
                    $august++;
                }

                if ($day >= $september_begin && $day <= $september_end) {
                    $september++;
                }

                if ($day >= $october_begin && $day <= $october_end) {
                    $october++;
                }

                if ($day >= $november_begin && $day <= $november_end) {
                    $november++;
                }

                if ($day >= $december_begin && $day <= $december_end) {
                    $december++;
                }

                if ($value->province_title == 'จ.เชียงราย' || $value->province_title == 'จ.เชียงใหม่' 
                || $value->province_title == 'จ.ตาก'|| $value->province_title == 'จ.แม่ฮ่องสอน'
                || $value->province_title == 'จ.พะเยา' || $value->province_title == 'จ.ลำปาง'
                || $value->province_title == 'จ.ลำพูน') {

                    $arrey_upper_north_area[] = $value; 
                }

                if ($value->province_title == 'จ.กำแพงเพชร' || $value->province_title == 'จ.พิจิตร' 
                || $value->province_title == 'จ.สุโขทัย'|| $value->province_title == 'จ.อุตรดิตถ์'
                || $value->province_title == 'จ.พิษณุโลก' || $value->province_title == 'จ.น่าน'
                || $value->province_title == 'จ.แพร่' || $value->province_title == 'จ.เพชรบูรณ์') {

                    $arrey_lower_north_area[] = $value;
                }

                if ($value->province_title == 'กรุงเทพมหานคร' || $value->province_title == 'จ.กาญจนบุรี' 
                || $value->province_title == 'จ.ชัยนาท'|| $value->province_title == 'จ.นครปฐม'
                || $value->province_title == 'จ.ลพบุรี' || $value->province_title == 'จ.นครสวรรค์'
                || $value->province_title == 'จ.สุพรรณบุรี' || $value->province_title == 'จ.นนทบุรี'
                || $value->province_title == 'จ.ปทุมธานี' || $value->province_title == 'จ.สระบุรี'
                || $value->province_title == 'จ.สิงห์บุรี' || $value->province_title == 'จ.พระนครศรีอยุธยา'
                || $value->province_title == 'จ.อ่างทอง' || $value->province_title == 'จ.อุทัยธานี') {

                    $arrey_central_area[] = $value;
                }

                if ($value->province_title == 'จ.กาฬสินธุ์' || $value->province_title == 'จ.ขอนแก่น' 
                || $value->province_title == 'จ.นครพนม'|| $value->province_title == 'จ.บึงกาฬ'
                || $value->province_title == 'จ.มุกดาหาร' || $value->province_title == 'จ.เลย'
                || $value->province_title == 'จ.สกลนคร' || $value->province_title == 'จ.หนองคาย'
                || $value->province_title == 'จ.หนองบัวลำภู' || $value->province_title == 'จ.อุดรธานี') {

                    $arrey_upper_northeast_area[] = $value;
                }

                if ($value->province_title == 'จ.ชัยภูมิ' || $value->province_title == 'จ.นครราชสีมา' 
                || $value->province_title == 'จ.บุรีรัมย์'|| $value->province_title == 'จ.มหาสารคาม'
                || $value->province_title == 'จ.ยโสธร' || $value->province_title == 'จ.ร้อยเอ็ด'
                || $value->province_title == 'จ.ศรีสะเกษ' || $value->province_title == 'จ.สุรินทร์'
                || $value->province_title == 'จ.อุบลราชธานี' || $value->province_title == 'จ.อำนาจเจริญ') {

                    $arrey_lower_northeast_area[] = $value;
                }

                if ($value->province_title == 'จ.จันทบุรี' || $value->province_title == 'จ.ฉะเชิงเทรา' 
                || $value->province_title == 'จ.ชลบุรี'|| $value->province_title == 'จ.ตราด'
                || $value->province_title == 'จ.นครนายก' || $value->province_title == 'จ.ปราจีนบุรี'
                || $value->province_title == 'จ.ระยอง' || $value->province_title == 'จ.สระแก้ว') {

                    $arrey_eastern_area[] = $value;
                }

                if ($value->province_title == 'จ.กระบี่' || $value->province_title == 'จ.ชุมพร' 
                || $value->province_title == 'จ.สุราษฎร์ธานี'|| $value->province_title == 'จ.นครศรีธรรมราช'
                || $value->province_title == 'จ.นราธิวาส'|| $value->province_title == 'จ.ประจวบคีรีขันธ์'
                || $value->province_title == 'จ.ปัตตานี'|| $value->province_title == 'จ.พังงา'
                || $value->province_title == 'จ.พัทลุง'|| $value->province_title == 'จ.สมุทรสงคราม'
                || $value->province_title == 'จ.เพชรบุรี'|| $value->province_title == 'จ.ภูเก็ต'
                || $value->province_title == 'จ.สมุทรสาคร'|| $value->province_title == 'จ.สมุทรปราการ'
                || $value->province_title == 'จ.ระนอง'|| $value->province_title == 'จ.สตูล'
                || $value->province_title == 'จ.ราชบุรี' || $value->province_title == 'จ.สงขลา'
                || $value->province_title == 'จ.ตรัง' || $value->province_title == 'จ.ยะลา') {

                    $arrey_southern_area[] = $value;
                }


            }
        }
        $reques_progress = $all_reques - $reques_sueecss;

        // set array
        $january_upper_north_area = array();
        $february_upper_north_area = array();
        $march_upper_north_area = array();
        $april_upper_north_area = array();
        $may_upper_north_area = array();
        $june_upper_north_area = array();
        $july_upper_north_area = array();
        $august_upper_north_area = array();
        $september_upper_north_area = array();
        $october_upper_north_area = array();
        $november_upper_north_area = array();
        $december_upper_north_area = array();

        $january_lower_north_area = array();
        $february_lower_north_area = array();
        $march_lower_north_area = array();
        $april_lower_north_area = array();
        $may_lower_north_area = array();
        $june_lower_north_area = array();
        $july_upper_north_area = array();
        $august_lower_north_area = array();
        $september_lower_north_area = array();
        $october_lower_north_area = array();
        $november_lower_north_area = array();
        $december_lower_north_area = array();

        $january_central_area = array();
        $february_central_area = array();
        $march_central_area = array();
        $april_central_area = array();
        $may_central_area = array();
        $june_central_area = array();
        $july_central_area = array();
        $august_central_area = array();
        $september_central_area = array();
        $october_central_area = array();
        $november_central_area = array();
        $december_central_area = array();

        $january_upper_northeast_area = array();
        $february_upper_northeast_area = array();
        $march_upper_northeast_area = array();
        $april_upper_northeast_area = array();
        $may_upper_northeast_area = array();
        $june_upper_northeast_area = array();
        $july_upper_northeast_area = array();
        $august_upper_northeast_area = array();
        $september_upper_northeast_area = array();
        $october_upper_northeast_area = array();
        $november_upper_northeast_area = array();
        $december_upper_northeast_area = array();

        $january_lower_northeast_area = array();
        $february_lower_northeast_area = array();
        $march_lower_northeast_area = array();
        $april_lower_northeast_area = array();
        $may_lower_northeast_area = array();
        $june_lower_northeast_area = array();
        $july_lower_northeast_area = array();
        $august_lower_northeast_area = array();
        $september_lower_northeast_area = array();
        $october_lower_northeast_area = array();
        $november_lower_northeast_area = array();
        $december_lower_northeast_area = array();

        $january_eastern_area = array();
        $february_eastern_area = array();
        $march_eastern_area = array();
        $april_eastern_area = array();
        $may_eastern_area = array();
        $june_eastern_area = array();
        $july_eastern_area = array();
        $august_eastern_area = array();
        $september_eastern_area = array();
        $october_eastern_area = array();
        $november_eastern_area = array();
        $december_eastern_area = array();

        $january_southern_area = array();
        $february_southern_area = array();
        $march_southern_area = array();
        $april_southern_area = array();
        $may_southern_area = array();
        $june_southern_area = array();
        $july_southern_area = array();
        $august_southern_area = array();
        $september_southern_area = array();
        $october_southern_area = array();
        $november_southern_area = array();
        $december_southern_area = array();

        
        // print_r($arrey_southern_area);
        // Area To month
        foreach ($arrey_upper_north_area as $key => $value) {
            $day = date('Y-m-d', strtotime($value->request_date));
            if ($day >= $january_begin && $day <= $january_end) {
                $january_upper_north_area[] = $value;
            }

            if ($day >= $february_begin && $day <= $february_end) {
                $february_upper_north_area[] = $value;
            }

            if ($day >= $march_begin && $day <= $march_end) {
                $march_upper_north_area[] = $value;
            }

            if ($day >= $april_begin && $day <= $april_end) {
                $april_upper_north_area[] = $value;
            }

            if ($day >= $may_begin && $day <= $may_end) {
                $may_upper_north_area[] = $value;
            }

            if ($day >= $june_begin && $day <= $june_end) {
                $june_upper_north_area[] = $value;
            }

            if ($day >= $july_begin && $day <= $july_end) {
                $july_upper_north_area[] = $value;
            }

            if ($day >= $august_begin && $day <= $august_end) {
                $august_upper_north_area[] = $value;
            }

            if ($day >= $september_begin && $day <= $september_end) {
                $september_upper_north_area[] = $value;
            }

            if ($day >= $october_begin && $day <= $october_end) {
                $october_upper_north_area[] = $value;
            }

            if ($day >= $november_begin && $day <= $november_end) {
                $november_upper_north_area[] = $value;
            }

            if ($day >= $december_begin && $day <= $december_end) {
                $december_upper_north_area[] = $value;
            }
        }

        foreach ($arrey_lower_north_area as $key => $value) {
            $day = date('Y-m-d', strtotime($value->request_date));
            if ($day >= $january_begin && $day <= $january_end) {
                $january_lower_north_area[] = $value;
            }

            if ($day >= $february_begin && $day <= $february_end) {
                $february_lower_north_area[] = $value;
            }

            if ($day >= $march_begin && $day <= $march_end) {
                $march_lower_north_area[] = $value;
            }

            if ($day >= $april_begin && $day <= $april_end) {
                $april_lower_north_area[] = $value;
            }

            if ($day >= $may_begin && $day <= $may_end) {
                $may_lower_north_area[] = $value;
            }

            if ($day >= $june_begin && $day <= $june_end) {
                $june_lower_north_area[] = $value;
            }

            if ($day >= $july_begin && $day <= $july_end) {
                $july_lower_north_area[] = $value;
            }

            if ($day >= $august_begin && $day <= $august_end) {
                $august_lower_north_area[] = $value;
            }

            if ($day >= $september_begin && $day <= $september_end) {
                $september_lower_north_area[] = $value;
            }

            if ($day >= $october_begin && $day <= $october_end) {
                $october_lower_north_area[] = $value;
            }

            if ($day >= $november_begin && $day <= $november_end) {
                $november_lower_north_area[] = $value;
            }

            if ($day >= $december_begin && $day <= $december_end) {
                $december_lower_north_area[] = $value;
            }
        }

        foreach ($arrey_central_area as $key => $value) {
            $day = date('Y-m-d', strtotime($value->request_date));
            if ($day >= $january_begin && $day <= $january_end) {
                $january_central_area[] = $value;
            }

            if ($day >= $february_begin && $day <= $february_end) {
                $february_central_area[] = $value;
            }

            if ($day >= $march_begin && $day <= $march_end) {
                $march_central_area[] = $value;
            }

            if ($day >= $april_begin && $day <= $april_end) {
                $april_central_area[] = $value;
            }

            if ($day >= $may_begin && $day <= $may_end) {
                $may_central_area[] = $value;
            }

            if ($day >= $june_begin && $day <= $june_end) {
                $june_central_area[] = $value;
            }

            if ($day >= $july_begin && $day <= $july_end) {
                $july_central_area[] = $value;
            }

            if ($day >= $august_begin && $day <= $august_end) {
                $august_central_area[] = $value;
            }

            if ($day >= $september_begin && $day <= $september_end) {
                $september_central_area[] = $value;
            }

            if ($day >= $october_begin && $day <= $october_end) {
                $october_central_area[] = $value;
            }

            if ($day >= $november_begin && $day <= $november_end) {
                $november_central_area[] = $value;
            }

            if ($day >= $december_begin && $day <= $december_end) {
                $december_central_area[] = $value;
            }
        }

        foreach ($arrey_upper_northeast_area as $key => $value) {
            $day = date('Y-m-d', strtotime($value->request_date));
            if ($day >= $january_begin && $day <= $january_end) {
                $january_upper_northeast_area[] = $value;
            }

            if ($day >= $february_begin && $day <= $february_end) {
                $february_upper_northeast_area[] = $value;
            }

            if ($day >= $march_begin && $day <= $march_end) {
                $march_upper_northeast_area[] = $value;
            }

            if ($day >= $april_begin && $day <= $april_end) {
                $april_upper_northeast_area[] = $value;
            }

            if ($day >= $may_begin && $day <= $may_end) {
                $may_upper_northeast_area[] = $value;
            }

            if ($day >= $june_begin && $day <= $june_end) {
                $june_upper_northeast_area[] = $value;
            }

            if ($day >= $july_begin && $day <= $july_end) {
                $july_upper_northeast_area[] = $value;
            }

            if ($day >= $august_begin && $day <= $august_end) {
                $august_upper_northeast_area[] = $value;
            }

            if ($day >= $september_begin && $day <= $september_end) {
                $september_upper_northeast_area[] = $value;
            }

            if ($day >= $october_begin && $day <= $october_end) {
                $october_upper_northeast_area[] = $value;
            }

            if ($day >= $november_begin && $day <= $november_end) {
                $november_upper_northeast_area[] = $value;
            }

            if ($day >= $december_begin && $day <= $december_end) {
                $december_upper_northeast_area[] = $value;
            }
        }

        foreach ($arrey_lower_northeast_area as $key => $value) {
            $day = date('Y-m-d', strtotime($value->request_date));
            if ($day >= $january_begin && $day <= $january_end) {
                $january_lower_northeast_area[] = $value;
            }

            if ($day >= $february_begin && $day <= $february_end) {
                $february_lower_northeast_area[] = $value;
            }

            if ($day >= $march_begin && $day <= $march_end) {
                $march_lower_northeast_area[] = $value;
            }

            if ($day >= $april_begin && $day <= $april_end) {
                $april_lower_northeast_area[] = $value;
            }

            if ($day >= $may_begin && $day <= $may_end) {
                $may_lower_northeast_area[] = $value;
            }

            if ($day >= $june_begin && $day <= $june_end) {
                $june_lower_northeast_area[] = $value;
            }

            if ($day >= $july_begin && $day <= $july_end) {
                $july_lower_northeast_area[] = $value;
            }

            if ($day >= $august_begin && $day <= $august_end) {
                $august_lower_northeast_area[] = $value;
            }

            if ($day >= $september_begin && $day <= $september_end) {
                $september_lower_northeast_area[] = $value;
            }

            if ($day >= $october_begin && $day <= $october_end) {
                $october_lower_northeast_area[] = $value;
            }

            if ($day >= $november_begin && $day <= $november_end) {
                $november_lower_northeast_area[] = $value;
            }

            if ($day >= $december_begin && $day <= $december_end) {
                $december_lower_northeast_area[] = $value;
            }
        }

        foreach ($arrey_eastern_area as $key => $value) {
            $day = date('Y-m-d', strtotime($value->request_date));
            if ($day >= $january_begin && $day <= $january_end) {
                $january_eastern_area[] = $value;
            }

            if ($day >= $february_begin && $day <= $february_end) {
                $february_eastern_area[] = $value;
            }

            if ($day >= $march_begin && $day <= $march_end) {
                $march_eastern_area[] = $value;
            }

            if ($day >= $april_begin && $day <= $april_end) {
                $april_eastern_area[] = $value;
            }

            if ($day >= $may_begin && $day <= $may_end) {
                $may_eastern_area[] = $value;
            }

            if ($day >= $june_begin && $day <= $june_end) {
                $june_eastern_area[] = $value;
            }

            if ($day >= $july_begin && $day <= $july_end) {
                $july_eastern_area[] = $value;
            }

            if ($day >= $august_begin && $day <= $august_end) {
                $august_eastern_area[] = $value;
            }

            if ($day >= $september_begin && $day <= $september_end) {
                $september_eastern_area[] = $value;
            }

            if ($day >= $october_begin && $day <= $october_end) {
                $october_eastern_area[] = $value;
            }

            if ($day >= $november_begin && $day <= $november_end) {
                $november_eastern_area[] = $value;
            }

            if ($day >= $december_begin && $day <= $december_end) {
                $december_eastern_area[] = $value;
            }
        }

        foreach ($arrey_southern_area as $key => $value) {
            $day = date('Y-m-d', strtotime($value->request_date));
            if ($day >= $january_begin && $day <= $january_end) {
                $january_southern_area[] = $value;
            }

            if ($day >= $february_begin && $day <= $february_end) {
                $february_southern_area[] = $value;
            }

            if ($day >= $march_begin && $day <= $march_end) {
                $march_southern_area[] = $value;
            }

            if ($day >= $april_begin && $day <= $april_end) {
                $april_southern_area[] = $value;
            }

            if ($day >= $may_begin && $day <= $may_end) {
                $may_southern_area[] = $value;
            }

            if ($day >= $june_begin && $day <= $june_end) {
                $june_southern_area[] = $value;
            }

            if ($day >= $july_begin && $day <= $july_end) {
                $july_southern_area[] = $value;
            }

            if ($day >= $august_begin && $day <= $august_end) {
                $august_southern_area[] = $value;
            }

            if ($day >= $september_begin && $day <= $september_end) {
                $september_southern_area[] = $value;
            }

            if ($day >= $october_begin && $day <= $october_end) {
                $october_southern_area[] = $value;
            }

            if ($day >= $november_begin && $day <= $november_end) {
                $november_southern_area[] = $value;
            }

            if ($day >= $december_begin && $day <= $december_end) {
                $december_southern_area[] = $value;
            }
        }
    ?>
                <?php echo "<p class='font_update_date layout_center'>ข้อมูลอัพเดตล่าสุดวันที่ ".DateThai($jsonData->update_date) ." </p>"; ?>
                <input type="hidden" value="<?php echo $january ?>" id="january">
                <input type="hidden" value="<?php echo $february ?>" id="february">
                <input type="hidden" value="<?php echo $march ?>" id="march">
                <input type="hidden" value="<?php echo $april ?>" id="april">
                <input type="hidden" value="<?php echo $may ?>" id="may">
                <input type="hidden" value="<?php echo $june ?>" id="june">
                <input type="hidden" value="<?php echo $july ?>" id="july">
                <input type="hidden" value="<?php echo $august ?>" id="august">
                <input type="hidden" value="<?php echo $september ?>" id="september">
                <input type="hidden" value="<?php echo $october ?>" id="october">
                <input type="hidden" value="<?php echo $november ?>" id="november">
                <input type="hidden" value="<?php echo $december ?>" id="december">

                <input type="hidden" value="<?php echo count($january_upper_north_area) ?>"
                    id="january_upper_north_area">
                <input type="hidden" value="<?php echo count($february_upper_north_area) ?>"
                    id="february_upper_north_area">
                <input type="hidden" value="<?php echo count($march_upper_north_area) ?>" id="march_upper_north_area">
                <input type="hidden" value="<?php echo count($april_upper_north_area) ?>" id="april_upper_north_area">
                <input type="hidden" value="<?php echo count($may_upper_north_area) ?>" id="may_upper_north_area">
                <input type="hidden" value="<?php echo count($june_upper_north_area) ?>" id="june_upper_north_area">
                <input type="hidden" value="<?php echo count($july_upper_north_area) ?>" id="july_upper_north_area">
                <input type="hidden" value="<?php echo count($august_upper_north_area) ?>" id="august_upper_north_area">
                <input type="hidden" value="<?php echo count($september_upper_north_area) ?>"
                    id="september_upper_north_area">
                <input type="hidden" value="<?php echo count($october_upper_north_area) ?>"
                    id="october_upper_north_area">
                <input type="hidden" value="<?php echo count($november_upper_north_area) ?>"
                    id="november_upper_north_area">
                <input type="hidden" value="<?php echo count($december_upper_north_area) ?>"
                    id="december_upper_north_area">

                <input type="hidden" value="<?php echo count($january_lower_north_area) ?>"
                    id="january_lower_north_area">
                <input type="hidden" value="<?php echo count($february_lower_north_area) ?>"
                    id="february_lower_north_area">
                <input type="hidden" value="<?php echo count($march_lower_north_area) ?>" id="march_lower_north_area">
                <input type="hidden" value="<?php echo count($april_lower_north_area) ?>" id="april_lower_north_area">
                <input type="hidden" value="<?php echo count($may_lower_north_area) ?>" id="may_lower_north_area">
                <input type="hidden" value="<?php echo count($june_lower_north_area) ?>" id="june_lower_north_area">
                <input type="hidden" value="<?php echo count($july_lower_north_area) ?>" id="july_lower_north_area">
                <input type="hidden" value="<?php echo count($august_lower_north_area) ?>" id="august_lower_north_area">
                <input type="hidden" value="<?php echo count($september_lower_north_area) ?>"
                    id="september_lower_north_area">
                <input type="hidden" value="<?php echo count($october_lower_north_area) ?>"
                    id="october_lower_north_area">
                <input type="hidden" value="<?php echo count($november_lower_north_area) ?>"
                    id="november_lower_north_area">
                <input type="hidden" value="<?php echo count($december_lower_north_area) ?>"
                    id="december_lower_north_area">

                <input type="hidden" value="<?php echo count($january_central_area) ?>" id="january_central_area">
                <input type="hidden" value="<?php echo count($february_central_area) ?>" id="february_central_area">
                <input type="hidden" value="<?php echo count($march_central_area) ?>" id="march_central_area">
                <input type="hidden" value="<?php echo count($april_central_area) ?>" id="april_central_area">
                <input type="hidden" value="<?php echo count($may_central_area) ?>" id="may_central_area">
                <input type="hidden" value="<?php echo count($june_central_area) ?>" id="june_central_area">
                <input type="hidden" value="<?php echo count($july_central_area) ?>" id="july_central_area">
                <input type="hidden" value="<?php echo count($august_central_area) ?>" id="august_central_area">
                <input type="hidden" value="<?php echo count($september_central_area) ?>" id="september_central_area">
                <input type="hidden" value="<?php echo count($october_central_area) ?>" id="october_central_area">
                <input type="hidden" value="<?php echo count($november_central_area) ?>" id="november_central_area">
                <input type="hidden" value="<?php echo count($december_central_area) ?>" id="december_central_area">

                <input type="hidden" value="<?php echo count($january_upper_northeast_area) ?>"
                    id="january_upper_northeast_area">
                <input type="hidden" value="<?php echo count($february_upper_northeast_area) ?>"
                    id="february_upper_northeast_area">
                <input type="hidden" value="<?php echo count($march_upper_northeast_area) ?>"
                    id="march_upper_northeast_area">
                <input type="hidden" value="<?php echo count($april_upper_northeast_area) ?>"
                    id="april_upper_northeast_area">
                <input type="hidden" value="<?php echo count($may_upper_northeast_area) ?>"
                    id="may_upper_northeast_area">
                <input type="hidden" value="<?php echo count($june_upper_northeast_area) ?>"
                    id="june_upper_northeast_area">
                <input type="hidden" value="<?php echo count($july_upper_northeast_area) ?>"
                    id="july_upper_northeast_area">
                <input type="hidden" value="<?php echo count($august_upper_northeast_area) ?>"
                    id="august_upper_northeast_area">
                <input type="hidden" value="<?php echo count($september_upper_northeast_area) ?>"
                    id="september_upper_northeast_area">
                <input type="hidden" value="<?php echo count($october_upper_northeast_area) ?>"
                    id="october_upper_northeast_area">
                <input type="hidden" value="<?php echo count($november_upper_northeast_area) ?>"
                    id="november_upper_northeast_area">
                <input type="hidden" value="<?php echo count($december_upper_northeast_area) ?>"
                    id="december_upper_northeast_area">

                <input type="hidden" value="<?php echo count($january_lower_northeast_area) ?>"
                    id="january_lower_northeast_area">
                <input type="hidden" value="<?php echo count($february_lower_northeast_area) ?>"
                    id="february_lower_northeast_area">
                <input type="hidden" value="<?php echo count($march_lower_northeast_area) ?>"
                    id="march_lower_northeast_area">
                <input type="hidden" value="<?php echo count($april_lower_northeast_area) ?>"
                    id="april_lower_northeast_area">
                <input type="hidden" value="<?php echo count($may_lower_northeast_area) ?>"
                    id="may_lower_northeast_area">
                <input type="hidden" value="<?php echo count($june_lower_northeast_area) ?>"
                    id="june_lower_northeast_area">
                <input type="hidden" value="<?php echo count($july_lower_northeast_area) ?>"
                    id="july_lower_northeast_area">
                <input type="hidden" value="<?php echo count($august_lower_northeast_area) ?>"
                    id="august_lower_northeast_area">
                <input type="hidden" value="<?php echo count($september_lower_northeast_area) ?>"
                    id="september_lower_northeast_area">
                <input type="hidden" value="<?php echo count($october_lower_northeast_area) ?>"
                    id="october_lower_northeast_area">
                <input type="hidden" value="<?php echo count($november_lower_northeast_area) ?>"
                    id="november_lower_northeast_area">
                <input type="hidden" value="<?php echo count($december_lower_northeast_area) ?>"
                    id="december_lower_northeast_area">

                <input type="hidden" value="<?php echo count($january_eastern_area) ?>" id="january_eastern_area">
                <input type="hidden" value="<?php echo count($february_eastern_area) ?>" id="february_eastern_area">
                <input type="hidden" value="<?php echo count($march_eastern_area) ?>" id="march_eastern_area">
                <input type="hidden" value="<?php echo count($april_eastern_area) ?>" id="april_eastern_area">
                <input type="hidden" value="<?php echo count($may_eastern_area) ?>" id="may_eastern_area">
                <input type="hidden" value="<?php echo count($june_eastern_area) ?>" id="june_eastern_area">
                <input type="hidden" value="<?php echo count($july_eastern_area) ?>" id="july_eastern_area">
                <input type="hidden" value="<?php echo count($august_eastern_area) ?>" id="august_eastern_area">
                <input type="hidden" value="<?php echo count($september_eastern_area) ?>" id="september_eastern_area">
                <input type="hidden" value="<?php echo count($october_eastern_area) ?>" id="october_eastern_area">
                <input type="hidden" value="<?php echo count($november_eastern_area) ?>" id="november_eastern_area">
                <input type="hidden" value="<?php echo count($december_eastern_area) ?>" id="december_eastern_area">

                <input type="hidden" value="<?php echo count($january_southern_area) ?>" id="january_southern_area">
                <input type="hidden" value="<?php echo count($february_southern_area) ?>" id="february_southern_area">
                <input type="hidden" value="<?php echo count($march_southern_area) ?>" id="march_southern_area">
                <input type="hidden" value="<?php echo count($april_southern_area) ?>" id="april_southern_area">
                <input type="hidden" value="<?php echo count($may_southern_area) ?>" id="may_southern_area">
                <input type="hidden" value="<?php echo count($june_southern_area) ?>" id="june_southern_area">
                <input type="hidden" value="<?php echo count($july_southern_area) ?>" id="july_southern_area">
                <input type="hidden" value="<?php echo count($august_southern_area) ?>" id="august_southern_area">
                <input type="hidden" value="<?php echo count($september_southern_area) ?>" id="september_southern_area">
                <input type="hidden" value="<?php echo count($october_southern_area) ?>" id="october_southern_area">
                <input type="hidden" value="<?php echo count($november_southern_area) ?>" id="november_southern_area">
                <input type="hidden" value="<?php echo count($december_southern_area) ?>" id="december_southern_area">

                <div class="card-body">
                    <?php 
                    function DateThai($strDate)
                    {
                        $strYear = date("Y",strtotime($strDate))+543;
                        $strMonth= date("n",strtotime($strDate));
                        $strDay= date("j",strtotime($strDate));
                        $strHour= date("H",strtotime($strDate));
                        $strMinute= date("i",strtotime($strDate));
                        $strSeconds= date("s",strtotime($strDate));
                        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
                        $strMonthThai=$strMonthCut[$strMonth];
                        return "$strDay $strMonthThai $strYear  เวลา  $strHour:$strMinute น.";
                    }

                    function DateThaiNoTime($strDate)
                    {
                        $strYear = date("Y",strtotime($strDate))+543;
                        $strMonth= date("n",strtotime($strDate));
                        $strDay= date("j",strtotime($strDate));
                        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
                        $strMonthThai=$strMonthCut[$strMonth];
                        return "$strDay $strMonthThai $strYear ";
                    }

                    // จังหวัดเหนื่อ บน
                    $province_chiangrin = array();
                    $province_chiangmai = array();
                    $province_tak = array();
                    $province_maehongson = array();
                    $province_phayao = array();
                    $province_lampang = array();
                    $province_lamphun = array();

                    $count_chiangrin = 0;
                    $count_chiangmai = 0;
                    $count_tak = 0;
                    $count_maehongson = 0;
                    $count_phayao = 0;
                    $count_lampang = 0;
                    $count_lamphun = 0;

                    foreach ($arrey_upper_north_area as $key => $value) {
                        if ($value->province_title == 'จ.เชียงราย') {
                            $province_chiangrin[] = $value;
                            $count_chiangrin++;
                        }
                        if ($value->province_title == 'จ.เชียงใหม่') {
                            $province_chiangmai[] = $value;
                            $count_chiangmai++;
                        }
                        if ($value->province_title == 'จ.ตาก') {
                            $province_tak[] = $value;
                            $count_tak++;
                        }
                        if ($value->province_title == 'จ.แม่ฮ่องสอน') {
                            $province_maehongson[] = $value;
                            $count_maehongson++;
                        }
                        if ($value->province_title == 'จ.พะเยา') {
                            $province_phayao[] = $value;
                            $count_phayao++;
                        }
                        if ($value->province_title == 'จ.ลำปาง') {
                            $province_lampang[] = $value;
                            $count_lampang++;
                        }
                        if ($value->province_title == 'จ.ลำพูน') {
                            $province_lamphun[] = $value;
                            $count_lamphun++;
                        }
                    }
                    // จังหวัดเหนื่อ ล่าง
                
                    $province_kamphaengphet = array();
                    $province_phichit = array();
                    $province_sukhothai = array();
                    $province_uttaradit = array();
                    $province_phitsanulok = array();
                    $province_nan = array();
                    $province_phrae = array();
                    $province_phetchabun = array();

                    $count_kamphaengphet = 0;
                    $count_phichit = 0;
                    $count_sukhothai = 0;
                    $count_uttaradit = 0;
                    $count_phitsanulok = 0;
                    $count_nan = 0;
                    $count_phrae = 0;
                    $count_phetchabun = 0;

                    foreach ($arrey_lower_north_area as $key => $value) {
                        if ($value->province_title == 'จ.กำแพงเพชร') {
                            $province_kamphaengphet[] = $value;
                            $count_kamphaengphet++;
                        }
                        if ($value->province_title == 'จ.พิจิตร') {
                            $province_phichit[] = $value;
                            $count_phichit++;
                        }
                        if ($value->province_title == 'จ.สุโขทัย') {
                            $province_sukhothai[] = $value;
                            $count_sukhothai++;
                        }
                        if ($value->province_title == 'จ.อุตรดิตถ์') {
                            $province_uttaradit[] = $value;
                            $count_uttaradit++;
                        }
                        if ($value->province_title == 'จ.พิษณุโลก') {
                            $province_phitsanulok[] = $value;
                            $count_phitsanulok++;
                        }
                        if ($value->province_title == 'จ.น่าน') {
                            $province_nan[] = $value;
                            $count_nan++;
                        }
                        if ($value->province_title == 'จ.แพร่') {
                            $province_phrae[] = $value;
                            $count_phrae++;
                        }
                        if ($value->province_title == 'จ.เพชรบูรณ์') {
                            $province_phetchabun[] = $value;
                            $count_phetchabun++;
                        }
                    }

                    // จังหวัด กลาง
                    $province_bangkok = array();
                    $province_kanchanaburi = array();
                    $province_chainati = array();
                    $province_nakhonpathom = array();
                    $province_lopburi = array();
                    $province_nakhonsawan = array();
                    $province_suphanburi = array();
                    $province_nonthaburi = array();
                    $province_pathumthani = array();
                    $province_saraburi = array();
                    $province_singburi = array();
                    $province_ayutthaya = array();
                    $province_angthong = array();
                    $province_uthaithani = array();

                    $count_bangkok = 0;
                    $count_kanchanaburi = 0;
                    $count_chainati = 0;
                    $count_nakhonpathom = 0;
                    $count_lopburi = 0;
                    $count_nakhonsawan = 0;
                    $count_suphanburi = 0;
                    $count_nonthaburi = 0;
                    $count_pathumthani = 0;
                    $count_saraburi = 0;
                    $count_singburi = 0;
                    $count_ayutthaya = 0;
                    $count_angthong = 0;
                    $count_uthaithani = 0;

                    foreach ($arrey_central_area as $key => $value) {
                        if ($value->province_title == 'กรุงเทพมหานคร') {
                            $province_bangkok[] = $value;
                            $count_bangkok++;
                        }
                        if ($value->province_title == 'จ.กาญจนบุรี') {
                            $province_kanchanaburi[] = $value;
                            $count_kanchanaburi++;
                        }
                        if ($value->province_title == 'จ.ชัยนาท') {
                            $province_chainati[] = $value;
                            $count_chainati++;
                        }
                        if ($value->province_title == 'จ.นครปฐม') {
                            $province_nakhonpathom[] = $value;
                            $count_nakhonpathom++;
                        }
                        if ($value->province_title == 'จ.ลพบุรี') {
                            $province_lopburi[] = $value;
                            $count_lopburi++;
                        }
                        if ($value->province_title == 'จ.นครสวรรค์') {
                            $province_nakhonsawan[] = $value;
                            $count_nakhonsawan++;
                        }
                        if ($value->province_title == 'จ.สุพรรณบุรี') {
                            $province_suphanburi[] = $value;
                            $count_suphanburi++;
                        }
                        if ($value->province_title == 'จ.นนทบุรี') {
                            $province_nonthaburi[] = $value;
                            $count_nonthaburi++;
                        }
                        if ($value->province_title == 'จ.ปทุมธานี') {
                            $province_pathumthani[] = $value;
                            $count_pathumthani++;
                        }
                        if ($value->province_title == 'จ.สระบุรี') {
                            $province_saraburi[] = $value;
                            $count_saraburi++;
                        }
                        if ($value->province_title == 'จ.สิงห์บุรี') {
                            $province_singburi[] = $value;
                            $count_singburi++;
                        }
                        if ($value->province_title == 'จ.พระนครศรีอยุธยา') {
                            $province_ayutthaya[] = $value;
                            $count_ayutthaya++;
                        }
                        if ($value->province_title == 'จ.อ่างทอง') {
                            $province_angthong[] = $value;
                            $count_angthong++;
                        }
                        if ($value->province_title == 'จ.อุทัยธานี') {
                            $province_uthaithani[] = $value;
                            $count_uthaithani++;
                        }
                    }

                    $province_kalasin = array();
                    $province_khonkaen = array();
                    $province_nakhonphanom = array();
                    $province_buengkan = array();
                    $province_mukdahan = array();
                    $province_loei = array();
                    $province_sakonnakhon = array();
                    $province_nongkhai = array();
                    $province_nongbualamphu = array();
                    $province_udonthani = array();

                    $count_kalasin = 0;
                    $count_khonkaen = 0;
                    $count_nakhonphanom = 0;
                    $count_buengkan = 0;
                    $count_mukdahan = 0;
                    $count_loei = 0;
                    $count_sakonnakhon = 0;
                    $count_nongkhai = 0;
                    $count_nongbualamphu = 0;
                    $count_udonthani = 0;

                    foreach ($arrey_upper_northeast_area as $key => $value) {
                        if ($value->province_title == 'จ.กาฬสินธุ์') {
                            $province_kalasin[] = $value;
                            $count_kalasin++;
                        }
                        if ($value->province_title == 'จ.ขอนแก่น') {
                            $province_khonkaen[] = $value;
                            $count_khonkaen++;
                        }
                        if ($value->province_title == 'จ.นครพนม') {
                            $province_nakhonphanom[] = $value;
                            $count_nakhonphanom++;
                        }
                        if ($value->province_title == 'จ.บึงกาฬ') {
                            $province_buengkan[] = $value;
                            $count_buengkan++;
                        }
                        if ($value->province_title == 'จ.มุกดาหาร') {
                            $province_mukdahan[] = $value;
                            $count_mukdahan++;
                        }
                        if ($value->province_title == 'จ.เลย') {
                            $province_loei[] = $value;
                            $count_loei++;
                        }
                        if ($value->province_title == 'จ.สกลนคร') {
                            $province_sakonnakhon[] = $value;
                            $count_sakonnakhon++;
                        }
                        if ($value->province_title == 'จ.หนองคาย') {
                            $province_nongkhai[] = $value;
                            $count_nongkhai++;
                        }
                        if ($value->province_title == 'จ.หนองบัวลำภู') {
                            $province_nongbualamphu[] = $value;
                            $count_nongbualamphu++;
                        }
                        if ($value->province_title == 'จ.อุดรธานี') {
                            $province_udonthani[] = $value;
                            $count_udonthani++;
                        }

                    }


                    $province_chaiyaphum = array();
                    $province_nakhonratchasima = array();
                    $province_buriram = array();
                    $province_mahasarakham = array();
                    $province_yasothon = array();
                    $province_roiet = array();
                    $province_sisaket = array();
                    $province_surin = array();
                    $province_ubon = array();
                    $province_amnatcharoen = array();

                    $count_chaiyaphum = 0;
                    $count_nakhonratchasima = 0;
                    $count_buriram = 0;
                    $count_mahasarakham = 0;
                    $count_yasothon = 0;
                    $count_roiet = 0;
                    $count_sisaket = 0;
                    $count_surin = 0;
                    $count_ubon = 0;
                    $count_amnatcharoen = 0;
                    

                    foreach ($arrey_lower_northeast_area as $key => $value) {
                        if ($value->province_title == 'จ.ชัยภูมิ') {
                            $province_chaiyaphum[] = $value;
                            $count_chaiyaphum++;
                        }
                        if ($value->province_title == 'จ.นครราชสีมา') {
                            $province_nakhonratchasima[] = $value;
                            $count_nakhonratchasima++;
                        }
                        if ($value->province_title == 'จ.บุรีรัมย์') {
                            $province_buriram[] = $value;
                            $count_buriram++;
                        }
                        if ($value->province_title == 'จ.มหาสารคาม') {
                            $province_mahasarakham[] = $value;
                            $count_mahasarakham++;
                        }
                        if ($value->province_title == 'จ.ยโสธร') {
                            $province_yasothon[] = $value;
                            $count_yasothon++;
                        }
                        if ($value->province_title == 'จ.ร้อยเอ็ด') {
                            $province_roiet[] = $value;
                            $count_roiet++;
                        }
                        if ($value->province_title == 'จ.ศรีสะเกษ') {
                            $province_sisaket[] = $value;
                            $count_sisaket++;
                        }
                        if ($value->province_title == 'จ.สุรินทร์') {
                            $province_surin[] = $value;
                            $count_surin++;
                        }
                        if ($value->province_title == 'จ.อุบลราชธานี') {
                            $province_ubon[] = $value;
                            $count_ubon++;
                        }
                        if ($value->province_title == 'จ.อำนาจเจริญ') {
                            $province_amnatcharoen[] = $value;
                            $count_amnatcharoen++;
                        }
                    }

                    $province_chanthaburi = array();
                    $province_chachoengsao = array();
                    $province_chonburi = array();
                    $province_trat = array();
                    $province_nakhonnayok = array();
                    $province_prachinburi = array();
                    $province_rayong = array();
                    $province_sakaeo = array();

                    $count_chanthaburi = 0;
                    $count_chachoengsao = 0;
                    $count_chonburi = 0;
                    $count_trat = 0;
                    $count_nakhonnayok = 0;
                    $count_prachinburi = 0;
                    $count_rayong = 0;
                    $count_sakaeo = 0;
                    
                    foreach ($arrey_eastern_area as $key => $value) {
                        if ($value->province_title == 'จ.จันทบุรี') {
                            $province_chanthaburi[] = $value;
                            $count_chanthaburi++;
                        }
                        if ($value->province_title == 'จ.ฉะเชิงเทรา') {
                            $province_chachoengsao[] = $value;
                            $count_chachoengsao++;
                        }
                        if ($value->province_title == 'จ.ชลบุรี') {
                            $province_chonburi[] = $value;
                            $count_chonburi++;
                        }
                        if ($value->province_title == 'จ.ตราด') {
                            $province_trat[] = $value;
                            $count_trat++;
                        }
                        if ($value->province_title == 'จ.นครนายก') {
                            $province_nakhonnayok[] = $value;
                            $count_nakhonnayok++;
                        }
                        if ($value->province_title == 'จ.ปราจีนบุรี') {
                            $province_prachinburi[] = $value;
                            $count_prachinburi++;
                        }
                        if ($value->province_title == 'จ.ระยอง') {
                            $province_rayong[] = $value;
                            $count_rayong++;
                        }
                        if ($value->province_title == 'จ.สระแก้ว') {
                            $province_sakaeo[] = $value;
                            $count_sakaeo++;
                        }
                    }

                    $province_krabi = array();
                    $province_chumphon = array();
                    $province_Suratthani = array();
                    $province_nakhonsithammarat = array();
                    $province_narathiwat = array();
                    $province_prachuapkhirikhan = array();
                    $province_pattani = array();
                    $province_phangnga = array();
                    $province_phatthalung = array();
                    $province_samutsongkhram = array();
                    $province_phetchaburi = array();
                    $province_phuket = array();
                    $province_samutsakhon = array();
                    $province_samutprakan = array();
                    $province_ranong = array();
                    $province_satun = array();
                    $province_ratchaburi = array();
                    $province_songkhla = array();
                    $province_trang = array();
                    $province_yala = array();

                    $count_krabi = 0;
                    $count_chumphon = 0;
                    $count_suratthani = 0;
                    $count_nakhonsithammarat = 0;
                    $count_narathiwat = 0;
                    $count_prachuapkhirikhan = 0;
                    $count_pattani = 0;
                    $count_phangnga = 0;
                    $count_phatthalung = 0;
                    $count_samutsongkhram = 0;
                    $count_phetchaburi = 0;
                    $count_phuket = 0;
                    $count_samutsakhon = 0;
                    $count_samutprakan = 0;
                    $count_ranong = 0;
                    $count_satun = 0;
                    $count_ratchaburi = 0;
                    $count_songkhla = 0;
                    $count_trang = 0;
                    $count_yala = 0;

                    foreach ($arrey_southern_area as $key => $value) {
                        if ($value->province_title == 'จ.กระบี่') {
                            $province_krabi[] = $value;
                            $count_krabi++;
                        }
                        if ($value->province_title == 'จ.ชุมพร') {
                            $province_chumphon[] = $value;
                            $count_chumphon++;
                        }
                        if ($value->province_title == 'จ.สุราษฎร์ธานี') {
                            $province_suratthani[] = $value;
                            $count_suratthani++;
                        }
                        if ($value->province_title == 'จ.นครศรีธรรมราช') {
                            $province_nakhonsithammarat[] = $value;
                            $count_nakhonsithammarat++;
                        }
                        if ($value->province_title == 'จ.นราธิวาส') {
                            $province_narathiwat[] = $value;
                            $count_narathiwat++;
                        }
                        if ($value->province_title == 'จ.ประจวบคีรีขันธ์') {
                            $province_prachuapkhirikhan[] = $value;
                            $count_prachuapkhirikhan++;
                        }
                        if ($value->province_title == 'จ.ปัตตานี') {
                            $province_pattani[] = $value;
                            $count_pattani++;
                        }
                        if ($value->province_title == 'จ.พังงา') {
                            $province_phangnga[] = $value;
                            $count_phangnga++;
                        }
                        if ($value->province_title == 'จ.พัทลุง') {
                            $province_phatthalung[] = $value;
                            $count_phatthalung++;
                        }
                        if ($value->province_title == 'จ.สมุทรสงคราม') {
                            $province_samutsongkhram[] = $value;
                            $count_samutsongkhram++;
                        }
                        if ($value->province_title == 'จ.เพชรบุรี') {
                            $province_phetchaburi[] = $value;
                            $count_phetchaburi++;
                        }
                        if ($value->province_title == 'จ.ภูเก็ต') {
                            $province_phuket[] = $value;
                            $count_phuket++;
                        }
                        if ($value->province_title == 'จ.สมุทรสาคร') {
                            $province_samutsakhon[] = $value;
                            $count_samutsakhon++;
                        }
                        if ($value->province_title == 'จ.สมุทรปราการ') {
                            $province_samutprakan[] = $value;
                            $count_samutprakan++;
                        }
                        if ($value->province_title == 'จ.ระนอง') {
                            $province_ranong[] = $value;
                            $count_ranong++;
                        }
                        if ($value->province_title == 'จ.สตูล') {
                            $province_satun[] = $value;
                            $count_satun++;
                        }
                        if ($value->province_title == 'จ.ราชบุรี') {
                            $province_ratchaburi[] = $value;
                            $count_ratchaburi++;
                        }
                        if ($value->province_title == 'จ.สงขลา') {
                            $province_songkhla[] = $value;
                            $count_songkhla++;
                        }
                        if ($value->province_title == 'จ.ตรัง') {
                            $province_trang[] = $value;
                            $count_trang++;
                        }
                        if ($value->province_title == 'จ.ยะลา') {
                            $province_yala[] = $value;
                            $count_yala++;
                        }
                    }

                    // คัดลำดับ เหนือบน
                    $upper_north_area = array(
                        'จ.เชียงราย' => $count_chiangrin,
                        'จ.เชียงใหม่' => $count_chiangmai,
                        'จ.ตาก' => $count_tak,
                        'จ.แม่ฮ่องสอน' => $count_maehongson,
                        'จ.พะเยา' => $count_phayao,
                        'จ.ลำปาง' => $count_lampang,
                        'จ.ลำพูน' =>  $count_lamphun
                    );

                    $upper_north_area_key = $upper_north_area;
                    rsort($upper_north_area_key);
                    arsort($upper_north_area);
                    $xx = 0;
                    foreach ($upper_north_area as $upper_north_area_key_loop => $upper_north_area_value) {
                        if(!isset($province_upper_north_nomber_one)){
                            foreach ($arrey_upper_north_area as $key => $value) {
                                if ($value->province_title == $upper_north_area_key_loop) {
                                    $province_one[] = $value;
                                }
                            }
                            
                            foreach ($province_one as $key => $value) {
                                $amphur_title_one[] = $value->amphur_title;
                            }
                            $number_amphur_title_one = array_count_values($amphur_title_one);
                            $number_amphur_title_key_one = array_count_values($amphur_title_one);
                            rsort($number_amphur_title_key_one);
                            arsort($number_amphur_title_one);   
                        
                            
                            $xaa = 0;
                        foreach ($number_amphur_title_one as $key => $value) {
                            if (!isset($amphur_point_one)) {
                                $amphur_point_one = $number_amphur_title_key_one[$xaa];

                                $upper_north_area_number[$upper_north_area_key_loop] = array(
                                    'no' => 1,
                                    'point' => $upper_north_area_key[$xx],
                                    array(
                                        $key=> $amphur_point_one
                                        )
                                );
                            }else{
                                array_push($upper_north_area_number[$upper_north_area_key_loop], array($key=>$number_amphur_title_key_one[$xaa]));
                            }
                            $xaa++;
                            }
                            $province_upper_north_nomber_one = $upper_north_area_key[$xx];
                        
                    }elseif(isset($province_upper_north_nomber_one) && !isset($province_upper_north_nomber_two)){
                        if ($province_upper_north_nomber_one == $upper_north_area_value) {
                            unset($amphur_point_one);
                            unset($province_one);
                            unset($amphur_title_one);
                            foreach ($arrey_upper_north_area as $key => $value) {
                                if ($value->province_title == $upper_north_area_key_loop) {
                                    $province_one[] = $value;
                                }
                            }

                            foreach ($province_one as $key => $value) {
                                $amphur_title_one[] = $value->amphur_title;
                            }
                            
                            $number_amphur_title_one = array_count_values($amphur_title_one);
                            $number_amphur_title_key_one = array_count_values($amphur_title_one);
                            rsort($number_amphur_title_key_one);
                            arsort($number_amphur_title_one);   
                            $xaa = 0;
                            foreach ($number_amphur_title_one as $key => $value) {
                                if (!isset($amphur_point_one)) {
                                    $amphur_point_one = $number_amphur_title_key_one[$xaa];
                                    
                                    $upper_north_area_number[$upper_north_area_key_loop] = array(
                                        'no' => 1,
                                        'point' => $upper_north_area_key[$xx],
                                        array(
                                            $key=> $amphur_point_one
                                            )
                                        );
                                    }else{
                                        array_push($upper_north_area_number[$upper_north_area_key_loop], array($key=>$number_amphur_title_key_one[$xaa]));
                                    }
                                    $xaa++;
                                }
                            
                        }else{
                            foreach ($arrey_upper_north_area as $key => $value) {
                                if ($value->province_title == $upper_north_area_key_loop) {
                                    $province_two[] = $value;
                                }
                            }

                            foreach ($province_two as $key => $value) {
                                $amphur_title_two[] = $value->amphur_title;
                            }
                            
                            $number_amphur_title_two = array_count_values($amphur_title_two);
                            $number_amphur_title_key_two = array_count_values($amphur_title_two);
                            rsort($number_amphur_title_key_two);
                            arsort($number_amphur_title_two);   
                            $xaa = 0;
                            foreach ($number_amphur_title_two as $key => $value) {
                                if (!isset($amphur_point_two)) {
                                    $amphur_point_two = $number_amphur_title_key_two[$xaa];
                                    
                                    $upper_north_area_number[$upper_north_area_key_loop] = array(
                                        'no' => 2,
                                        'point' => $upper_north_area_key[$xx],
                                        array(
                                            $key=> $amphur_point_two
                                            )
                                        );
                                    }else{
                                        array_push($upper_north_area_number[$upper_north_area_key_loop], array($key=>$number_amphur_title_key_two[$xaa]));
                                    }
                                    $xaa++;
                                }
                                $province_upper_north_nomber_two = $upper_north_area_key[$xx];
                        }
                    }elseif(isset($province_upper_north_nomber_two) && !isset($province_upper_north_nomber_three)){
                        if ($province_upper_north_nomber_two == $upper_north_area_value) {
                            unset($amphur_point_two);
                            unset($province_two);
                            unset($amphur_title_two);
                            foreach ($arrey_upper_north_area as $key => $value) {
                                if ($value->province_title == $upper_north_area_key_loop) {
                                    $province_two[] = $value;
                                }
                            }

                            foreach ($province_two as $key => $value) {
                                $amphur_title_two[] = $value->amphur_title;
                            }
                            
                            $number_amphur_title_two = array_count_values($amphur_title_two);
                            $number_amphur_title_key_two = array_count_values($amphur_title_two);
                            rsort($number_amphur_title_key_two);
                            arsort($number_amphur_title_two);   
                            $xaa = 0;
                            foreach ($number_amphur_title_two as $key => $value) {
                                if (!isset($amphur_point_two)) {
                                    $amphur_point_two = $number_amphur_title_key_two[$xaa];
                                    
                                    $upper_north_area_number[$upper_north_area_key_loop] = array(
                                        'no' => 2,
                                        'point' => $upper_north_area_key[$xx],
                                        array(
                                            $key=> $amphur_point_two
                                            )
                                        );
                                    }else{
                                        array_push($upper_north_area_number[$upper_north_area_key_loop], array($key=>$number_amphur_title_key_two[$xaa]));
                                    }
                                    $xaa++;
                                }
                        }else{
                            foreach ($arrey_upper_north_area as $key => $value) {
                                if ($value->province_title == $upper_north_area_key_loop) {
                                    $province_three[] = $value;
                                }
                            }

                            foreach ($province_three as $key => $value) {
                                $amphur_title_three[] = $value->amphur_title;
                            }
                            
                            $number_amphur_title_three = array_count_values($amphur_title_three);
                            $number_amphur_title_key_three = array_count_values($amphur_title_three);
                            rsort($number_amphur_title_key_three);
                            arsort($number_amphur_title_three);   
                            $xaa = 0;
                            foreach ($number_amphur_title_three as $key => $value) {
                                if (!isset($amphur_point_three)) {
                                    $amphur_point_three = $number_amphur_title_key_three[$xaa];
                                    
                                    $upper_north_area_number[$upper_north_area_key_loop] = array(
                                        'no' => 3,
                                        'point' => $upper_north_area_key[$xx],
                                        array(
                                            $key=> $amphur_point_three
                                            )
                                        );
                                    }else{
                                        array_push($upper_north_area_number[$upper_north_area_key_loop], array($key=>$number_amphur_title_key_three[$xaa]));
                                    }
                                    $xaa++;
                                }
                                $province_upper_north_nomber_three = $upper_north_area_key[$xx];
                        }
                    }elseif(isset($province_upper_north_nomber_three) && !isset($province_upper_north_nomber_four)){
                        if ($province_upper_north_nomber_three == $upper_north_area_value) {
                            unset($amphur_point_three);
                            unset($province_three);
                            unset($amphur_title_three);
                            foreach ($arrey_upper_north_area as $key => $value) {
                                if ($value->province_title == $upper_north_area_key_loop) {
                                    $province_three[] = $value;
                                }
                            }

                            foreach ($province_three as $key => $value) {
                                $amphur_title_three[] = $value->amphur_title;
                            }
                            
                            $number_amphur_title_three = array_count_values($amphur_title_three);
                            $number_amphur_title_key_three = array_count_values($amphur_title_three);
                            rsort($number_amphur_title_key_three);
                            arsort($number_amphur_title_three);   
                            $xaa = 0;
                            foreach ($number_amphur_title_three as $key => $value) {
                                if (!isset($amphur_point_three)) {
                                    $amphur_point_three = $number_amphur_title_key_three[$xaa];
                                    
                                    $upper_north_area_number[$upper_north_area_key_loop] = array(
                                        'no' => 3,
                                        'point' => $upper_north_area_key[$xx],
                                        array(
                                            $key=> $amphur_point_three
                                            )
                                        );
                                    }else{
                                        array_push($upper_north_area_number[$upper_north_area_key_loop], array($key=>$number_amphur_title_key_three[$xaa]));
                                    }
                                    $xaa++;
                                }
                        }else {
                            foreach ($arrey_upper_north_area as $key => $value) {
                                if ($value->province_title == $upper_north_area_key_loop) {
                                    $province_four[] = $value;
                                }
                            }

                            foreach ($province_four as $key => $value) {
                                $amphur_title_four[] = $value->amphur_title;
                            }
                            
                            $number_amphur_title_four = array_count_values($amphur_title_four);
                            $number_amphur_title_key_four = array_count_values($amphur_title_four);
                            rsort($number_amphur_title_key_four);
                            arsort($number_amphur_title_four);
                            $xaa = 0;
                            foreach ($number_amphur_title_four as $key => $value) {
                                if (!isset($amphur_point_four)) {
                                    $amphur_point_four = $number_amphur_title_key_four[$xaa];
                                    
                                    $upper_north_area_number[$upper_north_area_key_loop] = array(
                                        'no' => 4,
                                        'point' => $upper_north_area_key[$xx],
                                        array(
                                            $key=> $amphur_point_four
                                            )
                                        );
                                    }else{
                                        array_push($upper_north_area_number[$upper_north_area_key_loop], array($key=>$number_amphur_title_key_four[$xaa]));
                                    }
                                    $xaa++;
                                }
                                $province_upper_north_nomber_four = $upper_north_area_key[$xx];
                        }
                    }elseif(isset($province_upper_north_nomber_four) && !isset($province_upper_north_nomber_five)){
                        if ($province_upper_north_nomber_four == $upper_north_area_value) {
                            unset($amphur_point_four);
                            unset($province_four);
                            unset($amphur_title_four);
                            foreach ($arrey_upper_north_area as $key => $value) {
                                if ($value->province_title == $upper_north_area_key_loop) {
                                    $province_four[] = $value;
                                }
                            }

                            foreach ($province_four as $key => $value) {
                                $amphur_title_four[] = $value->amphur_title;
                            }
                            
                            $number_amphur_title_four = array_count_values($amphur_title_four);
                            $number_amphur_title_key_four = array_count_values($amphur_title_four);
                            rsort($number_amphur_title_key_four);
                            arsort($number_amphur_title_four);   
                            $xaa = 0;
                            foreach ($number_amphur_title_four as $key => $value) {
                                if (!isset($amphur_point_four)) {
                                    $amphur_point_four = $number_amphur_title_key_four[$xaa];
                                    
                                    $upper_north_area_number[$upper_north_area_key_loop] = array(
                                        'no' => 4,
                                        'point' => $upper_north_area_key[$xx],
                                        array(
                                            $key=> $amphur_point_four
                                            )
                                        );
                                    }else{
                                        array_push($upper_north_area_number[$upper_north_area_key_loop], array($key=>$number_amphur_title_key_four[$xaa]));
                                    }
                                    $xaa++;
                                }
                        }else{
                            foreach ($arrey_upper_north_area as $key => $value) {
                                if ($value->province_title == $upper_north_area_key_loop) {
                                    $province_five[] = $value;
                                }
                            }
                            if (isset($province_five)) {
                                foreach ($province_five as $key => $value) {
                                    $amphur_title_five[] = $value->amphur_title;
                                }
                            
                            
                            $number_amphur_title_five = array_count_values($amphur_title_five);
                            $number_amphur_title_key_five = array_count_values($amphur_title_five);
                            rsort($number_amphur_title_key_five);
                            arsort($number_amphur_title_five);
                            $xaa = 0;
                            foreach ($number_amphur_title_five as $key => $value) {
                                if (!isset($amphur_point_five)) {
                                    $amphur_point_five = $number_amphur_title_key_five[$xaa];
                                    
                                    $upper_north_area_number[$upper_north_area_key_loop] = array(
                                        'no' => 5,
                                        'point' => $upper_north_area_key[$xx],
                                        array(
                                            $key=> $amphur_point_five
                                            )
                                        );
                                    }else{
                                        array_push($upper_north_area_number[$upper_north_area_key_loop], array($key=>$number_amphur_title_key_five[$xaa]));
                                    }
                                    $xaa++;
                                }
                            }
                                $province_upper_north_nomber_five = $upper_north_area_key[$xx];
                        }
                    }
                $xx++; 
            }
            unset($amphur_point_one);
            unset($province_one);
            unset($amphur_title_one);
            unset($amphur_point_two);
            unset($province_two);
            unset($amphur_title_two);
            unset($amphur_point_three);
            unset($province_three);
            unset($amphur_title_three);
            unset($amphur_point_four);
            unset($province_four);
            unset($amphur_title_four);
            unset($amphur_point_five);
            unset($province_five);
            unset($amphur_title_five);
                            
                    // จบคัดลำดับเหนือบน
    // คัดลำดับเหนือล่าง
            $lower_north_area = array(
                'จ.กำแพงเพชร' =>  $count_kamphaengphet,
                'จ.พิจิตร' =>$count_phichit,
                'จ.สุโขทัย' => $count_sukhothai,
                'จ.อุตรดิตถ์' =>$count_uttaradit,
                'จ.พิษณุโลก' =>$count_phitsanulok,
                'จ.น่าน' => $count_nan,
                'จ.แพร่' =>$count_phrae,
                'จ.เพชรบูรณ์' =>$count_phetchabun
            );

            $lower_north_area_key = $lower_north_area;
            rsort($lower_north_area_key);
            arsort($lower_north_area);
            $xx = 0;
            foreach ($lower_north_area as $lower_north_area_key_loop => $lower_north_area_value) {
                if(!isset($province_lower_north_nomber_one)){
                    foreach ($arrey_lower_north_area as $key => $value) {
                        if ($value->province_title == $lower_north_area_key_loop) {
                            $province_one[] = $value;
                        }
                    }
                    
                    foreach ($province_one as $key => $value) {
                        $amphur_title_one[] = $value->amphur_title;
                    }
                    $number_amphur_title_one = array_count_values($amphur_title_one);
                    $number_amphur_title_key_one = array_count_values($amphur_title_one);
                    rsort($number_amphur_title_key_one);
                    arsort($number_amphur_title_one);   
                
                    
                    $xaa = 0;
                foreach ($number_amphur_title_one as $key => $value) {
                    if (!isset($amphur_point_one)) {
                        $amphur_point_one = $number_amphur_title_key_one[$xaa];

                        $lower_north_area_number[$lower_north_area_key_loop] = array(
                            'no' => 1,
                            'point' => $lower_north_area_key[$xx],
                            array(
                                $key=> $amphur_point_one
                                )
                        );
                    }else{
                        array_push($lower_north_area_number[$lower_north_area_key_loop], array($key=>$number_amphur_title_key_one[$xaa]));
                    }
                    $xaa++;
                    }
                    $province_lower_north_nomber_one = $lower_north_area_key[$xx];
                
            }elseif(isset($province_lower_north_nomber_one) && !isset($province_lower_north_nomber_two)){
                if ($province_lower_north_nomber_one == $lower_north_area_value) {
                    unset($amphur_point_one);
                    unset($province_one);
                    unset($amphur_title_one);
                    foreach ($arrey_lower_north_area as $key => $value) {
                        if ($value->province_title == $lower_north_area_key_loop) {
                            $province_one[] = $value;
                        }
                    }

                    foreach ($province_one as $key => $value) {
                        $amphur_title_one[] = $value->amphur_title;
                    }
                    
                    $number_amphur_title_one = array_count_values($amphur_title_one);
                    $number_amphur_title_key_one = array_count_values($amphur_title_one);
                    rsort($number_amphur_title_key_one);
                    arsort($number_amphur_title_one);   
                    $xaa = 0;
                    foreach ($number_amphur_title_one as $key => $value) {
                        if (!isset($amphur_point_one)) {
                            $amphur_point_one = $number_amphur_title_key_one[$xaa];
                            
                            $lower_north_area_number[$lower_north_area_key_loop] = array(
                                'no' => 1,
                                'point' => $lower_north_area_key[$xx],
                                array(
                                    $key=> $amphur_point_one
                                    )
                                );
                            }else{
                                array_push($lower_north_area_number[$lower_north_area_key_loop], array($key=>$number_amphur_title_key_one[$xaa]));
                            }
                            $xaa++;
                        }
                    
                }else{
                    foreach ($arrey_lower_north_area as $key => $value) {
                        if ($value->province_title == $lower_north_area_key_loop) {
                            $province_two[] = $value;
                        }
                    }

                    if (isset($province_two)) {
                    foreach ($province_two as $key => $value) {
                        $amphur_title_two[] = $value->amphur_title;
                    }
                
                    
                    $number_amphur_title_two = array_count_values($amphur_title_two);
                    $number_amphur_title_key_two = array_count_values($amphur_title_two);
                    rsort($number_amphur_title_key_two);
                    arsort($number_amphur_title_two);   
                    $xaa = 0;
                    foreach ($number_amphur_title_two as $key => $value) {
                        if (!isset($amphur_point_two)) {
                            $amphur_point_two = $number_amphur_title_key_two[$xaa];
                            
                            $lower_north_area_number[$lower_north_area_key_loop] = array(
                                'no' => 2,
                                'point' => $lower_north_area_key[$xx],
                                array(
                                    $key=> $amphur_point_two
                                    )
                                );
                            }else{
                                array_push($lower_north_area_number[$lower_north_area_key_loop], array($key=>$number_amphur_title_key_two[$xaa]));
                            }
                            $xaa++;
                        }
                    }
                        $province_lower_north_nomber_two = $lower_north_area_key[$xx];
                }
            }elseif(isset($province_lower_north_nomber_two) && !isset($province_lower_north_nomber_three)){
                if ($province_lower_north_nomber_two == $lower_north_area_value) {
                    unset($amphur_point_two);
                    unset($province_two);
                    unset($amphur_title_two);
                    foreach ($arrey_lower_north_area as $key => $value) {
                        if ($value->province_title == $lower_north_area_key_loop) {
                            $province_two[] = $value;
                        }
                    }

                    if (isset($province_two)) {
                    foreach ($province_two as $key => $value) {
                        $amphur_title_two[] = $value->amphur_title;
                    }
                    
                    $number_amphur_title_two = array_count_values($amphur_title_two);
                    $number_amphur_title_key_two = array_count_values($amphur_title_two);
                    rsort($number_amphur_title_key_two);
                    arsort($number_amphur_title_two);   
                    $xaa = 0;
                    foreach ($number_amphur_title_two as $key => $value) {
                        if (!isset($amphur_point_two)) {
                            $amphur_point_two = $number_amphur_title_key_two[$xaa];
                            
                            $lower_north_area_number[$lower_north_area_key_loop] = array(
                                'no' => 3,
                                'point' => $lower_north_area_key[$xx],
                                array(
                                    $key=> $amphur_point_two
                                    )
                                );
                            }else{
                                array_push($lower_north_area_number[$lower_north_area_key_loop], array($key=>$number_amphur_title_key_two[$xaa]));
                            }
                            $xaa++;
                        }
                    }
                }else{
                    foreach ($arrey_lower_north_area as $key => $value) {
                        if ($value->province_title == $lower_north_area_key_loop) {
                            $province_three[] = $value;
                        }
                    }

                    foreach ($province_three as $key => $value) {
                        $amphur_title_three[] = $value->amphur_title;
                    }
                    
                    $number_amphur_title_three = array_count_values($amphur_title_three);
                    $number_amphur_title_key_three = array_count_values($amphur_title_three);
                    rsort($number_amphur_title_key_three);
                    arsort($number_amphur_title_three);   
                    $xaa = 0;
                    foreach ($number_amphur_title_three as $key => $value) {
                        if (!isset($amphur_point_three)) {
                            $amphur_point_three = $number_amphur_title_key_three[$xaa];
                            
                            $lower_north_area_number[$lower_north_area_key_loop] = array(
                                'no' => 3,
                                'point' => $lower_north_area_key[$xx],
                                array(
                                    $key=> $amphur_point_three
                                    )
                                );
                            }else{
                                array_push($lower_north_area_number[$lower_north_area_key_loop], array($key=>$number_amphur_title_key_three[$xaa]));
                            }
                            $xaa++;
                        }
                        $province_lower_north_nomber_three = $lower_north_area_key[$xx];
                }
            }elseif(isset($province_lower_north_nomber_three) && !isset($province_lower_north_nomber_four)){
                if ($province_lower_north_nomber_three == $lower_north_area_value) {
                    unset($amphur_point_three);
                    unset($province_three);
                    unset($amphur_title_three);
                    foreach ($arrey_lower_north_area as $key => $value) {
                        if ($value->province_title == $lower_north_area_key_loop) {
                            $province_three[] = $value;
                        }
                    }

                    foreach ($province_three as $key => $value) {
                        $amphur_title_three[] = $value->amphur_title;
                    }
                    
                    $number_amphur_title_three = array_count_values($amphur_title_three);
                    $number_amphur_title_key_three = array_count_values($amphur_title_three);
                    rsort($number_amphur_title_key_three);
                    arsort($number_amphur_title_three);   
                    $xaa = 0;
                    foreach ($number_amphur_title_three as $key => $value) {
                        if (!isset($amphur_point_three)) {
                            $amphur_point_three = $number_amphur_title_key_three[$xaa];
                            
                            $lower_north_area_number[$lower_north_area_key_loop] = array(
                                'no' => 3,
                                'point' => $lower_north_area_key[$xx],
                                array(
                                    $key=> $amphur_point_three
                                    )
                                );
                            }else{
                                array_push($lower_north_area_number[$lower_north_area_key_loop], array($key=>$number_amphur_title_key_three[$xaa]));
                            }
                            $xaa++;
                        }
                }else {
                    foreach ($arrey_lower_north_area as $key => $value) {
                        if ($value->province_title == $lower_north_area_key_loop) {
                            $province_four[] = $value;
                        }
                    }

                    foreach ($province_four as $key => $value) {
                        $amphur_title_four[] = $value->amphur_title;
                    }
                    
                    $number_amphur_title_four = array_count_values($amphur_title_four);
                    $number_amphur_title_key_four = array_count_values($amphur_title_four);
                    rsort($number_amphur_title_key_four);
                    arsort($number_amphur_title_four);
                    $xaa = 0;
                    foreach ($number_amphur_title_four as $key => $value) {
                        if (!isset($amphur_point_four)) {
                            $amphur_point_four = $number_amphur_title_key_four[$xaa];
                            
                            $lower_north_area_number[$lower_north_area_key_loop] = array(
                                'no' => 4,
                                'point' => $lower_north_area_key[$xx],
                                array(
                                    $key=> $amphur_point_four
                                    )
                                );
                            }else{
                                array_push($lower_north_area_number[$lower_north_area_key_loop], array($key=>$number_amphur_title_key_four[$xaa]));
                            }
                            $xaa++;
                        }
                        $province_lower_north_nomber_four = $lower_north_area_key[$xx];
                }
            }elseif(isset($province_lower_north_nomber_four) && !isset($province_lower_north_nomber_five)){
                if ($province_lower_north_nomber_four == $lower_north_area_value) {
                    unset($amphur_point_four);
                    unset($province_four);
                    unset($amphur_title_four);
                    foreach ($arrey_lower_north_area as $key => $value) {
                        if ($value->province_title == $lower_north_area_key_loop) {
                            $province_four[] = $value;
                        }
                    }

                    foreach ($province_four as $key => $value) {
                        $amphur_title_four[] = $value->amphur_title;
                    }
                    
                    $number_amphur_title_four = array_count_values($amphur_title_four);
                    $number_amphur_title_key_four = array_count_values($amphur_title_four);
                    rsort($number_amphur_title_key_four);
                    arsort($number_amphur_title_four);   
                    $xaa = 0;
                    foreach ($number_amphur_title_four as $key => $value) {
                        if (!isset($amphur_point_four)) {
                            $amphur_point_four = $number_amphur_title_key_four[$xaa];
                            
                            $lower_north_area_number[$lower_north_area_key_loop] = array(
                                'no' => 4,
                                'point' => $lower_north_area_key[$xx],
                                array(
                                    $key=> $amphur_point_four
                                    )
                                );
                            }else{
                                array_push($lower_north_area_number[$lower_north_area_key_loop], array($key=>$number_amphur_title_key_four[$xaa]));
                            }
                            $xaa++;
                        }
                }else{
                    foreach ($arrey_lower_north_area as $key => $value) {
                        if ($value->province_title == $lower_north_area_key_loop) {
                            $province_five[] = $value;
                        }
                    }

                    foreach ($province_five as $key => $value) {
                        $amphur_title_five[] = $value->amphur_title;
                    }
                    
                    $number_amphur_title_five = array_count_values($amphur_title_five);
                    $number_amphur_title_key_five = array_count_values($amphur_title_five);
                    rsort($number_amphur_title_key_five);
                    arsort($number_amphur_title_five);
                    $xaa = 0;
                    foreach ($number_amphur_title_five as $key => $value) {
                        if (!isset($amphur_point_five)) {
                            $amphur_point_five = $number_amphur_title_key_five[$xaa];
                            
                            $lower_north_area_number[$lower_north_area_key_loop] = array(
                                'no' => 5,
                                'point' => $lower_north_area_key[$xx],
                                array(
                                    $key=> $amphur_point_five
                                    )
                                );
                            }else{
                                array_push($lower_north_area_number[$lower_north_area_key_loop], array($key=>$number_amphur_title_key_five[$xaa]));
                            }
                            $xaa++;
                        }
                        $province_lower_north_nomber_five = $lower_north_area_key[$xx];
                }
            }
        $xx++; 
    }
                unset($amphur_point_one);
                unset($province_one);
                unset($amphur_title_one);
                unset($amphur_point_two);
                unset($province_two);
                unset($amphur_title_two);
                unset($amphur_point_three);
                unset($province_three);
                unset($amphur_title_three);
                unset($amphur_point_four);
                unset($province_four);
                unset($amphur_title_four);
                unset($amphur_point_five);
                unset($province_five);
                unset($amphur_title_five);
                                

    // จบคัดลำดับเหนื่อล่าง


    // คัดลำดับกลาง
                $central_area = array(
                    'กรุงเทพมหานคร' => $count_bangkok,
                    'จ.กาญจนบุรี' =>$count_kanchanaburi,
                    'จ.ชัยนาท' =>$count_chainati,
                    'จ.นครปฐม' =>$count_nakhonpathom,
                    'จ.ลพบุรี' =>$count_lopburi,
                    'จ.นครสวรรค์' =>$count_nakhonsawan,
                    'จ.สุพรรณบุรี' =>$count_suphanburi,
                    'จ.นนทบุรี' =>$count_nonthaburi,
                    'จ.ปทุมธานี' =>$count_pathumthani,
                    'จ.สระบุรี' =>$count_saraburi,
                    'จ.สิงห์บุรี' => $count_singburi,
                    'จ.พระนครศรีอยุธยา' =>$count_ayutthaya,
                    'จ.อ่างทอง' =>$count_angthong,
                    'จ.อุทัยธานี' =>$count_uthaithani
                );

            $central_area_key = $central_area;
            rsort($central_area_key);
            arsort($central_area);
            $xx = 0;
            foreach ($central_area as $central_area_key_loop => $central_area_value) {
                if(!isset($province_central_nomber_one)){
                    foreach ($arrey_central_area as $key => $value) {
                        if ($value->province_title == $central_area_key_loop) {
                            $province_one[] = $value;
                        }
                    }
                    
                    foreach ($province_one as $key => $value) {
                        $amphur_title_one[] = $value->amphur_title;
                    }
                    $number_amphur_title_one = array_count_values($amphur_title_one);
                    $number_amphur_title_key_one = array_count_values($amphur_title_one);
                    rsort($number_amphur_title_key_one);
                    arsort($number_amphur_title_one);   
                
                    
                    $xaa = 0;
                foreach ($number_amphur_title_one as $key => $value) {
                    if (!isset($amphur_point_one)) {
                        $amphur_point_one = $number_amphur_title_key_one[$xaa];

                        $central_area_number[$central_area_key_loop] = array(
                            'no' => 1,
                            'point' => $central_area_key[$xx],
                            array(
                                $key=> $amphur_point_one
                                )
                        );
                    }else{
                        array_push($central_area_number[$central_area_key_loop], array($key=>$number_amphur_title_key_one[$xaa]));
                    }
                    $xaa++;
                    }
                    $province_central_nomber_one = $central_area_key[$xx];
                
            }elseif(isset($province_central_nomber_one) && !isset($province_central_nomber_two)){
                if ($province_central_nomber_one == $central_area_value) {
                    unset($amphur_point_one);
                    unset($province_one);
                    unset($amphur_title_one);
                    foreach ($arrey_central_area as $key => $value) {
                        if ($value->province_title == $central_area_key_loop) {
                            $province_one[] = $value;
                        }
                    }

                    foreach ($province_one as $key => $value) {
                        $amphur_title_one[] = $value->amphur_title;
                    }
                    
                    $number_amphur_title_one = array_count_values($amphur_title_one);
                    $number_amphur_title_key_one = array_count_values($amphur_title_one);
                    rsort($number_amphur_title_key_one);
                    arsort($number_amphur_title_one);   
                    $xaa = 0;
                    foreach ($number_amphur_title_one as $key => $value) {
                        if (!isset($amphur_point_one)) {
                            $amphur_point_one = $number_amphur_title_key_one[$xaa];
                            
                            $central_area_number[$central_area_key_loop] = array(
                                'no' => 2,
                                'point' => $central_area_key[$xx],
                                array(
                                    $key=> $amphur_point_one
                                    )
                                );
                            }else{
                                array_push($central_area_number[$central_area_key_loop], array($key=>$number_amphur_title_key_one[$xaa]));
                            }
                            $xaa++;
                        }
                    
                }else{
                    foreach ($arrey_central_area as $key => $value) {
                        if ($value->province_title == $central_area_key_loop) {
                            $province_two[] = $value;
                        }
                    }

                    foreach ($province_two as $key => $value) {
                        $amphur_title_two[] = $value->amphur_title;
                    }
                    
                    $number_amphur_title_two = array_count_values($amphur_title_two);
                    $number_amphur_title_key_two = array_count_values($amphur_title_two);
                    rsort($number_amphur_title_key_two);
                    arsort($number_amphur_title_two);   
                    $xaa = 0;
                    foreach ($number_amphur_title_two as $key => $value) {
                        if (!isset($amphur_point_two)) {
                            $amphur_point_two = $number_amphur_title_key_two[$xaa];
                            
                            $central_area_number[$central_area_key_loop] = array(
                                'no' => 2,
                                'point' => $central_area_key[$xx],
                                array(
                                    $key=> $amphur_point_two
                                    )
                                );
                            }else{
                                array_push($central_area_number[$central_area_key_loop], array($key=>$number_amphur_title_key_two[$xaa]));
                            }
                            $xaa++;
                        }
                        $province_central_nomber_two = $central_area_key[$xx];
                }
            }elseif(isset($province_central_nomber_two) && !isset($province_central_nomber_three)){
                if ($province_central_nomber_two == $central_area_value) {
                    unset($amphur_point_two);
                    unset($province_two);
                    unset($amphur_title_two);
                    foreach ($arrey_central_area as $key => $value) {
                        if ($value->province_title == $central_area_key_loop) {
                            $province_two[] = $value;
                        }
                    }

                    foreach ($province_two as $key => $value) {
                        $amphur_title_two[] = $value->amphur_title;
                    }
                    
                    $number_amphur_title_two = array_count_values($amphur_title_two);
                    $number_amphur_title_key_two = array_count_values($amphur_title_two);
                    rsort($number_amphur_title_key_two);
                    arsort($number_amphur_title_two);   
                    $xaa = 0;
                    foreach ($number_amphur_title_two as $key => $value) {
                        if (!isset($amphur_point_two)) {
                            $amphur_point_two = $number_amphur_title_key_two[$xaa];
                            
                            $central_area_number[$central_area_key_loop] = array(
                                'no' => 3,
                                'point' => $central_area_key[$xx],
                                array(
                                    $key=> $amphur_point_two
                                    )
                                );
                            }else{
                                array_push($central_area_number[$central_area_key_loop], array($key=>$number_amphur_title_key_two[$xaa]));
                            }
                            $xaa++;
                        }
                }else{
                    foreach ($arrey_central_area as $key => $value) {
                        if ($value->province_title == $central_area_key_loop) {
                            $province_three[] = $value;
                        }
                    }

                    foreach ($province_three as $key => $value) {
                        $amphur_title_three[] = $value->amphur_title;
                    }
                    
                    $number_amphur_title_three = array_count_values($amphur_title_three);
                    $number_amphur_title_key_three = array_count_values($amphur_title_three);
                    rsort($number_amphur_title_key_three);
                    arsort($number_amphur_title_three);   
                    $xaa = 0;
                    foreach ($number_amphur_title_three as $key => $value) {
                        if (!isset($amphur_point_three)) {
                            $amphur_point_three = $number_amphur_title_key_three[$xaa];
                            
                            $central_area_number[$central_area_key_loop] = array(
                                'no' => 3,
                                'point' => $central_area_key[$xx],
                                array(
                                    $key=> $amphur_point_three
                                    )
                                );
                            }else{
                                array_push($central_area_number[$central_area_key_loop], array($key=>$number_amphur_title_key_three[$xaa]));
                            }
                            $xaa++;
                        }
                        $province_central_nomber_three = $central_area_key[$xx];
                }
            }elseif(isset($province_central_nomber_three) && !isset($province_central_nomber_four)){
                if ($province_central_nomber_three == $central_area_value) {
                    unset($amphur_point_three);
                    unset($province_three);
                    unset($amphur_title_three);
                    foreach ($arrey_central_area as $key => $value) {
                        if ($value->province_title == $central_area_key_loop) {
                            $province_three[] = $value;
                        }
                    }

                    foreach ($province_three as $key => $value) {
                        $amphur_title_three[] = $value->amphur_title;
                    }
                    
                    $number_amphur_title_three = array_count_values($amphur_title_three);
                    $number_amphur_title_key_three = array_count_values($amphur_title_three);
                    rsort($number_amphur_title_key_three);
                    arsort($number_amphur_title_three);   
                    $xaa = 0;
                    foreach ($number_amphur_title_three as $key => $value) {
                        if (!isset($amphur_point_three)) {
                            $amphur_point_three = $number_amphur_title_key_three[$xaa];
                            
                            $central_area_number[$central_area_key_loop] = array(
                                'no' => 3,
                                'point' => $central_area_key[$xx],
                                array(
                                    $key=> $amphur_point_three
                                    )
                                );
                            }else{
                                array_push($central_area_number[$central_area_key_loop], array($key=>$number_amphur_title_key_three[$xaa]));
                            }
                            $xaa++;
                        }
                }else {
                    foreach ($arrey_central_area as $key => $value) {
                        if ($value->province_title == $central_area_key_loop) {
                            $province_four[] = $value;
                        }
                    }

                    if (isset($province_four)) {
                    
                
                    foreach ($province_four as $key => $value) {
                        $amphur_title_four[] = $value->amphur_title;
                    }
                    
                    $number_amphur_title_four = array_count_values($amphur_title_four);
                    $number_amphur_title_key_four = array_count_values($amphur_title_four);
                    rsort($number_amphur_title_key_four);
                    arsort($number_amphur_title_four);
                    $xaa = 0;
                    foreach ($number_amphur_title_four as $key => $value) {
                        if (!isset($amphur_point_four)) {
                            $amphur_point_four = $number_amphur_title_key_four[$xaa];
                            
                            $central_area_number[$central_area_key_loop] = array(
                                'no' => 4,
                                'point' => $central_area_key[$xx],
                                array(
                                    $key=> $amphur_point_four
                                    )
                                );
                            }else{
                                array_push($central_area_number[$central_area_key_loop], array($key=>$number_amphur_title_key_four[$xaa]));
                            }
                            $xaa++;
                        }
                        $province_central_nomber_four = $central_area_key[$xx];
                    }
                }
            }elseif(isset($province_central_nomber_four) && !isset($province_central_nomber_five)){
                if ($province_central_nomber_four == $central_area_value) {
                    unset($amphur_point_four);
                    unset($province_four);
                    unset($amphur_title_four);
                    foreach ($arrey_central_area as $key => $value) {
                        if ($value->province_title == $central_area_key_loop) {
                            $province_four[] = $value;
                        }
                    }

                    foreach ($province_four as $key => $value) {
                        $amphur_title_four[] = $value->amphur_title;
                    }
                    
                    $number_amphur_title_four = array_count_values($amphur_title_four);
                    $number_amphur_title_key_four = array_count_values($amphur_title_four);
                    rsort($number_amphur_title_key_four);
                    arsort($number_amphur_title_four);   
                    $xaa = 0;
                    foreach ($number_amphur_title_four as $key => $value) {
                        if (!isset($amphur_point_four)) {
                            $amphur_point_four = $number_amphur_title_key_four[$xaa];
                            
                            $central_area_number[$central_area_key_loop] = array(
                                'no' => 4,
                                'point' => $central_area_key[$xx],
                                array(
                                    $key=> $amphur_point_four
                                    )
                                );
                            }else{
                                array_push($central_area_number[$central_area_key_loop], array($key=>$number_amphur_title_key_four[$xaa]));
                            }
                            $xaa++;
                        }
                }else{
                    foreach ($arrey_central_area as $key => $value) {
                        if ($value->province_title == $central_area_key_loop) {
                            $province_five[] = $value;
                        }
                    }

                    foreach ($province_five as $key => $value) {
                        $amphur_title_five[] = $value->amphur_title;
                    }
                    
                    $number_amphur_title_five = array_count_values($amphur_title_five);
                    $number_amphur_title_key_five = array_count_values($amphur_title_five);
                    rsort($number_amphur_title_key_five);
                    arsort($number_amphur_title_five);
                    $xaa = 0;
                    foreach ($number_amphur_title_five as $key => $value) {
                        if (!isset($amphur_point_five)) {
                            $amphur_point_five = $number_amphur_title_key_five[$xaa];
                            
                            $central_area_number[$central_area_key_loop] = array(
                                'no' => 5,
                                'point' => $central_area_key[$xx],
                                array(
                                    $key=> $amphur_point_five
                                    )
                                );
                            }else{
                                array_push($central_area_number[$central_area_key_loop], array($key=>$number_amphur_title_key_five[$xaa]));
                            }
                            $xaa++;
                        }
                        $province_central_nomber_five = $central_area_key[$xx];
                }
            }
            $xx++; 
            }
            unset($amphur_point_one);
            unset($province_one);
            unset($amphur_title_one);
            unset($amphur_point_two);
            unset($province_two);
            unset($amphur_title_two);
            unset($amphur_point_three);
            unset($province_three);
            unset($amphur_title_three);
            unset($amphur_point_four);
            unset($province_four);
            unset($amphur_title_four);
            unset($amphur_point_five);
            unset($province_five);
            unset($amphur_title_five);
                    

    // จบคัดลำดับกลาง

    // คัดลำดับอีสานบน
    $upper_northeast_area = array(
        'จ.กาฬสินธุ์' =>$count_kalasin,
        'จ.ขอนแก่น'=>$count_khonkaen,
        'จ.นครพนม'=>$count_nakhonphanom,
        'จ.บึงกาฬ'=>$count_buengkan,
        'จ.มุกดาหาร'=>$count_mukdahan,
        'จ.เลย'=>$count_loei,
        'จ.สกลนคร'=>$count_sakonnakhon,
        'จ.หนองคาย'=>$count_nongkhai,
        'จ.หนองบัวลำภู'=>$count_nongbualamphu,
        'จ.อุดรธานี'=>$count_udonthani
    );

    $upper_northeast_area_key = $upper_northeast_area;
    rsort($upper_northeast_area_key);
    arsort($upper_northeast_area);
    $xx = 0;
    foreach ($upper_northeast_area as $upper_northeast_area_key_loop => $upper_northeast_area_value) {
    if(!isset($province_upper_northeast_nomber_one)){
        foreach ($arrey_upper_northeast_area as $key => $value) {
            if ($value->province_title == $upper_northeast_area_key_loop) {
                $province_one[] = $value;
            }
        }
        
        foreach ($province_one as $key => $value) {
            $amphur_title_one[] = $value->amphur_title;
        }
        $number_amphur_title_one = array_count_values($amphur_title_one);
        $number_amphur_title_key_one = array_count_values($amphur_title_one);
        rsort($number_amphur_title_key_one);
        arsort($number_amphur_title_one);   

        
        $xaa = 0;
    foreach ($number_amphur_title_one as $key => $value) {
        if (!isset($amphur_point_one)) {
            $amphur_point_one = $number_amphur_title_key_one[$xaa];

            $upper_northeast_area_number[$upper_northeast_area_key_loop] = array(
                'no' => 1,
                'point' => $upper_northeast_area_key[$xx],
                array(
                    $key=> $amphur_point_one
                    )
            );
        }else{
            array_push($upper_northeast_area_number[$upper_northeast_area_key_loop], array($key=>$number_amphur_title_key_one[$xaa]));
        }
        $xaa++;
        }
        $province_upper_northeast_nomber_one = $upper_northeast_area_key[$xx];

    }elseif(isset($province_upper_northeast_nomber_one) && !isset($province_upper_northeast_nomber_two)){
    if ($province_upper_northeast_nomber_one == $upper_northeast_area_value) {
        unset($amphur_point_one);
        unset($province_one);
        unset($amphur_title_one);
        foreach ($arrey_upper_northeast_area as $key => $value) {
            if ($value->province_title == $upper_northeast_area_key_loop) {
                $province_one[] = $value;
            }
        }

        foreach ($province_one as $key => $value) {
            $amphur_title_one[] = $value->amphur_title;
        }
        
        $number_amphur_title_one = array_count_values($amphur_title_one);
        $number_amphur_title_key_one = array_count_values($amphur_title_one);
        rsort($number_amphur_title_key_one);
        arsort($number_amphur_title_one);   
        $xaa = 0;
        foreach ($number_amphur_title_one as $key => $value) {
            if (!isset($amphur_point_one)) {
                $amphur_point_one = $number_amphur_title_key_one[$xaa];
                
                $upper_northeast_area_number[$upper_northeast_area_key_loop] = array(
                    'no' => 2,
                    'point' => $upper_northeast_area_key[$xx],
                    array(
                        $key=> $amphur_point_one
                        )
                    );
                }else{
                    array_push($upper_northeast_area_number[$upper_northeast_area_key_loop], array($key=>$number_amphur_title_key_one[$xaa]));
                }
                $xaa++;
            }
        
    }else{
        foreach ($arrey_upper_northeast_area as $key => $value) {
            if ($value->province_title == $upper_northeast_area_key_loop) {
                $province_two[] = $value;
            }
        }

        foreach ($province_two as $key => $value) {
            $amphur_title_two[] = $value->amphur_title;
        }
        
        $number_amphur_title_two = array_count_values($amphur_title_two);
        $number_amphur_title_key_two = array_count_values($amphur_title_two);
        rsort($number_amphur_title_key_two);
        arsort($number_amphur_title_two);   
        $xaa = 0;
        foreach ($number_amphur_title_two as $key => $value) {
            if (!isset($amphur_point_two)) {
                $amphur_point_two = $number_amphur_title_key_two[$xaa];
                
                $upper_northeast_area_number[$upper_northeast_area_key_loop] = array(
                    'no' => 2,
                    'point' => $upper_northeast_area_key[$xx],
                    array(
                        $key=> $amphur_point_two
                        )
                    );
                }else{
                    array_push($upper_northeast_area_number[$upper_northeast_area_key_loop], array($key=>$number_amphur_title_key_two[$xaa]));
                }
                $xaa++;
            }
            $province_upper_northeast_nomber_two = $upper_northeast_area_key[$xx];
    }
    }elseif(isset($province_upper_northeast_nomber_two) && !isset($province_upper_northeast_nomber_three)){
    if ($province_upper_northeast_nomber_two == $upper_northeast_area_value) {
        unset($amphur_point_two);
        unset($province_two);
        unset($amphur_title_two);
        foreach ($arrey_upper_northeast_area as $key => $value) {
            if ($value->province_title == $upper_northeast_area_key_loop) {
                $province_two[] = $value;
            }
        }

        foreach ($province_two as $key => $value) {
            $amphur_title_two[] = $value->amphur_title;
        }
        
        $number_amphur_title_two = array_count_values($amphur_title_two);
        $number_amphur_title_key_two = array_count_values($amphur_title_two);
        rsort($number_amphur_title_key_two);
        arsort($number_amphur_title_two);   
        $xaa = 0;
        foreach ($number_amphur_title_two as $key => $value) {
            if (!isset($amphur_point_two)) {
                $amphur_point_two = $number_amphur_title_key_two[$xaa];
                
                $upper_northeast_area_number[$upper_northeast_area_key_loop] = array(
                    'no' => 3,
                    'point' => $upper_northeast_area_key[$xx],
                    array(
                        $key=> $amphur_point_two
                        )
                    );
                }else{
                    array_push($upper_northeast_area_number[$upper_northeast_area_key_loop], array($key=>$number_amphur_title_key_two[$xaa]));
                }
                $xaa++;
            }
    }else{
        foreach ($arrey_upper_northeast_area as $key => $value) {
            if ($value->province_title == $upper_northeast_area_key_loop) {
                $province_three[] = $value;
            }
        }
        if (isset($province_three)) {
        
        
        foreach ($province_three as $key => $value) {
            $amphur_title_three[] = $value->amphur_title;
        }
        
        $number_amphur_title_three = array_count_values($amphur_title_three);
        $number_amphur_title_key_three = array_count_values($amphur_title_three);
        rsort($number_amphur_title_key_three);
        arsort($number_amphur_title_three);   
        $xaa = 0;
        foreach ($number_amphur_title_three as $key => $value) {
            if (!isset($amphur_point_three)) {
                $amphur_point_three = $number_amphur_title_key_three[$xaa];
                
                $upper_northeast_area_number[$upper_northeast_area_key_loop] = array(
                    'no' => 3,
                    'point' => $upper_northeast_area_key[$xx],
                    array(
                        $key=> $amphur_point_three
                        )
                    );
                }else{
                    array_push($upper_northeast_area_number[$upper_northeast_area_key_loop], array($key=>$number_amphur_title_key_three[$xaa]));
                }
                $xaa++;
            }
            $province_upper_northeast_nomber_three = $upper_northeast_area_key[$xx];
        }
    }
    }elseif(isset($province_upper_northeast_nomber_three) && !isset($province_upper_northeast_nomber_four)){
    if ($province_upper_northeast_nomber_three == $upper_northeast_area_value) {
        unset($amphur_point_three);
        unset($province_three);
        unset($amphur_title_three);
        foreach ($arrey_upper_northeast_area as $key => $value) {
            if ($value->province_title == $upper_northeast_area_key_loop) {
                $province_three[] = $value;
            }
        }

        foreach ($province_three as $key => $value) {
            $amphur_title_three[] = $value->amphur_title;
        }
        
        $number_amphur_title_three = array_count_values($amphur_title_three);
        $number_amphur_title_key_three = array_count_values($amphur_title_three);
        rsort($number_amphur_title_key_three);
        arsort($number_amphur_title_three);   
        $xaa = 0;
        foreach ($number_amphur_title_three as $key => $value) {
            if (!isset($amphur_point_three)) {
                $amphur_point_three = $number_amphur_title_key_three[$xaa];
                
                $upper_northeast_area_number[$upper_northeast_area_key_loop] = array(
                    'no' => 3,
                    'point' => $upper_northeast_area_key[$xx],
                    array(
                        $key=> $amphur_point_three
                        )
                    );
                }else{
                    array_push($upper_northeast_area_number[$upper_northeast_area_key_loop], array($key=>$number_amphur_title_key_three[$xaa]));
                }
                $xaa++;
            }
    }else {
        foreach ($arrey_upper_northeast_area as $key => $value) {
            if ($value->province_title == $upper_northeast_area_key_loop) {
                $province_four[] = $value;
            }
        }

        foreach ($province_four as $key => $value) {
            $amphur_title_four[] = $value->amphur_title;
        }
        
        $number_amphur_title_four = array_count_values($amphur_title_four);
        $number_amphur_title_key_four = array_count_values($amphur_title_four);
        rsort($number_amphur_title_key_four);
        arsort($number_amphur_title_four);
        $xaa = 0;
        foreach ($number_amphur_title_four as $key => $value) {
            if (!isset($amphur_point_four)) {
                $amphur_point_four = $number_amphur_title_key_four[$xaa];
                
                $upper_northeast_area_number[$upper_northeast_area_key_loop] = array(
                    'no' => 4,
                    'point' => $upper_northeast_area_key[$xx],
                    array(
                        $key=> $amphur_point_four
                        )
                    );
                }else{
                    array_push($upper_northeast_area_number[$upper_northeast_area_key_loop], array($key=>$number_amphur_title_key_four[$xaa]));
                }
                $xaa++;
            }
            $province_upper_northeast_nomber_four = $upper_northeast_area_key[$xx];
    }
    }elseif(isset($province_upper_northeast_nomber_four) && !isset($province_upper_northeast_nomber_five)){
    if ($province_upper_northeast_nomber_four == $upper_northeast_area_value) {
        unset($amphur_point_four);
        unset($province_four);
        unset($amphur_title_four);
        foreach ($arrey_upper_northeast_area as $key => $value) {
            if ($value->province_title == $upper_northeast_area_key_loop) {
                $province_four[] = $value;
            }
        }

        foreach ($province_four as $key => $value) {
            $amphur_title_four[] = $value->amphur_title;
        }
        
        $number_amphur_title_four = array_count_values($amphur_title_four);
        $number_amphur_title_key_four = array_count_values($amphur_title_four);
        rsort($number_amphur_title_key_four);
        arsort($number_amphur_title_four);   
        $xaa = 0;
        foreach ($number_amphur_title_four as $key => $value) {
            if (!isset($amphur_point_four)) {
                $amphur_point_four = $number_amphur_title_key_four[$xaa];
                
                $upper_northeast_area_number[$upper_northeast_area_key_loop] = array(
                    'no' => 4,
                    'point' => $upper_northeast_area_key[$xx],
                    array(
                        $key=> $amphur_point_four
                        )
                    );
                }else{
                    array_push($upper_northeast_area_number[$upper_northeast_area_key_loop], array($key=>$number_amphur_title_key_four[$xaa]));
                }
                $xaa++;
            }
    }else{
        foreach ($arrey_upper_northeast_area as $key => $value) {
            if ($value->province_title == $upper_northeast_area_key_loop) {
                $province_five[] = $value;
            }
        }

        if (isset($province_five)) {
    
        foreach ($province_five as $key => $value) {
            $amphur_title_five[] = $value->amphur_title;
        }
        
        $number_amphur_title_five = array_count_values($amphur_title_five);
        $number_amphur_title_key_five = array_count_values($amphur_title_five);
        rsort($number_amphur_title_key_five);
        arsort($number_amphur_title_five);
        $xaa = 0;
        foreach ($number_amphur_title_five as $key => $value) {
            if (!isset($amphur_point_five)) {
                $amphur_point_five = $number_amphur_title_key_five[$xaa];
                
                $upper_northeast_area_number[$upper_northeast_area_key_loop] = array(
                    'no' => 5,
                    'point' => $upper_northeast_area_key[$xx],
                    array(
                        $key=> $amphur_point_five
                        )
                    );
                }else{
                    array_push($upper_northeast_area_number[$upper_northeast_area_key_loop], array($key=>$number_amphur_title_key_five[$xaa]));
                }
                $xaa++;
            }
            $province_upper_northeast_nomber_five = $upper_northeast_area_key[$xx];
        }
    }
    }
    $xx++; 
    }
    unset($amphur_point_one);
    unset($province_one);
    unset($amphur_title_one);
    unset($amphur_point_two);
    unset($province_two);
    unset($amphur_title_two);
    unset($amphur_point_three);
    unset($province_three);
    unset($amphur_title_three);
    unset($amphur_point_four);
    unset($province_four);
    unset($amphur_title_four);
    unset($amphur_point_five);
    unset($province_five);
    unset($amphur_title_five);
        

    // จบคัดลำดับอีสานบน

    // คัดลำดับอีสานล่าง
    $lower_northeast_area = array(
        'จ.ชัยภูมิ' =>  $count_chaiyaphum,
        'จ.นครราชสีมา'=> $count_nakhonratchasima,
        'จ.บุรีรัมย์'=> $count_buriram,
        'จ.มหาสารคาม'=> $count_mahasarakham,
        'จ.ยโสธร'=> $count_yasothon,
        'จ.ร้อยเอ็ด'=> $count_roiet,
        'จ.ศรีสะเกษ'=> $count_sisaket,
        'จ.สุรินทร์'=> $count_surin,
        'จ.อุบลราชธานี'=> $count_ubon,
        'จ.อำนาจเจริญ'=> $count_amnatcharoen
    );

    $lower_northeast_area_key = $lower_northeast_area;
    rsort($lower_northeast_area_key);
    arsort($lower_northeast_area);
    $xx = 0;
    foreach ($lower_northeast_area as $lower_northeast_area_key_loop => $lower_northeast_area_value) {
    if(!isset($province_lower_northeast_nomber_one)){
        foreach ($arrey_lower_northeast_area as $key => $value) {
            if ($value->province_title == $lower_northeast_area_key_loop) {
                $province_one[] = $value;
            }
        }
        
        foreach ($province_one as $key => $value) {
            $amphur_title_one[] = $value->amphur_title;
        }
        $number_amphur_title_one = array_count_values($amphur_title_one);
        $number_amphur_title_key_one = array_count_values($amphur_title_one);
        rsort($number_amphur_title_key_one);
        arsort($number_amphur_title_one);   

        
        $xaa = 0;
    foreach ($number_amphur_title_one as $key => $value) {
        if (!isset($amphur_point_one)) {
            $amphur_point_one = $number_amphur_title_key_one[$xaa];

            $lower_northeast_area_number[$lower_northeast_area_key_loop] = array(
                'no' => 1,
                'point' => $lower_northeast_area_key[$xx],
                array(
                    $key=> $amphur_point_one
                    )
            );
        }else{
            array_push($lower_northeast_area_number[$lower_northeast_area_key_loop], array($key=>$number_amphur_title_key_one[$xaa]));
        }
        $xaa++;
        }
        $province_lower_northeast_nomber_one = $lower_northeast_area_key[$xx];

    }elseif(isset($province_lower_northeast_nomber_one) && !isset($province_lower_northeast_nomber_two)){
    if ($province_lower_northeast_nomber_one == $lower_northeast_area_value) {
        unset($amphur_point_one);
        unset($province_one);
        unset($amphur_title_one);
        foreach ($arrey_lower_northeast_area as $key => $value) {
            if ($value->province_title == $lower_northeast_area_key_loop) {
                $province_one[] = $value;
            }
        }

        foreach ($province_one as $key => $value) {
            $amphur_title_one[] = $value->amphur_title;
        }
        
        $number_amphur_title_one = array_count_values($amphur_title_one);
        $number_amphur_title_key_one = array_count_values($amphur_title_one);
        rsort($number_amphur_title_key_one);
        arsort($number_amphur_title_one);   
        $xaa = 0;
        foreach ($number_amphur_title_one as $key => $value) {
            if (!isset($amphur_point_one)) {
                $amphur_point_one = $number_amphur_title_key_one[$xaa];
                
                $lower_northeast_area_number[$lower_northeast_area_key_loop] = array(
                    'no' => 1,
                    'point' => $lower_northeast_area_key[$xx],
                    array(
                        $key=> $amphur_point_one
                        )
                    );
                }else{
                    array_push($lower_northeast_area_number[$lower_northeast_area_key_loop], array($key=>$number_amphur_title_key_one[$xaa]));
                }
                $xaa++;
            }
        
    }else{
        foreach ($arrey_lower_northeast_area as $key => $value) {
            if ($value->province_title == $lower_northeast_area_key_loop) {
                $province_two[] = $value;
            }
        }

        foreach ($province_two as $key => $value) {
            $amphur_title_two[] = $value->amphur_title;
        }
        
        $number_amphur_title_two = array_count_values($amphur_title_two);
        $number_amphur_title_key_two = array_count_values($amphur_title_two);
        rsort($number_amphur_title_key_two);
        arsort($number_amphur_title_two);   
        $xaa = 0;
        foreach ($number_amphur_title_two as $key => $value) {
            if (!isset($amphur_point_two)) {
                $amphur_point_two = $number_amphur_title_key_two[$xaa];
                
                $lower_northeast_area_number[$lower_northeast_area_key_loop] = array(
                    'no' => 2,
                    'point' => $lower_northeast_area_key[$xx],
                    array(
                        $key=> $amphur_point_two
                        )
                    );
                }else{
                    array_push($lower_northeast_area_number[$lower_northeast_area_key_loop], array($key=>$number_amphur_title_key_two[$xaa]));
                }
                $xaa++;
            }
            $province_lower_northeast_nomber_two = $lower_northeast_area_key[$xx];
    }
    }elseif(isset($province_lower_northeast_nomber_two) && !isset($province_lower_northeast_nomber_three)){
    if ($province_lower_northeast_nomber_two == $lower_northeast_area_value) {
        unset($amphur_point_two);
        unset($province_two);
        unset($amphur_title_two);
        foreach ($arrey_lower_northeast_area as $key => $value) {
            if ($value->province_title == $lower_northeast_area_key_loop) {
                $province_two[] = $value;
            }
        }

        foreach ($province_two as $key => $value) {
            $amphur_title_two[] = $value->amphur_title;
        }
        
        $number_amphur_title_two = array_count_values($amphur_title_two);
        $number_amphur_title_key_two = array_count_values($amphur_title_two);
        rsort($number_amphur_title_key_two);
        arsort($number_amphur_title_two);   
        $xaa = 0;
        foreach ($number_amphur_title_two as $key => $value) {
            if (!isset($amphur_point_two)) {
                $amphur_point_two = $number_amphur_title_key_two[$xaa];
                
                $lower_northeast_area_number[$lower_northeast_area_key_loop] = array(
                    'no' => 2,
                    'point' => $lower_northeast_area_key[$xx],
                    array(
                        $key=> $amphur_point_two
                        )
                    );
                }else{
                    array_push($lower_northeast_area_number[$lower_northeast_area_key_loop], array($key=>$number_amphur_title_key_two[$xaa]));
                }
                $xaa++;
            }
    }else{
        foreach ($arrey_lower_northeast_area as $key => $value) {
            if ($value->province_title == $lower_northeast_area_key_loop) {
                $province_three[] = $value;
            }
        }

        foreach ($province_three as $key => $value) {
            $amphur_title_three[] = $value->amphur_title;
        }
        
        $number_amphur_title_three = array_count_values($amphur_title_three);
        $number_amphur_title_key_three = array_count_values($amphur_title_three);
        rsort($number_amphur_title_key_three);
        arsort($number_amphur_title_three);   
        $xaa = 0;
        foreach ($number_amphur_title_three as $key => $value) {
            if (!isset($amphur_point_three)) {
                $amphur_point_three = $number_amphur_title_key_three[$xaa];
                
                $lower_northeast_area_number[$lower_northeast_area_key_loop] = array(
                    'no' => 3,
                    'point' => $lower_northeast_area_key[$xx],
                    array(
                        $key=> $amphur_point_three
                        )
                    );
                }else{
                    array_push($lower_northeast_area_number[$lower_northeast_area_key_loop], array($key=>$number_amphur_title_key_three[$xaa]));
                }
                $xaa++;
            }
            $province_lower_northeast_nomber_three = $lower_northeast_area_key[$xx];
    }
    }elseif(isset($province_lower_northeast_nomber_three) && !isset($province_lower_northeast_nomber_four)){
    if ($province_lower_northeast_nomber_three == $lower_northeast_area_value) {
        unset($amphur_point_three);
        unset($province_three);
        unset($amphur_title_three);
        foreach ($arrey_lower_northeast_area as $key => $value) {
            if ($value->province_title == $lower_northeast_area_key_loop) {
                $province_three[] = $value;
            }
        }

        foreach ($province_three as $key => $value) {
            $amphur_title_three[] = $value->amphur_title;
        }
        
        $number_amphur_title_three = array_count_values($amphur_title_three);
        $number_amphur_title_key_three = array_count_values($amphur_title_three);
        rsort($number_amphur_title_key_three);
        arsort($number_amphur_title_three);   
        $xaa = 0;
        foreach ($number_amphur_title_three as $key => $value) {
            if (!isset($amphur_point_three)) {
                $amphur_point_three = $number_amphur_title_key_three[$xaa];
                
                $lower_northeast_area_number[$lower_northeast_area_key_loop] = array(
                    'no' => 3,
                    'point' => $lower_northeast_area_key[$xx],
                    array(
                        $key=> $amphur_point_three
                        )
                    );
                }else{
                    array_push($lower_northeast_area_number[$lower_northeast_area_key_loop], array($key=>$number_amphur_title_key_three[$xaa]));
                }
                $xaa++;
            }
    }else {
        foreach ($arrey_lower_northeast_area as $key => $value) {
            if ($value->province_title == $lower_northeast_area_key_loop) {
                $province_four[] = $value;
            }
        }

        foreach ($province_four as $key => $value) {
            $amphur_title_four[] = $value->amphur_title;
        }
        
        $number_amphur_title_four = array_count_values($amphur_title_four);
        $number_amphur_title_key_four = array_count_values($amphur_title_four);
        rsort($number_amphur_title_key_four);
        arsort($number_amphur_title_four);
        $xaa = 0;
        foreach ($number_amphur_title_four as $key => $value) {
            if (!isset($amphur_point_four)) {
                $amphur_point_four = $number_amphur_title_key_four[$xaa];
                
                $lower_northeast_area_number[$lower_northeast_area_key_loop] = array(
                    'no' => 4,
                    'point' => $lower_northeast_area_key[$xx],
                    array(
                        $key=> $amphur_point_four
                        )
                    );
                }else{
                    array_push($lower_northeast_area_number[$lower_northeast_area_key_loop], array($key=>$number_amphur_title_key_four[$xaa]));
                }
                $xaa++;
            }
            $province_lower_northeast_nomber_four = $lower_northeast_area_key[$xx];
    }
    }elseif(isset($province_lower_northeast_nomber_four) && !isset($province_lower_northeast_nomber_five)){
    if ($province_lower_northeast_nomber_four == $lower_northeast_area_value) {
        unset($amphur_point_four);
        unset($province_four);
        unset($amphur_title_four);
        foreach ($arrey_lower_northeast_area as $key => $value) {
            if ($value->province_title == $lower_northeast_area_key_loop) {
                $province_four[] = $value;
            }
        }

        foreach ($province_four as $key => $value) {
            $amphur_title_four[] = $value->amphur_title;
        }
        
        $number_amphur_title_four = array_count_values($amphur_title_four);
        $number_amphur_title_key_four = array_count_values($amphur_title_four);
        rsort($number_amphur_title_key_four);
        arsort($number_amphur_title_four);   
        $xaa = 0;
        foreach ($number_amphur_title_four as $key => $value) {
            if (!isset($amphur_point_four)) {
                $amphur_point_four = $number_amphur_title_key_four[$xaa];
                
                $lower_northeast_area_number[$lower_northeast_area_key_loop] = array(
                    'no' => 4,
                    'point' => $lower_northeast_area_key[$xx],
                    array(
                        $key=> $amphur_point_four
                        )
                    );
                }else{
                    array_push($lower_northeast_area_number[$lower_northeast_area_key_loop], array($key=>$number_amphur_title_key_four[$xaa]));
                }
                $xaa++;
            }
    }else{
        foreach ($arrey_lower_northeast_area as $key => $value) {
            if ($value->province_title == $lower_northeast_area_key_loop) {
                $province_five[] = $value;
            }
        }

        if (isset($province_five)) {
            
        
        foreach ($province_five as $key => $value) {
            $amphur_title_five[] = $value->amphur_title;
        }
        
        $number_amphur_title_five = array_count_values($amphur_title_five);
        $number_amphur_title_key_five = array_count_values($amphur_title_five);
        rsort($number_amphur_title_key_five);
        arsort($number_amphur_title_five);
        $xaa = 0;
        foreach ($number_amphur_title_five as $key => $value) {
            if (!isset($amphur_point_five)) {
                $amphur_point_five = $number_amphur_title_key_five[$xaa];
                
                $lower_northeast_area_number[$lower_northeast_area_key_loop] = array(
                    'no' => 5,
                    'point' => $lower_northeast_area_key[$xx],
                    array(
                        $key=> $amphur_point_five
                        )
                    );
                }else{
                    array_push($lower_northeast_area_number[$lower_northeast_area_key_loop], array($key=>$number_amphur_title_key_five[$xaa]));
                }
                $xaa++;
            }
            $province_lower_northeast_nomber_five = $lower_northeast_area_key[$xx];
        }
    }
    }
    $xx++; 
    }
    unset($amphur_point_one);
    unset($province_one);
    unset($amphur_title_one);
    unset($amphur_point_two);
    unset($province_two);
    unset($amphur_title_two);
    unset($amphur_point_three);
    unset($province_three);
    unset($amphur_title_three);
    unset($amphur_point_four);
    unset($province_four);
    unset($amphur_title_four);
    unset($amphur_point_five);
    unset($province_five);
    unset($amphur_title_five);
        

    // จบคัดลำดับอีสานล่าง

    // คัดลำดับตะวันออก

    $eastern_area = array(
        'จ.จันทบุรี' => $count_chanthaburi,
        'จ.ฉะเชิงเทรา' =>$count_chachoengsao,
        'จ.ชลบุรี' =>$count_chonburi,
        'จ.ตราด' =>$count_trat,
        'จ.นครนายก' =>$count_nakhonnayok,
        'จ.ปราจีนบุรี' =>$count_prachinburi,
        'จ.ระยอง' =>$count_rayong,
        'จ.สระแก้ว' =>$count_sakaeo
    );

    $eastern_area_key = $eastern_area;
    rsort($eastern_area_key);
    arsort($eastern_area);
    $xx = 0;
    foreach ($eastern_area as $eastern_area_key_loop => $eastern_area_value) {
    if(!isset($province_eastern_nomber_one)){
        foreach ($arrey_eastern_area as $key => $value) {
            if ($value->province_title == $eastern_area_key_loop) {
                $province_one[] = $value;
            }
        }
        
        foreach ($province_one as $key => $value) {
            $amphur_title_one[] = $value->amphur_title;
        }
        $number_amphur_title_one = array_count_values($amphur_title_one);
        $number_amphur_title_key_one = array_count_values($amphur_title_one);
        rsort($number_amphur_title_key_one);
        arsort($number_amphur_title_one);   

        
        $xaa = 0;
    foreach ($number_amphur_title_one as $key => $value) {
        if (!isset($amphur_point_one)) {
            $amphur_point_one = $number_amphur_title_key_one[$xaa];

            $eastern_area_number[$eastern_area_key_loop] = array(
                'no' => 1,
                'point' => $eastern_area_key[$xx],
                array(
                    $key=> $amphur_point_one
                    )
            );
        }else{
            array_push($eastern_area_number[$eastern_area_key_loop], array($key=>$number_amphur_title_key_one[$xaa]));
        }
        $xaa++;
        }
        $province_eastern_nomber_one = $eastern_area_key[$xx];

    }elseif(isset($province_eastern_nomber_one) && !isset($province_eastern_nomber_two)){
    if ($province_eastern_nomber_one == $eastern_area_value) {
        unset($amphur_point_one);
        unset($province_one);
        unset($amphur_title_one);
        foreach ($arrey_eastern_area as $key => $value) {
            if ($value->province_title == $eastern_area_key_loop) {
                $province_one[] = $value;
            }
        }

        foreach ($province_one as $key => $value) {
            $amphur_title_one[] = $value->amphur_title;
        }
        
        $number_amphur_title_one = array_count_values($amphur_title_one);
        $number_amphur_title_key_one = array_count_values($amphur_title_one);
        rsort($number_amphur_title_key_one);
        arsort($number_amphur_title_one);   
        $xaa = 0;
        foreach ($number_amphur_title_one as $key => $value) {
            if (!isset($amphur_point_one)) {
                $amphur_point_one = $number_amphur_title_key_one[$xaa];
                
                $eastern_area_number[$eastern_area_key_loop] = array(
                    'no' => 1,
                    'point' => $eastern_area_key[$xx],
                    array(
                        $key=> $amphur_point_one
                        )
                    );
                }else{
                    array_push($eastern_area_number[$eastern_area_key_loop], array($key=>$number_amphur_title_key_one[$xaa]));
                }
                $xaa++;
            }
        
    }else{
        foreach ($arrey_eastern_area as $key => $value) {
            if ($value->province_title == $eastern_area_key_loop) {
                $province_two[] = $value;
            }
        }

        if (isset($province_two)) {
    
        foreach ($province_two as $key => $value) {
            $amphur_title_two[] = $value->amphur_title;
        }
        
        $number_amphur_title_two = array_count_values($amphur_title_two);
        $number_amphur_title_key_two = array_count_values($amphur_title_two);
        rsort($number_amphur_title_key_two);
        arsort($number_amphur_title_two);   
        $xaa = 0;
        foreach ($number_amphur_title_two as $key => $value) {
            if (!isset($amphur_point_two)) {
                $amphur_point_two = $number_amphur_title_key_two[$xaa];
                
                $eastern_area_number[$eastern_area_key_loop] = array(
                    'no' => 2,
                    'point' => $eastern_area_key[$xx],
                    array(
                        $key=> $amphur_point_two
                        )
                    );
                }else{
                    array_push($eastern_area_number[$eastern_area_key_loop], array($key=>$number_amphur_title_key_two[$xaa]));
                }
                $xaa++;
            }
            $province_eastern_nomber_two = $eastern_area_key[$xx]; 
        }
    }
    }elseif(isset($province_eastern_nomber_two) && !isset($province_eastern_nomber_three)){
    if ($province_eastern_nomber_two == $eastern_area_value) {
        unset($amphur_point_two);
        unset($province_two);
        unset($amphur_title_two);
        foreach ($arrey_eastern_area as $key => $value) {
            if ($value->province_title == $eastern_area_key_loop) {
                $province_two[] = $value;
            }
        }

        foreach ($province_two as $key => $value) {
            $amphur_title_two[] = $value->amphur_title;
        }
        
        $number_amphur_title_two = array_count_values($amphur_title_two);
        $number_amphur_title_key_two = array_count_values($amphur_title_two);
        rsort($number_amphur_title_key_two);
        arsort($number_amphur_title_two);   
        $xaa = 0;
        foreach ($number_amphur_title_two as $key => $value) {
            if (!isset($amphur_point_two)) {
                $amphur_point_two = $number_amphur_title_key_two[$xaa];
                
                $eastern_area_number[$eastern_area_key_loop] = array(
                    'no' => 2,
                    'point' => $eastern_area_key[$xx],
                    array(
                        $key=> $amphur_point_two
                        )
                    );
                }else{
                    array_push($eastern_area_number[$eastern_area_key_loop], array($key=>$number_amphur_title_key_two[$xaa]));
                }
                $xaa++;
            }
    }else{
        foreach ($arrey_eastern_area as $key => $value) {
            if ($value->province_title == $eastern_area_key_loop) {
                $province_three[] = $value;
            }
        }

        foreach ($province_three as $key => $value) {
            $amphur_title_three[] = $value->amphur_title;
        }
        
        $number_amphur_title_three = array_count_values($amphur_title_three);
        $number_amphur_title_key_three = array_count_values($amphur_title_three);
        rsort($number_amphur_title_key_three);
        arsort($number_amphur_title_three);   
        $xaa = 0;
        foreach ($number_amphur_title_three as $key => $value) {
            if (!isset($amphur_point_three)) {
                $amphur_point_three = $number_amphur_title_key_three[$xaa];
                
                $eastern_area_number[$eastern_area_key_loop] = array(
                    'no' => 3,
                    'point' => $eastern_area_key[$xx],
                    array(
                        $key=> $amphur_point_three
                        )
                    );
                }else{
                    array_push($eastern_area_number[$eastern_area_key_loop], array($key=>$number_amphur_title_key_three[$xaa]));
                }
                $xaa++;
            }
            $province_eastern_nomber_three = $eastern_area_key[$xx];
    }
    }elseif(isset($province_eastern_nomber_three) && !isset($province_eastern_nomber_four)){
    if ($province_eastern_nomber_three == $eastern_area_value) {
        unset($amphur_point_three);
        unset($province_three);
        unset($amphur_title_three);
        foreach ($arrey_eastern_area as $key => $value) {
            if ($value->province_title == $eastern_area_key_loop) {
                $province_three[] = $value;
            }
        }

        foreach ($province_three as $key => $value) {
            $amphur_title_three[] = $value->amphur_title;
        }
        
        $number_amphur_title_three = array_count_values($amphur_title_three);
        $number_amphur_title_key_three = array_count_values($amphur_title_three);
        rsort($number_amphur_title_key_three);
        arsort($number_amphur_title_three);   
        $xaa = 0;
        foreach ($number_amphur_title_three as $key => $value) {
            if (!isset($amphur_point_three)) {
                $amphur_point_three = $number_amphur_title_key_three[$xaa];
                
                $eastern_area_number[$eastern_area_key_loop] = array(
                    'no' => 3,
                    'point' => $eastern_area_key[$xx],
                    array(
                        $key=> $amphur_point_three
                        )
                    );
                }else{
                    array_push($eastern_area_number[$eastern_area_key_loop], array($key=>$number_amphur_title_key_three[$xaa]));
                }
                $xaa++;
            }
    }else {
        foreach ($arrey_eastern_area as $key => $value) {
            if ($value->province_title == $eastern_area_key_loop) {
                $province_four[] = $value;
            }
        }

        foreach ($province_four as $key => $value) {
            $amphur_title_four[] = $value->amphur_title;
        }
        
        $number_amphur_title_four = array_count_values($amphur_title_four);
        $number_amphur_title_key_four = array_count_values($amphur_title_four);
        rsort($number_amphur_title_key_four);
        arsort($number_amphur_title_four);
        $xaa = 0;
        foreach ($number_amphur_title_four as $key => $value) {
            if (!isset($amphur_point_four)) {
                $amphur_point_four = $number_amphur_title_key_four[$xaa];
                
                $eastern_area_number[$eastern_area_key_loop] = array(
                    'no' => 4,
                    'point' => $eastern_area_key[$xx],
                    array(
                        $key=> $amphur_point_four
                        )
                    );
                }else{
                    array_push($eastern_area_number[$eastern_area_key_loop], array($key=>$number_amphur_title_key_four[$xaa]));
                }
                $xaa++;
            }
            $province_eastern_nomber_four = $eastern_area_key[$xx];
    }
    }elseif(isset($province_eastern_nomber_four) && !isset($province_eastern_nomber_five)){
    if ($province_eastern_nomber_four == $eastern_area_value) {
        unset($amphur_point_four);
        unset($province_four);
        unset($amphur_title_four);
        foreach ($arrey_eastern_area as $key => $value) {
            if ($value->province_title == $eastern_area_key_loop) {
                $province_four[] = $value;
            }
        }

        foreach ($province_four as $key => $value) {
            $amphur_title_four[] = $value->amphur_title;
        }
        
        $number_amphur_title_four = array_count_values($amphur_title_four);
        $number_amphur_title_key_four = array_count_values($amphur_title_four);
        rsort($number_amphur_title_key_four);
        arsort($number_amphur_title_four);   
        $xaa = 0;
        foreach ($number_amphur_title_four as $key => $value) {
            if (!isset($amphur_point_four)) {
                $amphur_point_four = $number_amphur_title_key_four[$xaa];
                
                $eastern_area_number[$eastern_area_key_loop] = array(
                    'no' => 4,
                    'point' => $eastern_area_key[$xx],
                    array(
                        $key=> $amphur_point_four
                        )
                    );
                }else{
                    array_push($eastern_area_number[$eastern_area_key_loop], array($key=>$number_amphur_title_key_four[$xaa]));
                }
                $xaa++;
            }
    }else{
        foreach ($arrey_eastern_area as $key => $value) {
            if ($value->province_title == $eastern_area_key_loop) {
                $province_five[] = $value;
            }
        }

        foreach ($province_five as $key => $value) {
            $amphur_title_five[] = $value->amphur_title;
        }
        
        $number_amphur_title_five = array_count_values($amphur_title_five);
        $number_amphur_title_key_five = array_count_values($amphur_title_five);
        rsort($number_amphur_title_key_five);
        arsort($number_amphur_title_five);
        $xaa = 0;
        foreach ($number_amphur_title_five as $key => $value) {
            if (!isset($amphur_point_five)) {
                $amphur_point_five = $number_amphur_title_key_five[$xaa];
                
                $eastern_area_number[$eastern_area_key_loop] = array(
                    'no' => 5,
                    'point' => $eastern_area_key[$xx],
                    array(
                        $key=> $amphur_point_five
                        )
                    );
                }else{
                    array_push($eastern_area_number[$eastern_area_key_loop], array($key=>$number_amphur_title_key_five[$xaa]));
                }
                $xaa++;
            }
            $province_eastern_nomber_five = $eastern_area_key[$xx];
    }
    }
    $xx++; 
    }
    unset($amphur_point_one);
    unset($province_one);
    unset($amphur_title_one);
    unset($amphur_point_two);
    unset($province_two);
    unset($amphur_title_two);
    unset($amphur_point_three);
    unset($province_three);
    unset($amphur_title_three);
    unset($amphur_point_four);
    unset($province_four);
    unset($amphur_title_four);
    unset($amphur_point_five);
    unset($province_five);
    unset($amphur_title_five);
        

    // จบคัดลำดับตะวันออก

                    $southern_area = array(
                        'จ.กระบี่' =>$count_krabi,
                        'จ.ชุมพร' =>$count_chumphon,
                        'จ.สุราษฎร์ธานี' =>$count_suratthani,
                        'จ.นครศรีธรรมราช' =>$count_nakhonsithammarat,
                        'จ.นราธิวาส' =>$count_narathiwat,
                        'จ.ประจวบคีรีขันธ์' =>$count_prachuapkhirikhan,
                        'จ.ปัตตานี' =>$count_pattani,
                        'จ.พังงา' =>$count_phangnga,
                        'จ.พัทลุง' =>$count_phatthalung,
                        'จ.สมุทรสงคราม' =>$count_samutsongkhram,
                        'จ.เพชรบุรี' =>$count_phetchaburi,
                        'จ.ภูเก็ต' =>$count_phuket,
                        'จ.สมุทรสาคร' =>$count_samutsakhon,
                        'จ.สมุทรปราการ' =>$count_samutprakan,
                        'จ.ระนอง' =>$count_ranong,
                        'จ.สตูล' =>$count_satun,
                        'จ.ราชบุรี' =>$count_ratchaburi,
                        'จ.สงขลา' =>$count_songkhla,
                        'จ.ตรัง' =>$count_trang,
                        'จ.ยะลา' =>$count_yala
                    );
                    $southern_area_key = $southern_area;
                    
                    rsort($southern_area_key);
                    arsort($southern_area);
                    $xx = 0;
                    foreach ($southern_area as $southern_area_key_loop => $southern_area_value) {
                        if(!isset($province_southern_nomber_one)){
                            foreach ($arrey_southern_area as $key => $value) {
                                if ($value->province_title == $southern_area_key_loop) {
                                    $province_one[] = $value;
                                }
                            }
                            
                            foreach ($province_one as $key => $value) {
                                $amphur_title_one[] = $value->amphur_title;
                            }
                            $number_amphur_title_one = array_count_values($amphur_title_one);
                            $number_amphur_title_key_one = array_count_values($amphur_title_one);
                            rsort($number_amphur_title_key_one);
                            arsort($number_amphur_title_one);   
                        
                            
                            $xaa = 0;
                        foreach ($number_amphur_title_one as $key => $value) {
                            if (!isset($amphur_point_one)) {
                                $amphur_point_one = $number_amphur_title_key_one[$xaa];

                                $southern_area_number[$southern_area_key_loop] = array(
                                    'no' => 1,
                                    'point' => $southern_area_key[$xx],
                                    array(
                                        $key=> $amphur_point_one
                                        )
                                );
                            }else{
                                array_push($southern_area_number[$southern_area_key_loop], array($key=>$number_amphur_title_key_one[$xaa]));
                            }
                            $xaa++;
                            }
                            $province_southern_nomber_one = $southern_area_key[$xx];
                        
                    }elseif(isset($province_southern_nomber_one) && !isset($province_southern_nomber_two)){
                        if ($province_southern_nomber_one == $southern_area_value) {
                            unset($amphur_point_one);
                            unset($province_one);
                            unset($amphur_title_one);
                            foreach ($arrey_southern_area as $key => $value) {
                                if ($value->province_title == $southern_area_key_loop) {
                                    $province_one[] = $value;
                                }
                            }

                            foreach ($province_one as $key => $value) {
                                $amphur_title_one[] = $value->amphur_title;
                            }
                            
                            $number_amphur_title_one = array_count_values($amphur_title_one);
                            $number_amphur_title_key_one = array_count_values($amphur_title_one);
                            rsort($number_amphur_title_key_one);
                            arsort($number_amphur_title_one);   
                            $xaa = 0;
                            foreach ($number_amphur_title_one as $key => $value) {
                                if (!isset($amphur_point_one)) {
                                    $amphur_point_one = $number_amphur_title_key_one[$xaa];
                                    
                                    $southern_area_number[$southern_area_key_loop] = array(
                                        'no' => 1,
                                        'point' => $southern_area_key[$xx],
                                        array(
                                            $key=> $amphur_point_one
                                            )
                                        );
                                    }else{
                                        array_push($southern_area_number[$southern_area_key_loop], array($key=>$number_amphur_title_key_one[$xaa]));
                                    }
                                    $xaa++;
                                }
                            
                        }else{
                            foreach ($arrey_southern_area as $key => $value) {
                                if ($value->province_title == $southern_area_key_loop) {
                                    $province_two[] = $value;
                                }
                            }

                            foreach ($province_two as $key => $value) {
                                $amphur_title_two[] = $value->amphur_title;
                            }
                            
                            $number_amphur_title_two = array_count_values($amphur_title_two);
                            $number_amphur_title_key_two = array_count_values($amphur_title_two);
                            rsort($number_amphur_title_key_two);
                            arsort($number_amphur_title_two);   
                            $xaa = 0;
                            foreach ($number_amphur_title_two as $key => $value) {
                                if (!isset($amphur_point_two)) {
                                    $amphur_point_two = $number_amphur_title_key_two[$xaa];
                                    
                                    $southern_area_number[$southern_area_key_loop] = array(
                                        'no' => 2,
                                        'point' => $southern_area_key[$xx],
                                        array(
                                            $key=> $amphur_point_two
                                            )
                                        );
                                    }else{
                                        array_push($southern_area_number[$southern_area_key_loop], array($key=>$number_amphur_title_key_two[$xaa]));
                                    }
                                    $xaa++;
                                }
                                $province_southern_nomber_two = $southern_area_key[$xx];
                        }
                    }elseif(isset($province_southern_nomber_two) && !isset($province_southern_nomber_three)){
                        if ($province_southern_nomber_two == $southern_area_value) {
                            unset($amphur_point_two);
                            unset($province_two);
                            unset($amphur_title_two);
                            foreach ($arrey_southern_area as $key => $value) {
                                if ($value->province_title == $southern_area_key_loop) {
                                    $province_two[] = $value;
                                }
                            }

                            foreach ($province_two as $key => $value) {
                                $amphur_title_two[] = $value->amphur_title;
                            }
                            
                            $number_amphur_title_two = array_count_values($amphur_title_two);
                            $number_amphur_title_key_two = array_count_values($amphur_title_two);
                            rsort($number_amphur_title_key_two);
                            arsort($number_amphur_title_two);   
                            $xaa = 0;
                            foreach ($number_amphur_title_two as $key => $value) {
                                if (!isset($amphur_point_two)) {
                                    $amphur_point_two = $number_amphur_title_key_two[$xaa];
                                    
                                    $southern_area_number[$southern_area_key_loop] = array(
                                        'no' => 2,
                                        'point' => $southern_area_key[$xx],
                                        array(
                                            $key=> $amphur_point_two
                                            )
                                        );
                                    }else{
                                        array_push($southern_area_number[$southern_area_key_loop], array($key=>$number_amphur_title_key_two[$xaa]));
                                    }
                                    $xaa++;
                                }
                        }else{
                            foreach ($arrey_southern_area as $key => $value) {
                                if ($value->province_title == $southern_area_key_loop) {
                                    $province_three[] = $value;
                                }
                            }

                            if (isset($province_three)) {
                                
                        
                            foreach ($province_three as $key => $value) {
                                $amphur_title_three[] = $value->amphur_title;
                            }
                            
                            $number_amphur_title_three = array_count_values($amphur_title_three);
                            $number_amphur_title_key_three = array_count_values($amphur_title_three);
                            rsort($number_amphur_title_key_three);
                            arsort($number_amphur_title_three);   
                            $xaa = 0;
                            foreach ($number_amphur_title_three as $key => $value) {
                                if (!isset($amphur_point_three)) {
                                    $amphur_point_three = $number_amphur_title_key_three[$xaa];
                                    
                                    $southern_area_number[$southern_area_key_loop] = array(
                                        'no' => 3,
                                        'point' => $southern_area_key[$xx],
                                        array(
                                            $key=> $amphur_point_three
                                            )
                                        );
                                    }else{
                                        array_push($southern_area_number[$southern_area_key_loop], array($key=>$number_amphur_title_key_three[$xaa]));
                                    }
                                    $xaa++;
                                }
                                $province_southern_nomber_three = $southern_area_key[$xx];
                            }
                        }
                    }elseif(isset($province_southern_nomber_three) && !isset($province_southern_nomber_four)){
                        if ($province_southern_nomber_three == $southern_area_value) {
                            unset($amphur_point_three);
                            unset($province_three);
                            unset($amphur_title_three);
                            foreach ($arrey_southern_area as $key => $value) {
                                if ($value->province_title == $southern_area_key_loop) {
                                    $province_three[] = $value;
                                }
                            }

                            foreach ($province_three as $key => $value) {
                                $amphur_title_three[] = $value->amphur_title;
                            }
                            
                            $number_amphur_title_three = array_count_values($amphur_title_three);
                            $number_amphur_title_key_three = array_count_values($amphur_title_three);
                            rsort($number_amphur_title_key_three);
                            arsort($number_amphur_title_three);   
                            $xaa = 0;
                            foreach ($number_amphur_title_three as $key => $value) {
                                if (!isset($amphur_point_three)) {
                                    $amphur_point_three = $number_amphur_title_key_three[$xaa];
                                    
                                    $southern_area_number[$southern_area_key_loop] = array(
                                        'no' => 3,
                                        'point' => $southern_area_key[$xx],
                                        array(
                                            $key=> $amphur_point_three
                                            )
                                        );
                                    }else{
                                        array_push($southern_area_number[$southern_area_key_loop], array($key=>$number_amphur_title_key_three[$xaa]));
                                    }
                                    $xaa++;
                                }
                        }else {
                            foreach ($arrey_southern_area as $key => $value) {
                                if ($value->province_title == $southern_area_key_loop) {
                                    $province_four[] = $value;
                                }
                            }

                            if (isset($province_four)) {
                           
                            foreach ($province_four as $key => $value) {
                                $amphur_title_four[] = $value->amphur_title;
                            }
                            
                            $number_amphur_title_four = array_count_values($amphur_title_four);
                            $number_amphur_title_key_four = array_count_values($amphur_title_four);
                            rsort($number_amphur_title_key_four);
                            arsort($number_amphur_title_four);
                            $xaa = 0;
                            foreach ($number_amphur_title_four as $key => $value) {
                                if (!isset($amphur_point_four)) {
                                    $amphur_point_four = $number_amphur_title_key_four[$xaa];
                                    
                                    $southern_area_number[$southern_area_key_loop] = array(
                                        'no' => 4,
                                        'point' => $southern_area_key[$xx],
                                        array(
                                            $key=> $amphur_point_four
                                            )
                                        );
                                    }else{
                                        array_push($southern_area_number[$southern_area_key_loop], array($key=>$number_amphur_title_key_four[$xaa]));
                                    }
                                    $xaa++;
                                }
                                $province_southern_nomber_four = $southern_area_key[$xx];
                            }
                        }
                    }elseif(isset($province_southern_nomber_four) && !isset($province_southern_nomber_five)){
                        if ($province_southern_nomber_four == $southern_area_value) {
                            unset($amphur_point_four);
                            unset($province_four);
                            unset($amphur_title_four);
                            foreach ($arrey_southern_area as $key => $value) {
                                if ($value->province_title == $southern_area_key_loop) {
                                    $province_four[] = $value;
                                }
                            }

                            foreach ($province_four as $key => $value) {
                                $amphur_title_four[] = $value->amphur_title;
                            }
                            
                            $number_amphur_title_four = array_count_values($amphur_title_four);
                            $number_amphur_title_key_four = array_count_values($amphur_title_four);
                            rsort($number_amphur_title_key_four);
                            arsort($number_amphur_title_four);   
                            $xaa = 0;
                            foreach ($number_amphur_title_four as $key => $value) {
                                if (!isset($amphur_point_four)) {
                                    $amphur_point_four = $number_amphur_title_key_four[$xaa];
                                    
                                    $southern_area_number[$southern_area_key_loop] = array(
                                        'no' => 4,
                                        'point' => $southern_area_key[$xx],
                                        array(
                                            $key=> $amphur_point_four
                                            )
                                        );
                                    }else{
                                        array_push($southern_area_number[$southern_area_key_loop], array($key=>$number_amphur_title_key_four[$xaa]));
                                    }
                                    $xaa++;
                                }
                        }else{
                            foreach ($arrey_southern_area as $key => $value) {
                                if ($value->province_title == $southern_area_key_loop) {
                                    $province_five[] = $value;
                                }
                            }

                            foreach ($province_five as $key => $value) {
                                $amphur_title_five[] = $value->amphur_title;
                            }
                            
                            $number_amphur_title_five = array_count_values($amphur_title_five);
                            $number_amphur_title_key_five = array_count_values($amphur_title_five);
                            rsort($number_amphur_title_key_five);
                            arsort($number_amphur_title_five);
                            $xaa = 0;
                            foreach ($number_amphur_title_five as $key => $value) {
                                if (!isset($amphur_point_five)) {
                                    $amphur_point_five = $number_amphur_title_key_five[$xaa];
                                    
                                    $southern_area_number[$southern_area_key_loop] = array(
                                        'no' => 5,
                                        'point' => $southern_area_key[$xx],
                                        array(
                                            $key=> $amphur_point_five
                                            )
                                        );
                                    }else{
                                        array_push($southern_area_number[$southern_area_key_loop], array($key=>$number_amphur_title_key_five[$xaa]));
                                    }
                                    $xaa++;
                                }
                                $province_southern_nomber_five = $southern_area_key[$xx];
                        }
                    }
                $xx++; 
            }
                // echo '<pre>';
                // echo 'เหนื่อยน';
                // print_r($upper_north_area_number);
                // echo 'เหนื่อล่าง';
                // print_r($lower_north_area_number);
                // echo 'กลาง';
                // print_r($central_area_number);
                // echo 'อีสานล่าง';
                // print_r($upper_northeast_area_number);
                // echo 'อีสานบน';
                // print_r($lower_northeast_area_number);
                // echo 'ตะวันออก';
                // print_r($eastern_area_number);
                // echo 'ใต้';
                // print_r($southern_area_number);

                $percentage_sueecss = ($reques_sueecss/$all_reques)*100;
                $percentage_progress = ($reques_progress/$all_reques)*100;
                ?>
                    <div class="text-right">
                    <a href="daily.php" class="btn btn-info btn-sm" target="_blank">ดูการขอรับบริการประจำวัน</a>
                    </div>
                    <div class="row mt-4">
                        <div class="col-12 col-md-6">
                            <h5 class="text-center"><?php echo 'แบ่งตามสถานะการดำเนินการ' ?></h5>
                            <canvas id="my-chart-reques"></canvas>
                            <div class="mt-4">
                                <table class="table table-hover table-borderless table-responsive">
                                    <tbody>
                                        <tr>
                                            <td style="width: 290px;">
                                                <p><?php echo 'จำนวนการขอรับการขอฝนหลวงทั้งหมด' ; ?></p>
                                            </td>
                                            <td>
                                                <p><?php echo '<strong class="number_all_reques">' .number_format($all_reques). '</strong> รายการ' ; ?>
                                                </p>
                                            </td>
                                            <td></td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <p><?php echo 'ดำเนินการแล้ว' ; ?></p>
                                            </td>
                                            <td>
                                                <p class="text-right">
                                                    <?php echo '<strong class="number_reques_sueecss">' .number_format($reques_sueecss). '</strong> รายการ' ; ?>
                                                </p>
                                            </td>
                                            <td style="width: 80px;">
                                                <p><?php echo number_format($percentage_sueecss,2).'%' ; ?></p>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <p><?php echo 'อยู่ระหว่างดำเนินการ' ; ?></p>
                                            </td>
                                            <td>
                                                <p class="text-right">
                                                    <?php echo '<strong class="number_reques_progress">' .$reques_progress. '</strong> รายการ' ; ?>
                                                </p>
                                            </td>
                                            <td>
                                                <p><?php echo number_format($percentage_progress,2).'%' ; ?></p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="row">
                                <div class="col-12 col-md-12">
                                    <div class="text-center">
                                        <h5><?php echo 'แบ่งตามศูนย์ปฏิบัติการ' ?></h5>
                                        <canvas id="my-chart-province"></canvas>
                                    </div>
                                    <div class="mt-4">
                                        <table class="table table-hover table-borderless table-responsive">
                                            <tbody>
                                                <tr>
                                                    <td style="width: 280px;">
                                                        <p>ศูนย์ปฏิบัติการฝนหลวงภาคเหนือตอนบน</p>
                                                    </td>
                                                    <td>
                                                        <p class="text-right"><strong
                                                                class="upper_north_area"><?php echo number_format(count($arrey_upper_north_area)) ?></strong><span>รายการ</span>
                                                        </p>
                                                    </td>
                                                    <td>
                                                        <p><a href="#" data-toggle="modal"
                                                                data-target="#arrey_upper_north_area_modal">รายละเอียด</a>
                                                        </p>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <p>ศูนย์ปฏิบัติการฝนหลวงภาคเหนือตอนล่าง</p>
                                                    </td>
                                                    <td>
                                                        <p class="text-right"><strong
                                                                class="lower_north_area"><?php echo number_format(count($arrey_lower_north_area)) ?></strong><span>รายการ</span>
                                                        </p>
                                                    </td>
                                                    <td>
                                                        <p><a href="#" data-toggle="modal"
                                                                data-target="#lower_north_area_modal"
                                                                class="">รายละเอียด</a></p>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <p>ศูนย์ปฏิบัติการฝนหลวงภาคกลาง</p>
                                                    </td>
                                                    <td>
                                                        <p class="text-right"><strong
                                                                class="central_area"><?php echo number_format(count($arrey_central_area)) ?></strong><span>รายการ</span>
                                                        </p>
                                                    </td>
                                                    <td>
                                                        <p><a href="#" data-toggle="modal"
                                                                data-target="#central_area_modal"
                                                                class="">รายละเอียด</a></p>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <p>ศูนย์ปฏิบัติการฝนหลวงภาคตะวันออกเฉียงเหนือตอนบน</p>
                                                    </td>
                                                    <td>
                                                        <p class="text-right"><strong
                                                                class="upper_northeast_area"><?php echo number_format(count($arrey_upper_northeast_area)) ?></strong><span>รายการ</span>
                                                        </p>
                                                    </td>
                                                    <td>
                                                        <p><a href="#" data-toggle="modal"
                                                                data-target="#upper_northeast_area_modal"
                                                                class="">รายละเอียด</a></p>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <p>ศูนย์ปฏิบัติการฝนหลวงภาคตะวันออกเฉียงเหนือตอนล่าง</p>
                                                    </td>
                                                    <td>
                                                        <p class="text-right"><strong
                                                                class="lower_northeast_area"><?php echo number_format(count($arrey_lower_northeast_area)) ?></strong><span>รายการ</span>
                                                        </p>
                                                    </td>
                                                    <td>
                                                        <p><a href="#" data-toggle="modal"
                                                                data-target="#lower_northeast_area_modal">รายละเอียด</a>
                                                        </p>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <p>ศูนย์ปฏิบัติการฝนหลวงภาคตะวันออก</p>
                                                    </td>
                                                    <td>
                                                        <p class="text-right"><strong
                                                                class="eastern_area"><?php echo number_format(count($arrey_eastern_area)) ?></strong><span>รายการ</span>
                                                        </p>
                                                    </td>
                                                    <td>
                                                        <p><a href="#" data-toggle="modal"
                                                                data-target="#eastern_area_modal">รายละเอียด</a></p>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <p>ศูนย์ปฏิบัติการฝนหลวงภาคใต้</p>
                                                    </td>
                                                    <td>
                                                        <p class="text-right"><strong
                                                                class="southern_area"><?php echo number_format(count($arrey_southern_area)) ?></strong><span>รายการ</span>
                                                        </p>
                                                    </td>
                                                    <td>
                                                        <p><a href="#" data-toggle="modal"
                                                                data-target="#southern_area_modal">รายละเอียด</a></p>
                                                    </td>
                                                </tr>


                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <h4 class="text-center mt-4">แบ่งตามเดือน</h4>
                    <canvas id="chart-month"></canvas>
                </div>
            </div>
        </div>



        <!-- Modal -->
        <div class="modal fade" id="arrey_upper_north_area_modal" tabindex="-1" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-scrollable">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">ศูนย์ปฏิบัติการฝนหลวงภาคเหนือตอนบน</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12 col-md-12">
                                <h4 class="text-center">จังหวัดที่มีการขอรับบริการฝนหลวงมากที่สุด 5 ลำดับแรก</h4>

                            </div>
                            <?php
                             foreach ($upper_north_area_number as $key => $value) { 
                                 if ($value['no'] == 1 && !isset($font_no_one)) { ?>
                            <div class="col-12 col-md-12">
                                <h5>อันดับที่ 1 </h5>
                            </div>
                            <div class="col-12 col-md-12">
                                <?php
                                     $x = 0;
                                     echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                     echo '<div class="row">';
                                     for ($i=2; $i < count($upper_north_area_number[$key]); $i++) { 
                                        foreach ($upper_north_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                            echo '<div class="col-12 col-md-6">';
                                            echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                            // echo $amphur_value.'<br>';
                                            echo '</div>';
                                        }
                                        $x++;
                                     }
                                     echo '</div>';
                                     echo '</div>';
                                     $font_no_one = $value['no'];
                                   
                                 }elseif($value['no'] == 1 && isset($font_no_one) && $font_no_one == 1){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 1 (ร่วม)</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($upper_north_area_number[$key]); $i++) { 
                                       foreach ($upper_north_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                 }elseif($value['no'] == 2 && !isset($font_no_two)){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 2</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($upper_north_area_number[$key]); $i++) { 
                                       foreach ($upper_north_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                    $font_no_two = $value['no'];
                                 }elseif($value['no'] == 2 && isset($font_no_two) && $font_no_two == 2){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 2 (ร่วม)</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($upper_north_area_number[$key]); $i++) { 
                                       foreach ($upper_north_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                 }elseif($value['no'] == 3 && !isset($font_no_three)){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 3</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($upper_north_area_number[$key]); $i++) { 
                                       foreach ($upper_north_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                    $font_no_three = $value['no'];
                                 }elseif($value['no'] == 3 && isset($font_no_three) && $font_no_three == 3){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 3 (ร่วม)</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($upper_north_area_number[$key]); $i++) { 
                                       foreach ($upper_north_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                 }elseif($value['no'] == 4 && !isset($font_no_four)){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 4</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($upper_north_area_number[$key]); $i++) { 
                                       foreach ($upper_north_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                    $font_no_four = $value['no'];
                                 }elseif($value['no'] == 4 && isset($font_no_four) && $font_no_four == 4){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 4 (ร่วม)</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($upper_north_area_number[$key]); $i++) { 
                                       foreach ($upper_north_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                 }elseif($value['no'] == 5  && !isset($font_no_five)){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 5</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($upper_north_area_number[$key]); $i++) { 
                                       foreach ($upper_north_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                    $font_no_five = $value['no'];
                                 }elseif($value['no'] == 5 && isset($font_no_five) && $font_no_five == 5){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 5 (ร่วม)</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($upper_north_area_number[$key]); $i++) { 
                                       foreach ($upper_north_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                 }
                             }
                             unset($font_no_one);
                             unset($font_no_two);
                             unset($font_no_three);
                             unset($font_no_four);
                             unset($font_no_five);
                            ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="lower_north_area_modal" tabindex="-1" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-scrollable">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">ศูนย์ปฏิบัติการฝนหลวงภาคเหนือตอนล่าง</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12 col-md-12">
                                    <h4 class="text-center">จังหวัดที่มีการขอรับบริการฝนหลวงมากที่สุด 5 ลำดับแรก</h4>

                                </div>
                                <?php
                             foreach ($lower_north_area_number as $key => $value) { 
                                 if ($value['no'] == 1 && !isset($font_no_one)) { ?>
                                <div class="col-12 col-md-12">
                                    <h5>อันดับที่ 1 </h5>
                                </div>
                                <div class="col-12 col-md-12">
                                    <?php
                                    // echo '<pre>';
                                        // print_r($arrey_lower_north_area);

                                     $x = 0;
                                     echo '<pre>'. $key .' จำนวน ' . $value['point'] . ' รายการ </pre>'; 
                                     echo '<div class="row">';
                                     for ($i=2; $i < count($lower_north_area_number[$key]); $i++) { 
                                        foreach ($lower_north_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                            echo '<div class="col-12 col-md-6">';
                                            echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                            // echo $amphur_value.'<br>';
                                            echo '</div>';
                                        }
                                        $x++;
                                     }
                                     echo '</div>';
                                     echo '</div>';
                                     $font_no_one = $value['no'];
                                   
                                 }elseif($value['no'] == 1 && isset($font_no_one) && $font_no_one == 1){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 1 (ร่วม)</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($lower_north_area_number[$key]); $i++) { 
                                       foreach ($lower_north_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                 }elseif($value['no'] == 2 && !isset($font_no_two)){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 2</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($lower_north_area_number[$key]); $i++) { 
                                       foreach ($lower_north_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                    $font_no_two = $value['no'];
                                 }elseif($value['no'] == 2 && isset($font_no_two) && $font_no_two == 2){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 2 (ร่วม)</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($lower_north_area_number[$key]); $i++) { 
                                       foreach ($lower_north_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                 }elseif($value['no'] == 3 && !isset($font_no_three)){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 3</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($lower_north_area_number[$key]); $i++) { 
                                       foreach ($lower_north_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                    $font_no_three = $value['no'];
                                 }elseif($value['no'] == 3 && isset($font_no_three) && $font_no_three == 3){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 3 (ร่วม)</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($lower_north_area_number[$key]); $i++) { 
                                       foreach ($lower_north_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                 }elseif($value['no'] == 4 && !isset($font_no_four)){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 4</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($lower_north_area_number[$key]); $i++) { 
                                       foreach ($lower_north_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                    $font_no_four = $value['no'];
                                 }elseif($value['no'] == 4 && isset($font_no_four) && $font_no_four == 4){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 4 (ร่วม)</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($lower_north_area_number[$key]); $i++) { 
                                       foreach ($lower_north_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                 }elseif($value['no'] == 5  && !isset($font_no_five)){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 5</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($lower_north_area_number[$key]); $i++) { 
                                       foreach ($lower_north_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                    $font_no_five = $value['no'];
                                 }elseif($value['no'] == 5 && isset($font_no_five) && $font_no_five == 5){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 5 (ร่วม)</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($lower_north_area_number[$key]); $i++) { 
                                       foreach ($lower_north_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                 }
                             }
                             unset($font_no_one);
                             unset($font_no_two);
                             unset($font_no_three);
                             unset($font_no_four);
                             unset($font_no_five);
                            ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="central_area_modal" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-scrollable">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">ศูนย์ปฏิบัติการฝนหลวงภาคกลาง</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12 col-md-12">
                                    <h4 class="text-center">จังหวัดที่มีการขอรับบริการฝนหลวงมากที่สุด 5 ลำดับแรก</h4>

                                </div>
                                <?php
                             foreach ($central_area_number as $key => $value) { 
                                 if ($value['no'] == 1 && !isset($font_no_one)) { ?>
                                <div class="col-12 col-md-12">
                                    <h5>อันดับที่ 1 </h5>
                                </div>
                                <div class="col-12 col-md-12">
                                    <?php
                                     $x = 0;
                                     echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                     echo '<div class="row">';
                                     for ($i=2; $i < count($central_area_number[$key]); $i++) { 
                                        foreach ($central_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                            echo '<div class="col-12 col-md-6">';
                                            echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                            // echo $amphur_value.'<br>';
                                            echo '</div>';
                                        }
                                        $x++;
                                     }
                                     echo '</div>';
                                     echo '</div>';
                                     $font_no_one = $value['no'];
                                   
                                 }elseif($value['no'] == 1 && isset($font_no_one) && $font_no_one == 1){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 1 (ร่วม)</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($central_area_number[$key]); $i++) { 
                                       foreach ($central_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                 }elseif($value['no'] == 2 && !isset($font_no_two)){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 2</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($central_area_number[$key]); $i++) { 
                                       foreach ($central_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                    $font_no_two = $value['no'];
                                 }elseif($value['no'] == 2 && isset($font_no_two) && $font_no_two == 2){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 2 (ร่วม)</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($central_area_number[$key]); $i++) { 
                                       foreach ($central_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                 }elseif($value['no'] == 3 && !isset($font_no_three)){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 3</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($central_area_number[$key]); $i++) { 
                                       foreach ($central_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                    $font_no_three = $value['no'];
                                 }elseif($value['no'] == 3 && isset($font_no_three) && $font_no_three == 3){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 3 (ร่วม)</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($central_area_number[$key]); $i++) { 
                                       foreach ($central_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                 }elseif($value['no'] == 4 && !isset($font_no_four)){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 4</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($central_area_number[$key]); $i++) { 
                                       foreach ($central_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                    $font_no_four = $value['no'];
                                 }elseif($value['no'] == 4 && isset($font_no_four) && $font_no_four == 4){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 4 (ร่วม)</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($central_area_number[$key]); $i++) { 
                                       foreach ($central_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                 }elseif($value['no'] == 5  && !isset($font_no_five)){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 5</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($central_area_number[$key]); $i++) { 
                                       foreach ($central_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                    $font_no_five = $value['no'];
                                 }elseif($value['no'] == 5 && isset($font_no_five) && $font_no_five == 5){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 5 (ร่วม)</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($central_area_number[$key]); $i++) { 
                                       foreach ($central_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                 }
                             }
                             unset($font_no_one);
                             unset($font_no_two);
                             unset($font_no_three);
                             unset($font_no_four);
                             unset($font_no_five);
                            ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="modal fade" id="upper_northeast_area_modal" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-scrollable">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">ศูนย์ปฏิบัติการฝนหลวงภาคตะวันออกเฉียงเหนือตอนบน</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12 col-md-12">
                                    <h4 class="text-center">จังหวัดที่มีการขอรับบริการฝนหลวงมากที่สุด 5 ลำดับแรก</h4>

                                </div>
                                <?php
                             foreach ($upper_northeast_area_number as $key => $value) { 
                                 if ($value['no'] == 1 && !isset($font_no_one)) { ?>
                                <div class="col-12 col-md-12">
                                    <h5>อันดับที่ 1 </h5>
                                </div>
                                <div class="col-12 col-md-12">
                                    <?php
                                     $x = 0;
                                     echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                     echo '<div class="row">';
                                     for ($i=2; $i < count($upper_northeast_area_number[$key]); $i++) { 
                                        foreach ($upper_northeast_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                            echo '<div class="col-12 col-md-6">';
                                            echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                            // echo $amphur_value.'<br>';
                                            echo '</div>';
                                        }
                                        $x++;
                                     }
                                     echo '</div>';
                                     echo '</div>';
                                     $font_no_one = $value['no'];
                                   
                                 }elseif($value['no'] == 1 && isset($font_no_one) && $font_no_one == 1){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 1 (ร่วม)</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($upper_northeast_area_number[$key]); $i++) { 
                                       foreach ($upper_northeast_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                 }elseif($value['no'] == 2 && !isset($font_no_two)){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 2</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($upper_northeast_area_number[$key]); $i++) { 
                                       foreach ($upper_northeast_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                    $font_no_two = $value['no'];
                                 }elseif($value['no'] == 2 && isset($font_no_two) && $font_no_two == 2){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 2 (ร่วม)</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($upper_northeast_area_number[$key]); $i++) { 
                                       foreach ($upper_northeast_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                 }elseif($value['no'] == 3 && !isset($font_no_three)){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 3</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($upper_northeast_area_number[$key]); $i++) { 
                                       foreach ($upper_northeast_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                    $font_no_three = $value['no'];
                                 }elseif($value['no'] == 3 && isset($font_no_three) && $font_no_three == 3){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 3 (ร่วม)</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($upper_northeast_area_number[$key]); $i++) { 
                                       foreach ($upper_northeast_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                 }elseif($value['no'] == 4 && !isset($font_no_four)){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 4</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($upper_northeast_area_number[$key]); $i++) { 
                                       foreach ($upper_northeast_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                    $font_no_four = $value['no'];
                                 }elseif($value['no'] == 4 && isset($font_no_four) && $font_no_four == 4){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 4 (ร่วม)</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($upper_northeast_area_number[$key]); $i++) { 
                                       foreach ($upper_northeast_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                 }elseif($value['no'] == 5  && !isset($font_no_five)){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 5</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($upper_northeast_area_number[$key]); $i++) { 
                                       foreach ($upper_northeast_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                    $font_no_five = $value['no'];
                                 }elseif($value['no'] == 5 && isset($font_no_five) && $font_no_five == 5){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 5 (ร่วม)</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($upper_northeast_area_number[$key]); $i++) { 
                                       foreach ($upper_northeast_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                 }
                             }
                             unset($font_no_one);
                             unset($font_no_two);
                             unset($font_no_three);
                             unset($font_no_four);
                             unset($font_no_five);
                            ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="lower_northeast_area_modal" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-scrollable">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">ศูนย์ปฏิบัติการฝนหลวงภาคตะวันออกเฉียงเหนือตอนล่าง</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12 col-md-12">
                                    <h4 class="text-center">จังหวัดที่มีการขอรับบริการฝนหลวงมากที่สุด 5 ลำดับแรก</h4>

                                </div>
                                <?php
                             foreach ($lower_northeast_area_number as $key => $value) { 
                                 if ($value['no'] == 1 && !isset($font_no_one)) { ?>
                                <div class="col-12 col-md-12">
                                    <h5>อันดับที่ 1 </h5>
                                </div>
                                <div class="col-12 col-md-12">
                                    <?php
                                     $x = 0;
                                     echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                     echo '<div class="row">';
                                     for ($i=2; $i < count($lower_northeast_area_number[$key]); $i++) { 
                                        foreach ($lower_northeast_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                            echo '<div class="col-12 col-md-6">';
                                            echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                            // echo $amphur_value.'<br>';
                                            echo '</div>';
                                        }
                                        $x++;
                                     }
                                     echo '</div>';
                                     echo '</div>';
                                     $font_no_one = $value['no'];
                                   
                                 }elseif($value['no'] == 1 && isset($font_no_one) && $font_no_one == 1){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 1 (ร่วม)</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($lower_northeast_area_number[$key]); $i++) { 
                                       foreach ($lower_northeast_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                 }elseif($value['no'] == 2 && !isset($font_no_two)){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 2</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($lower_northeast_area_number[$key]); $i++) { 
                                       foreach ($lower_northeast_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                    $font_no_two = $value['no'];
                                 }elseif($value['no'] == 2 && isset($font_no_two) && $font_no_two == 2){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 2 (ร่วม)</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($lower_northeast_area_number[$key]); $i++) { 
                                       foreach ($lower_northeast_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                 }elseif($value['no'] == 3 && !isset($font_no_three)){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 3</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($lower_northeast_area_number[$key]); $i++) { 
                                       foreach ($lower_northeast_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                    $font_no_three = $value['no'];
                                 }elseif($value['no'] == 3 && isset($font_no_three) && $font_no_three == 3){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 3 (ร่วม)</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($lower_northeast_area_number[$key]); $i++) { 
                                       foreach ($lower_northeast_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                 }elseif($value['no'] == 4 && !isset($font_no_four)){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 4</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($lower_northeast_area_number[$key]); $i++) { 
                                       foreach ($lower_northeast_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                    $font_no_four = $value['no'];
                                 }elseif($value['no'] == 4 && isset($font_no_four) && $font_no_four == 4){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 4 (ร่วม)</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($lower_northeast_area_number[$key]); $i++) { 
                                       foreach ($lower_northeast_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                 }elseif($value['no'] == 5  && !isset($font_no_five)){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 5</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($lower_northeast_area_number[$key]); $i++) { 
                                       foreach ($lower_northeast_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                    $font_no_five = $value['no'];
                                 }elseif($value['no'] == 5 && isset($font_no_five) && $font_no_five == 5){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 5 (ร่วม)</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($lower_northeast_area_number[$key]); $i++) { 
                                       foreach ($lower_northeast_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                 }
                             }
                             unset($font_no_one);
                             unset($font_no_two);
                             unset($font_no_three);
                             unset($font_no_four);
                             unset($font_no_five);
                            ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="eastern_area_modal" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-scrollable">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">ศูนย์ปฏิบัติการฝนหลวงภาคตะวันออก</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12 col-md-12">
                                    <h4 class="text-center">จังหวัดที่มีการขอรับบริการฝนหลวงมากที่สุด 5 ลำดับแรก</h4>

                                </div>
                                <?php
                             foreach ($eastern_area_number as $key => $value) { 
                                 if ($value['no'] == 1 && !isset($font_no_one)) { ?>
                                <div class="col-12 col-md-12">
                                    <h5>อันดับที่ 1 </h5>
                                </div>
                                <div class="col-12 col-md-12">
                                    <?php
                                     $x = 0;
                                     echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                     echo '<div class="row">';
                                     for ($i=2; $i < count($eastern_area_number[$key]); $i++) { 
                                        foreach ($eastern_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                            echo '<div class="col-12 col-md-6">';
                                            echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                            // echo $amphur_value.'<br>';
                                            echo '</div>';
                                        }
                                        $x++;
                                     }
                                     echo '</div>';
                                     echo '</div>';
                                     $font_no_one = $value['no'];
                                   
                                 }elseif($value['no'] == 1 && isset($font_no_one) && $font_no_one == 1){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 1 (ร่วม)</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($eastern_area_number[$key]); $i++) { 
                                       foreach ($eastern_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                 }elseif($value['no'] == 2 && !isset($font_no_two)){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 2</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($eastern_area_number[$key]); $i++) { 
                                       foreach ($eastern_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                    $font_no_two = $value['no'];
                                 }elseif($value['no'] == 2 && isset($font_no_two) && $font_no_two == 2){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 2 (ร่วม)</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($eastern_area_number[$key]); $i++) { 
                                       foreach ($eastern_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                 }elseif($value['no'] == 3 && !isset($font_no_three)){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 3</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($eastern_area_number[$key]); $i++) { 
                                       foreach ($eastern_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                    $font_no_three = $value['no'];
                                 }elseif($value['no'] == 3 && isset($font_no_three) && $font_no_three == 3){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 3 (ร่วม)</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($eastern_area_number[$key]); $i++) { 
                                       foreach ($eastern_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                 }elseif($value['no'] == 4 && !isset($font_no_four)){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 4</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($eastern_area_number[$key]); $i++) { 
                                       foreach ($eastern_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                    $font_no_four = $value['no'];
                                 }elseif($value['no'] == 4 && isset($font_no_four) && $font_no_four == 4){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 4 (ร่วม)</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($eastern_area_number[$key]); $i++) { 
                                       foreach ($eastern_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                 }elseif($value['no'] == 5  && !isset($font_no_five)){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 5</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($eastern_area_number[$key]); $i++) { 
                                       foreach ($eastern_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                    $font_no_five = $value['no'];
                                 }elseif($value['no'] == 5 && isset($font_no_five) && $font_no_five == 5){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 5 (ร่วม)</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($eastern_area_number[$key]); $i++) { 
                                       foreach ($eastern_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                 }
                             }
                             unset($font_no_one);
                             unset($font_no_two);
                             unset($font_no_three);
                             unset($font_no_four);
                             unset($font_no_five);
                            ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="southern_area_modal" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-scrollable">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">ศูนย์ปฏิบัติการฝนหลวงภาคใต้</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12 col-md-12">
                                    <h4 class="text-center">จังหวัดที่มีการขอรับบริการฝนหลวงมากที่สุด 5 ลำดับแรก</h4>
                                </div>
                                <?php
                             foreach ($southern_area_number as $key => $value) { 
                                 if ($value['no'] == 1 && !isset($font_no_one)) { ?>
                                <div class="col-12 col-md-12">
                                    <h5>อันดับที่ 1 </h5>
                                </div>
                                <div class="col-12 col-md-12">
                                    <?php
                                     $x = 0;
                                     echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                     echo '<div class="row">';
                                     for ($i=2; $i < count($southern_area_number[$key]); $i++) { 
                                        foreach ($southern_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                            echo '<div class="col-12 col-md-6">';
                                            echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                            // echo $amphur_value.'<br>';
                                            echo '</div>';
                                        }
                                        $x++;
                                     }
                                     echo '</div>';
                                     echo '</div>';
                                     $font_no_one = $value['no'];
                                   
                                 }elseif($value['no'] == 1 && isset($font_no_one) && $font_no_one == 1){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 1 (ร่วม)</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($southern_area_number[$key]); $i++) { 
                                       foreach ($southern_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                 }elseif($value['no'] == 2 && !isset($font_no_two)){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 2</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($southern_area_number[$key]); $i++) { 
                                       foreach ($southern_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                    $font_no_two = $value['no'];
                                 }elseif($value['no'] == 2 && isset($font_no_two) && $font_no_two == 2){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 2 (ร่วม)</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($southern_area_number[$key]); $i++) { 
                                       foreach ($southern_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                 }elseif($value['no'] == 3 && !isset($font_no_three)){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 3</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($southern_area_number[$key]); $i++) { 
                                       foreach ($southern_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                    $font_no_three = $value['no'];
                                 }elseif($value['no'] == 3 && isset($font_no_three) && $font_no_three == 3){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 3 (ร่วม)</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($southern_area_number[$key]); $i++) { 
                                       foreach ($southern_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                 }elseif($value['no'] == 4 && !isset($font_no_four)){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 4</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($southern_area_number[$key]); $i++) { 
                                       foreach ($southern_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                    $font_no_four = $value['no'];
                                 }elseif($value['no'] == 4 && isset($font_no_four) && $font_no_four == 4){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 4 (ร่วม)</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($southern_area_number[$key]); $i++) { 
                                       foreach ($southern_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                 }elseif($value['no'] == 5  && !isset($font_no_five)){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 5</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($southern_area_number[$key]); $i++) { 
                                       foreach ($southern_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                    $font_no_five = $value['no'];
                                 }elseif($value['no'] == 5 && isset($font_no_five) && $font_no_five == 5){
                                    echo '<div class="col-12 col-md-12">';
                                    echo '<h5>อันดับที่ 5 (ร่วม)</h5>';
                                    echo '</div>';
                                    echo '<div class="col-12 col-md-12">';
                                    $x = 0;
                                    echo '<p>'. $key .' จำนวน ' . $value['point'] . ' รายการ </p>'; 
                                    echo '<div class="row">';
                                    for ($i=2; $i < count($southern_area_number[$key]); $i++) { 
                                       foreach ($eastern_area_number[$key][$x] as $amphur_key => $amphur_value) {
                                           echo '<div class="col-12 col-md-6">';
                                           echo '<p>'.($x + 1). '. ' . $amphur_key . ' <span class="float-right mr-3"> ' . $amphur_value . ' รายการ </span> </p> <hr>';
                                           echo '</div>';
                                       }
                                       $x++;
                                    }
                                    echo '</div>';
                                    echo '</div>';
                                 }
                             }
                             unset($font_no_one);
                             unset($font_no_two);
                             unset($font_no_three);
                             unset($font_no_four);
                             unset($font_no_five);
                            ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Modal -->
            <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js"></script>

            <script>
                var ctx = document.getElementById('my-chart-reques');
                var province = document.getElementById('my-chart-province');
                var chart_month = document.getElementById('chart-month');

                var january = $('#january').val()
                var february = $('#february').val()
                var march = $('#march').val()
                var april = $('#april').val()
                var may = $('#may').val()
                var june = $('#june').val()
                var july = $('#july').val()
                var august = $('#august').val()
                var september = $('#september').val()
                var october = $('#october').val()
                var november = $('#november').val()
                var december = $('#december').val()

                var january_upper_north_area = $('#january_upper_north_area').val()
                var february_upper_north_area = $('#february_upper_north_area').val()
                var march_upper_north_area = $('#march_upper_north_area').val()
                var april_upper_north_area = $('#april_upper_north_area').val()
                var may_upper_north_area = $('#may_upper_north_area').val()
                var june_upper_north_area = $('#june_upper_north_area').val()
                var july_upper_north_area = $('#july_upper_north_area').val()
                var august_upper_north_area = $('#august_upper_north_area').val()
                var september_upper_north_area = $('#september_upper_north_area').val()
                var october_upper_north_area = $('#october_upper_north_area').val()
                var november_upper_north_area = $('#november_upper_north_area').val()
                var december_upper_north_area = $('#december_upper_north_area').val()

                var january_lower_north_area = $('#january_lower_north_area').val()
                var february_lower_north_area = $('#february_lower_north_area').val()
                var march_lower_north_area = $('#march_lower_north_area').val()
                var april_lower_north_area = $('#april_lower_north_area').val()
                var may_lower_north_area = $('#may_lower_north_area').val()
                var june_lower_north_area = $('#june_lower_north_area').val()
                var july_lower_north_area = $('#july_lower_north_area').val()
                var august_lower_north_area = $('#august_lower_north_area').val()
                var september_lower_north_area = $('#september_lower_north_area').val()
                var october_lower_north_area = $('#october_lower_north_area').val()
                var november_lower_north_area = $('#november_lower_north_area').val()
                var december_lower_north_area = $('#december_lower_north_area').val()

                var january_central_area = $('#january_central_area').val()
                var february_central_area = $('#february_central_area').val()
                var march_central_area = $('#march_central_area').val()
                var april_central_area = $('#april_central_area').val()
                var may_central_area = $('#may_central_area').val()
                var june_central_area = $('#june_central_area').val()
                var july_central_area = $('#july_central_area').val()
                var august_central_area = $('#august_central_area').val()
                var september_central_area = $('#september_central_area').val()
                var october_central_area = $('#october_central_area').val()
                var november_central_area = $('#november_central_area').val()
                var december_central_area = $('#december_central_area').val()

                var january_upper_northeast_area = $('#january_upper_northeast_area').val()
                var february_upper_northeast_area = $('#february_upper_northeast_area').val()
                var march_upper_northeast_area = $('#march_upper_northeast_area').val()
                var april_upper_northeast_area = $('#april_upper_northeast_area').val()
                var may_upper_northeast_area = $('#may_upper_northeast_area').val()
                var june_upper_northeast_area = $('#june_upper_northeast_area').val()
                var july_upper_northeast_area = $('#july_upper_northeast_area').val()
                var august_upper_northeast_area = $('#august_upper_northeast_area').val()
                var september_upper_northeast_area = $('#september_upper_northeast_area').val()
                var october_upper_northeast_area = $('#october_upper_northeast_area').val()
                var november_upper_northeast_area = $('#november_upper_northeast_area').val()
                var december_upper_northeast_area = $('#december_upper_northeast_area').val()

                var january_lower_northeast_area = $('#january_lower_northeast_area').val()
                var february_lower_northeast_area = $('#february_lower_northeast_area').val()
                var march_lower_northeast_area = $('#march_lower_northeast_area').val()
                var april_lower_northeast_area = $('#april_lower_northeast_area').val()
                var may_lower_northeast_area = $('#may_lower_northeast_area').val()
                var june_lower_northeast_area = $('#june_lower_northeast_area').val()
                var july_lower_northeast_area = $('#july_lower_northeast_area').val()
                var august_lower_northeast_area = $('#august_lower_northeast_area').val()
                var september_lower_northeast_area = $('#september_lower_northeast_area').val()
                var october_lower_northeast_area = $('#october_lower_northeast_area').val()
                var november_lower_northeast_area = $('#november_lower_northeast_area').val()
                var december_lower_northeast_area = $('#december_lower_northeast_area').val()

                var january_eastern_area = $('#january_eastern_area').val()
                var february_eastern_area = $('#february_eastern_area').val()
                var march_eastern_area = $('#march_eastern_area').val()
                var april_eastern_area = $('#april_eastern_area').val()
                var may_eastern_area = $('#may_eastern_area').val()
                var june_eastern_area = $('#june_eastern_area').val()
                var july_eastern_area = $('#july_eastern_area').val()
                var august_eastern_area = $('#august_eastern_area').val()
                var september_eastern_area = $('#september_eastern_area').val()
                var october_eastern_area = $('#october_eastern_area').val()
                var november_eastern_area = $('#november_eastern_area').val()
                var december_eastern_area = $('#december_eastern_area').val()

                var january_southern_area = $('#january_southern_area').val()
                var february_southern_area = $('#february_southern_area').val()
                var march_southern_area = $('#march_southern_area').val()
                var april_southern_area = $('#april_southern_area').val()
                var may_southern_area = $('#may_southern_area').val()
                var june_southern_area = $('#june_southern_area').val()
                var july_southern_area = $('#july_southern_area').val()
                var august_southern_area = $('#august_southern_area').val()
                var september_southern_area = $('#september_southern_area').val()
                var october_southern_area = $('#october_southern_area').val()
                var november_southern_area = $('#november_southern_area').val()
                var december_southern_area = $('#december_southern_area').val()

                var reques_progress = '<?php echo $reques_progress; ?>';
                var reques_sueecss = '<?php echo $reques_sueecss; ?>';

                var upper_north_area = '<?php echo count($arrey_upper_north_area); ?>';
                var lower_north_area = '<?php echo count($arrey_lower_north_area); ?>';
                var central_area = '<?php echo count($arrey_central_area); ?>';
                var upper_northeast_area = '<?php echo count($arrey_upper_northeast_area); ?>';
                var lower_northeast_area = '<?php echo count($arrey_lower_northeast_area); ?>';
                var eastern_area = '<?php echo count($arrey_eastern_area); ?>';
                var southern_area = '<?php echo count($arrey_southern_area); ?>';

                $(document).ready(function () {
                    $('#year').on('change', function () {
                        var $form = $(this).closest('form');
                        $form.find('input[type=submit]').click();
                    });
                });

                var myChart = new Chart(ctx, {
                    type: 'pie',
                    data: {
                        labels: ['ดำเนินการแล้ว', 'อยู่ระหว่างดำเนินการ'],
                        datasets: [{
                            label: 'จำนวนการขอฝนหลวง',
                            data: [reques_sueecss, reques_progress],
                            backgroundColor: ['#4cd137', '#fbc531'],
                            borderColor: '#fff',
                            fill: false
                        }]
                    },
                    options: {
                        responsive: true
                    }
                });

                var myChartProvince = new Chart(province, {
                    type: 'pie',
                    data: {
                        labels: ['ศปน(บ).', 'ศปน(ล).', 'ศปก', 'ศปฉ(บ).', 'ศปฉ(ล).', 'ศปอ', 'ศปต'],
                        datasets: [{
                            label: 'แบ่งตามศูนย์ปฏิบัติการ',
                            data: [upper_north_area, lower_north_area, central_area,
                                upper_northeast_area,
                                lower_northeast_area, eastern_area, southern_area
                            ],
                            backgroundColor: ['#ff0d0d', '#f59723', '#f3ed2b', '#00FF7F', '#41e1d2',
                                '#6495ED',
                                '#8A2BE2'
                            ],

                            borderColor: ['#fff'],
                            fill: false
                        }]
                    },
                    options: {
                        responsive: true
                    }
                });

                var myChartMonth = new Chart(chart_month, {
                    type: 'bar',
                    data: {
                        labels: ["ต.ค.(" + october + ")", "พ.ย.(" + november + ")", "ธ.ค.(" + december + ")",
                            "ม.ค. (" +
                            january + ")", "ก.พ.(" + february + ")", "มี.ค.(" + march + ")", "เม.ย.(" +
                            april + ")", "พ.ค.(" + may + ")", "มิ.ย.(" + june + ")", "ก.ค.(" + july + ")",
                            "สิ.ค.(" + august + ")", "ก.ย.(" + september + ")"
                        ],
                        datasets: [{
                                label: "ศปน(บ).",
                                backgroundColor: '#ff0d0db8',
                                hoverBackgroundColor: '#ff0d0d',
                                borderColor: 'rgba(255,99,132,1)',
                                data: [october_upper_north_area,
                                    november_upper_north_area,
                                    december_upper_north_area,
                                    january_upper_north_area,
                                    february_upper_north_area,
                                    march_upper_north_area,
                                    april_upper_north_area,
                                    may_upper_north_area,
                                    june_upper_north_area,
                                    july_upper_north_area,
                                    august_upper_north_area,
                                    september_upper_north_area

                                ]
                            },
                            {
                                label: "ศปน(ล).",
                                backgroundColor: '#f59723b8',
                                hoverBackgroundColor: '#f59723',
                                borderColor: 'rgba(54, 162, 235, 1)',
                                data: [october_lower_north_area,
                                    november_lower_north_area,
                                    december_lower_north_area,
                                    january_lower_north_area,
                                    february_lower_north_area,
                                    march_lower_north_area,
                                    april_lower_north_area,
                                    june_lower_north_area,
                                    february_eastern_area,
                                    july_lower_north_area,
                                    august_lower_north_area,
                                    september_lower_north_area

                                ]
                            },
                            {
                                label: "ศปก",
                                backgroundColor: '#f3ed2bb8',
                                hoverBackgroundColor: '#f3ed2b',
                                borderColor: 'rgba(255, 206, 86, 1)',
                                data: [october_central_area,
                                    november_central_area,
                                    december_lower_north_area,
                                    january_central_area,
                                    february_central_area,
                                    march_central_area,
                                    april_central_area,
                                    may_central_area,
                                    june_central_area,
                                    july_central_area,
                                    august_central_area,
                                    september_central_area
                                ]
                            },
                            {
                                label: "ศปฉ(บ).",
                                backgroundColor: '#00FF7Fb8',
                                hoverBackgroundColor: '#00FF7F',
                                borderColor: 'rgba(75, 192, 192, 1)',
                                data: [october_upper_northeast_area,
                                    november_upper_northeast_area,
                                    december_upper_northeast_area,
                                    january_upper_northeast_area,
                                    february_upper_northeast_area,
                                    march_upper_northeast_area,
                                    april_upper_northeast_area,
                                    may_upper_northeast_area,
                                    june_upper_northeast_area,
                                    july_upper_northeast_area,
                                    august_upper_northeast_area,
                                    september_upper_northeast_area
                                ]
                            },
                            {
                                label: "ศปฉ(ล).",
                                backgroundColor: '#41e1d2b8',
                                hoverBackgroundColor: '#41e1d2',
                                borderColor: 'rgba(153, 102, 255, 1)',
                                data: [october_lower_northeast_area,
                                    november_lower_northeast_area,
                                    december_lower_northeast_area,
                                    january_lower_northeast_area,
                                    february_lower_northeast_area,
                                    march_lower_northeast_area,
                                    april_lower_northeast_area,
                                    may_lower_northeast_area,
                                    june_lower_northeast_area,
                                    july_lower_northeast_area,
                                    august_lower_northeast_area,
                                    september_lower_northeast_area
                                ]
                            },
                            {
                                label: "ศปอ",
                                backgroundColor: '#317afdb8',
                                hoverBackgroundColor: '#317afd',
                                borderColor: 'rgba(255, 159, 64, 1)',
                                data: [october_eastern_area,
                                    november_eastern_area,
                                    december_eastern_area,
                                    january_eastern_area,
                                    february_eastern_area,
                                    march_eastern_area,
                                    april_eastern_area,
                                    may_eastern_area,
                                    june_eastern_area,
                                    july_eastern_area,
                                    august_eastern_area,
                                    september_eastern_area
                                ]
                            },
                            {
                                label: "ศปต",
                                backgroundColor: '#8A2BE2b8',
                                hoverBackgroundColor: '#8A2BE2',
                                borderColor: '#ff00e0',
                                data: [october_southern_area,
                                    november_southern_area,
                                    december_southern_area,
                                    january_southern_area,
                                    february_southern_area,
                                    march_southern_area,
                                    april_southern_area,
                                    may_southern_area,
                                    june_southern_area,
                                    july_southern_area,
                                    august_southern_area,
                                    september_southern_area
                                ]
                            }
                        ]

                    },
                    options: {
                        scales: {
                            xAxes: [{
                                stacked: true
                            }],
                            yAxes: [{
                                stacked: true
                            }]
                        }
                    }
                });
            </script>
    </body>

    </html>