<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>การขอรับฝนหลวง ประจำวันที่ <?php echo date('Y-m-d'); ?></title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous">
    </script>
    <style>
        .upper_north {
            background-color: #ff0d0d;
            padding: 0px 6px 0px 15px;
            margin-right: 10px;
            border: 2px solid #000;
        }

        .lower_north {
            background-color: #f59723;
            padding: 0px 6px 0px 15px;
            margin-right: 10px;
            border: 2px solid #000;
        }

        .central {
            background-color: #d8d220;
            padding: 0px 6px 0px 15px;
            margin-right: 10px;
            border: 2px solid #000;
        }

        .upper_northeast {
            background-color: #00FF7F;
            padding: 0px 6px 0px 15px;
            margin-right: 10px;
            border: 2px solid #000;
        }

        .lower_northeast {
            background-color: #41e1d2;
            padding: 0px 6px 0px 15px;
            margin-right: 10px;
            border: 2px solid #000;
        }

        .eastern {
            background-color: #6495ED;
            padding: 0px 6px 0px 15px;
            margin-right: 10px;
            border: 2px solid #000;
        }

        .southern {
            background-color: #8A2BE2;
            padding: 0px 6px 0px 15px;
            margin-right: 10px;
            border: 2px solid #000;
        }
    </style>
</head>

<body>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous">
    </script>

    <?php
    // echo date('Y-m-d');
    // $to_day = date('Y-m-d');
    // echo "<hr>";
    $curlSession = curl_init();
    curl_setopt($curlSession, CURLOPT_URL, 'https://opendata.royalrain.go.th/request/all');
    curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);

    $jsonData = json_decode(curl_exec($curlSession));
    curl_close($curlSession);
    $all_reques_today = 0;

     // set array area
     $arrey_upper_north_area_yester_day = array();
     $arrey_lower_north_area_yester_day = array();
     $arrey_central_area_yester_day = array();
     $arrey_upper_northeast_area_yester_day = array();
     $arrey_lower_northeast_area_yester_day = array();
     $arrey_eastern_area_yester_day = array();
     $arrey_southern_area_yester_day = array();

    // set array area yester day
    $arrey_upper_north_area = array();
    $arrey_lower_north_area = array();
    $arrey_central_area = array();
    $arrey_upper_northeast_area = array();
    $arrey_lower_northeast_area = array();
    $arrey_eastern_area = array();
    $arrey_southern_area = array();


    // echo '<pre>';
    foreach ($jsonData->data as $key => $value) {
        $day = date('Y-m-d', strtotime($value->request_date));
        // echo $day ." == " . date('Y-m-d') . "<hr>";
        if ($day == date('Y-m-d')) {
            $all_reques_today++;
            // print_r($value);
            if ($value->province_title == 'จ.เชียงราย' || $value->province_title == 'จ.เชียงใหม่' 
            || $value->province_title == 'จ.ตาก'|| $value->province_title == 'จ.แม่ฮ่องสอน'
            || $value->province_title == 'จ.พะเยา' || $value->province_title == 'จ.ลำปาง'
            || $value->province_title == 'จ.ลำพูน') {

                $arrey_upper_north_area[] = $value; 
            }

            if ($value->province_title == 'จ.กำแพงเพชร' || $value->province_title == 'จ.พิจิตร' 
            || $value->province_title == 'จ.สุโขทัย'|| $value->province_title == 'จ.อุตรดิตถ์'
            || $value->province_title == 'จ.พิษณุโลก' || $value->province_title == 'จ.น่าน'
            || $value->province_title == 'จ.แพร่' || $value->province_title == 'จ.เพชรบูรณ์') {

                $arrey_lower_north_area[] = $value;
            }

            if ($value->province_title == 'กรุงเทพมหานคร' || $value->province_title == 'จ.กาญจนบุรี' 
            || $value->province_title == 'จ.ชัยนาท'|| $value->province_title == 'จ.นครปฐม'
            || $value->province_title == 'จ.ลพบุรี' || $value->province_title == 'จ.นครสวรรค์'
            || $value->province_title == 'จ.สุพรรณบุรี' || $value->province_title == 'จ.นนทบุรี'
            || $value->province_title == 'จ.ปทุมธานี' || $value->province_title == 'จ.สระบุรี'
            || $value->province_title == 'จ.สิงห์บุรี' || $value->province_title == 'จ.พระนครศรีอยุธยา'
            || $value->province_title == 'จ.อ่างทอง' || $value->province_title == 'จ.อุทัยธานี') {

                $arrey_central_area[] = $value;
            }

            if ($value->province_title == 'จ.กาฬสินธุ์' || $value->province_title == 'จ.ขอนแก่น' 
            || $value->province_title == 'จ.นครพนม'|| $value->province_title == 'จ.บึงกาฬ'
            || $value->province_title == 'จ.มุกดาหาร' || $value->province_title == 'จ.เลย'
            || $value->province_title == 'จ.สกลนคร' || $value->province_title == 'จ.หนองคาย'
            || $value->province_title == 'จ.หนองบัวลำภู' || $value->province_title == 'จ.อุดรธานี') {

                $arrey_upper_northeast_area[] = $value;
            }

            if ($value->province_title == 'จ.ชัยภูมิ' || $value->province_title == 'จ.นครราชสีมา' 
            || $value->province_title == 'จ.บุรีรัมย์'|| $value->province_title == 'จ.มหาสารคาม'
            || $value->province_title == 'จ.ยโสธร' || $value->province_title == 'จ.ร้อยเอ็ด'
            || $value->province_title == 'จ.ศรีสะเกษ' || $value->province_title == 'จ.สุรินทร์'
            || $value->province_title == 'จ.อุบลราชธานี' || $value->province_title == 'จ.อำนาจเจริญ') {

                $arrey_lower_northeast_area[] = $value;
            }

            if ($value->province_title == 'จ.จันทบุรี' || $value->province_title == 'จ.ฉะเชิงเทรา' 
            || $value->province_title == 'จ.ชลบุรี'|| $value->province_title == 'จ.ตราด'
            || $value->province_title == 'จ.นครนายก' || $value->province_title == 'จ.ปราจีนบุรี'
            || $value->province_title == 'จ.ระยอง' || $value->province_title == 'จ.สระแก้ว') {

                $arrey_eastern_area[] = $value;
            }

            if ($value->province_title == 'จ.กระบี่' || $value->province_title == 'จ.ชุมพร' 
            || $value->province_title == 'จ.สุราษฎร์ธานี'|| $value->province_title == 'จ.นครศรีธรรมราช'
            || $value->province_title == 'จ.นราธิวาส'|| $value->province_title == 'จ.ประจวบคีรีขันธ์'
            || $value->province_title == 'จ.ปัตตานี'|| $value->province_title == 'จ.พังงา'
            || $value->province_title == 'จ.พัทลุง'|| $value->province_title == 'จ.สมุทรสงคราม'
            || $value->province_title == 'จ.เพชรบุรี'|| $value->province_title == 'จ.ภูเก็ต'
            || $value->province_title == 'จ.สมุทรสาคร'|| $value->province_title == 'จ.สมุทรปราการ'
            || $value->province_title == 'จ.ระนอง'|| $value->province_title == 'จ.สตูล'
            || $value->province_title == 'จ.ราชบุรี' || $value->province_title == 'จ.สงขลา'
            || $value->province_title == 'จ.ตรัง' || $value->province_title == 'จ.ยะลา') {

                $arrey_southern_area[] = $value;
            }
        }

        // yester_day
        if ($day == date('Y-m-d',strtotime("-1 days"))) {
            if ($value->province_title == 'จ.เชียงราย' || $value->province_title == 'จ.เชียงใหม่' 
            || $value->province_title == 'จ.ตาก'|| $value->province_title == 'จ.แม่ฮ่องสอน'
            || $value->province_title == 'จ.พะเยา' || $value->province_title == 'จ.ลำปาง'
            || $value->province_title == 'จ.ลำพูน') {

                $arrey_upper_north_area_yester_day[] = $value; 
            }

            if ($value->province_title == 'จ.กำแพงเพชร' || $value->province_title == 'จ.พิจิตร' 
            || $value->province_title == 'จ.สุโขทัย'|| $value->province_title == 'จ.อุตรดิตถ์'
            || $value->province_title == 'จ.พิษณุโลก' || $value->province_title == 'จ.น่าน'
            || $value->province_title == 'จ.แพร่' || $value->province_title == 'จ.เพชรบูรณ์') {

                $arrey_lower_north_area_yester_day[] = $value;
            }

            if ($value->province_title == 'กรุงเทพมหานคร' || $value->province_title == 'จ.กาญจนบุรี' 
            || $value->province_title == 'จ.ชัยนาท'|| $value->province_title == 'จ.นครปฐม'
            || $value->province_title == 'จ.ลพบุรี' || $value->province_title == 'จ.นครสวรรค์'
            || $value->province_title == 'จ.สุพรรณบุรี' || $value->province_title == 'จ.นนทบุรี'
            || $value->province_title == 'จ.ปทุมธานี' || $value->province_title == 'จ.สระบุรี'
            || $value->province_title == 'จ.สิงห์บุรี' || $value->province_title == 'จ.พระนครศรีอยุธยา'
            || $value->province_title == 'จ.อ่างทอง' || $value->province_title == 'จ.อุทัยธานี') {

                $arrey_central_area_yester_day[] = $value;
            }

            if ($value->province_title == 'จ.กาฬสินธุ์' || $value->province_title == 'จ.ขอนแก่น' 
            || $value->province_title == 'จ.นครพนม'|| $value->province_title == 'จ.บึงกาฬ'
            || $value->province_title == 'จ.มุกดาหาร' || $value->province_title == 'จ.เลย'
            || $value->province_title == 'จ.สกลนคร' || $value->province_title == 'จ.หนองคาย'
            || $value->province_title == 'จ.หนองบัวลำภู' || $value->province_title == 'จ.อุดรธานี') {

                $arrey_upper_northeast_area_yester_day[] = $value;
            }

            if ($value->province_title == 'จ.ชัยภูมิ' || $value->province_title == 'จ.นครราชสีมา' 
            || $value->province_title == 'จ.บุรีรัมย์'|| $value->province_title == 'จ.มหาสารคาม'
            || $value->province_title == 'จ.ยโสธร' || $value->province_title == 'จ.ร้อยเอ็ด'
            || $value->province_title == 'จ.ศรีสะเกษ' || $value->province_title == 'จ.สุรินทร์'
            || $value->province_title == 'จ.อุบลราชธานี' || $value->province_title == 'จ.อำนาจเจริญ') {

                $arrey_lower_northeast_area_yester_day[] = $value;
            }

            if ($value->province_title == 'จ.จันทบุรี' || $value->province_title == 'จ.ฉะเชิงเทรา' 
            || $value->province_title == 'จ.ชลบุรี'|| $value->province_title == 'จ.ตราด'
            || $value->province_title == 'จ.นครนายก' || $value->province_title == 'จ.ปราจีนบุรี'
            || $value->province_title == 'จ.ระยอง' || $value->province_title == 'จ.สระแก้ว') {

                $arrey_eastern_area_yester_day[] = $value;
            }

            if ($value->province_title == 'จ.กระบี่' || $value->province_title == 'จ.ชุมพร' 
            || $value->province_title == 'จ.สุราษฎร์ธานี'|| $value->province_title == 'จ.นครศรีธรรมราช'
            || $value->province_title == 'จ.นราธิวาส'|| $value->province_title == 'จ.ประจวบคีรีขันธ์'
            || $value->province_title == 'จ.ปัตตานี'|| $value->province_title == 'จ.พังงา'
            || $value->province_title == 'จ.พัทลุง'|| $value->province_title == 'จ.สมุทรสงคราม'
            || $value->province_title == 'จ.เพชรบุรี'|| $value->province_title == 'จ.ภูเก็ต'
            || $value->province_title == 'จ.สมุทรสาคร'|| $value->province_title == 'จ.สมุทรปราการ'
            || $value->province_title == 'จ.ระนอง'|| $value->province_title == 'จ.สตูล'
            || $value->province_title == 'จ.ราชบุรี' || $value->province_title == 'จ.สงขลา'
            || $value->province_title == 'จ.ตรัง' || $value->province_title == 'จ.ยะลา') {

                $arrey_southern_area_yester_day[] = $value;
            }
        }
    }
    // echo '<pre>';
    // echo '<hr>';
    // echo $all_reques_today;
    // print_r($arrey_upper_north_area);
    // print_r($arrey_lower_north_area);
    // print_r($arrey_central_area);
    // print_r($arrey_upper_northeast_area);
    // print_r($arrey_lower_northeast_area);
    // print_r($arrey_eastern_area);
    // print_r($arrey_southern_area);


    function DateThaiNoTime($strDate)
    {
        $strYear = date("Y",strtotime($strDate))+543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];
        return "$strDay $strMonthThai $strYear ";
    }

    function DateThai($strDate)
    {
        $strYear = date("Y",strtotime($strDate))+543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];
        return "$strDay $strMonthThai $strYear  เวลา  $strHour:$strMinute น.";
    }

    function provinceNumber($province_arrey = array()){
        foreach ($province_arrey as $key => $value) {
                $province[] = $value->province_title;
        }
        if (isset($province)) {
            $count_province = array_unique($province);
        }
        if (isset($count_province)) {
            return count($count_province);
        }
    }

    function amphurNumber($amphur_arrey = array()){
        foreach ($amphur_arrey as $key => $value) {
                $amphur[] = $value->amphur_title;
        }
        if (isset($amphur)) {
            $count_amphur = array_unique($amphur);
        }
        if (isset($count_amphur)) {
            return count($count_amphur);
        }
    }
    $totle_province_number = (provinceNumber($arrey_upper_north_area) + provinceNumber($arrey_lower_north_area) + provinceNumber($arrey_central_area) + provinceNumber($arrey_upper_northeast_area) + provinceNumber($arrey_lower_northeast_area) + provinceNumber($arrey_eastern_area) + provinceNumber($arrey_southern_area));
    $totle_amphur_number = (amphurNumber($arrey_upper_north_area) + amphurNumber($arrey_lower_north_area) + amphurNumber($arrey_central_area) + amphurNumber($arrey_upper_northeast_area) + amphurNumber($arrey_lower_northeast_area) + amphurNumber($arrey_eastern_area) + amphurNumber($arrey_southern_area));
   ?>
    <div class="container">
        <div class="card card-default">
            <div class="card-header">
                <div class="text-center">
                    <h4><?php echo "การขอรับบริการฝนหลวง ประจ้าวันที่ " . DateThaiNoTime(date('Y-m-d')); ?></h4>
                    <h4><?php echo "จำนวนรวม " . $all_reques_today . " แห่ง ครอบคลุม ".$totle_province_number." จังหวัด ". $totle_amphur_number ." อำเภอ"?>
                    </h4>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12 col-md-8">
                        <?php
                        if ($all_reques_today == 0) {
                            echo '<div class="alert alert-danger mt-5">';
                            echo '<h4 class="text-center">ไม่มีการขอรับบริการฝนหลวงในวันนี้</h4>';
                            echo '<P class="text-center">ข้อมูลอัพเดตวันที่ ' .DateThai($jsonData->update_date) . '</P>';
                            echo '</div>';
                            echo '<canvas id="my-chart-province"></canvas>';
                        }else{
                            echo '<canvas id="my-chart-province"></canvas>';
                        }
                        ?>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="pt-5 chrs">
                            <p><span
                                    class="upper_north"></span><?php echo "ภาคเหนือตอนบน (" . count($arrey_upper_north_area). " แห่ง)" ?>
                            </p>
                            <p><span
                                    class="lower_north"></span><?php echo "ภาคเหนือตอนล่าง (" . count($arrey_lower_north_area). " แห่ง)" ?>
                            </p>
                            <p><span
                                    class="central"></span><?php echo "ภาคกลาง (" . count($arrey_central_area). "แห่ง)" ?>
                            </p>
                            <p><span
                                    class="upper_northeast"></span><?php echo "ภาคตะวันออกเฉียงเหนือตอนบน (" . count($arrey_upper_northeast_area). " แห่ง)" ?>
                            </p>
                            <p><span
                                    class="lower_northeast"></span><?php echo "ภาคตะวันออกเฉียงเหนือตอนล่าง (" . count($arrey_lower_northeast_area). " แห่ง)" ?>
                            </p>
                            <p><span
                                    class="eastern"></span><?php echo "ภาคตะวันออก (" . count($arrey_eastern_area). " แห่ง)" ?>
                            </p>
                            <p><span
                                    class="southern"></span><?php echo "ภาคใต้ (" . count($arrey_southern_area). " แห่ง)" ?>
                            </p>
                        </div>
                    </div>
                    <div class="col-12 col-md-12">
                        <!-- <p><?php //echo date('d-m-Y',strtotime("-1 days")); ?></p> -->
                        <div class="mt-5">
                            <h4 class="text-center mt-5">การขอรับบริการฝนหลวง</h4>
                            <canvas id="myChart"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" value="<?php echo count($arrey_upper_north_area) ?>" id="arrey_upper_north_area">
    <input type="hidden" value="<?php echo count($arrey_lower_north_area) ?>" id="arrey_lower_north_area">
    <input type="hidden" value="<?php echo count($arrey_central_area) ?>" id="arrey_central_area">
    <input type="hidden" value="<?php echo count($arrey_upper_northeast_area) ?>" id="arrey_upper_northeast_area">
    <input type="hidden" value="<?php echo count($arrey_lower_northeast_area) ?>" id="arrey_lower_northeast_area">
    <input type="hidden" value="<?php echo count($arrey_eastern_area) ?>" id="arrey_eastern_area">
    <input type="hidden" value="<?php echo count($arrey_southern_area) ?>" id="arrey_southern_area">

    <input type="hidden" value="<?php echo count($arrey_upper_north_area_yester_day) ?>"
        id="arrey_upper_north_area_yester_day">
    <input type="hidden" value="<?php echo count($arrey_lower_north_area_yester_day) ?>"
        id="arrey_lower_north_area_yester_day">
    <input type="hidden" value="<?php echo count($arrey_central_area_yester_day) ?>" id="arrey_central_area_yester_day">
    <input type="hidden" value="<?php echo count($arrey_upper_northeast_area_yester_day) ?>"
        id="arrey_upper_northeast_area_yester_day">
    <input type="hidden" value="<?php echo count($arrey_lower_northeast_area_yester_day) ?>"
        id="arrey_lower_northeast_area_yester_day">
    <input type="hidden" value="<?php echo count($arrey_eastern_area_yester_day) ?>" id="arrey_eastern_area_yester_day">
    <input type="hidden" value="<?php echo count($arrey_southern_area_yester_day) ?>"
        id="arrey_southern_area_yester_day">

    <input type="hidden" value="<?php echo DateThaiNoTime(date('Y-m-d')); ?>" id="to_day">
    <input type="hidden" value="<?php echo DateThaiNoTime(date('Y-m-d',strtotime("-1 days"))); ?>" id="yester_day">


    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@0.7.0"></script>
    <script>
        var arrey_upper_north_area = $('#arrey_upper_north_area').val()
        var arrey_lower_north_area = $('#arrey_lower_north_area').val()
        var arrey_central_area = $('#arrey_central_area').val()
        var arrey_upper_northeast_area = $('#arrey_upper_northeast_area').val()
        var arrey_lower_northeast_area = $('#arrey_lower_northeast_area').val()
        var arrey_eastern_area = $('#arrey_eastern_area').val()
        var arrey_southern_area = $('#arrey_southern_area').val()

        var arrey_upper_north_area_yester_day = $('#arrey_upper_north_area_yester_day').val()
        var arrey_lower_north_area_yester_day = $('#arrey_lower_north_area_yester_day').val()
        var arrey_central_area_yester_day = $('#arrey_central_area_yester_day').val()
        var arrey_upper_northeast_area_yester_day = $('#arrey_upper_northeast_area_yester_day').val()
        var arrey_lower_northeast_area_yester_day = $('#arrey_lower_northeast_area_yester_day').val()
        var arrey_eastern_area_yester_day = $('#arrey_eastern_area_yester_day').val()
        var arrey_southern_area_yester_day = $('#arrey_southern_area_yester_day').val()

        var to_day = $('#to_day').val()
        var yester_day = $('#yester_day').val()

        // console.log(arrey_southern_area);

        var data = [{
            data: [parseInt(arrey_upper_north_area), parseInt(arrey_lower_north_area), parseInt(
                    arrey_central_area),
                parseInt(arrey_upper_northeast_area),
                parseInt(arrey_lower_northeast_area), parseInt(arrey_eastern_area), parseInt(
                    arrey_southern_area)
            ],
            labels: ['ศปน(บ).', 'ศปน(ล).', 'ศปก', 'ศปฉ(บ).', 'ศปฉ(ล).', 'ศปอ', 'ศปต'],
            backgroundColor: ['#ff0d0d', '#f59723', '#e4de21', '#00FF7F', '#41e1d2',
                '#6495ED',
                '#8A2BE2'
            ],
            borderColor: "#fff"
        }];
        var options = {
            tooltips: {
                enabled: false
            },
            plugins: {
                datalabels: {
                    formatter: (value, ctx) => {
                        let sum = 0;
                        let dataArr = ctx.chart.data.datasets[0].data;
                        dataArr.map(data => {
                            sum += data;
                        });
                        // console.log(value)
                        let percentage = (value * 100 / sum).toFixed(2) + "%";
                        if (percentage == '0.00%') {
                            return ""
                        } else {
                            return percentage;
                        }
                    },
                    color: '#fff'
                }
            },

        };
        var ctx = document.getElementById('my-chart-province').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'pie',
            data: {
                datasets: data
            },
            options: options
        });

        var ctxx = document.getElementById("myChart").getContext("2d");

        var data = {
            labels: ['ศปน(บ).', 'ศปน(ล).', 'ศปก', 'ศปฉ(บ).', 'ศปฉ(ล).', 'ศปอ', 'ศปต'],
            datasets: [{
                    // เมือวาน
                    label: yester_day,
                    backgroundColor: "#18dcff",
                    data: [parseInt(arrey_upper_north_area_yester_day), parseInt(
                            arrey_lower_north_area_yester_day), parseInt(
                            arrey_central_area_yester_day),
                        parseInt(arrey_upper_northeast_area_yester_day),
                        parseInt(arrey_lower_northeast_area_yester_day), parseInt(
                            arrey_eastern_area_yester_day), parseInt(
                            arrey_southern_area_yester_day)
                    ]
                },

                {
                    // วันปัจจุบัน
                    label: to_day,
                    backgroundColor: "#ffaf40",
                    data: [parseInt(arrey_upper_north_area), parseInt(arrey_lower_north_area), parseInt(
                            arrey_central_area),
                        parseInt(arrey_upper_northeast_area),
                        parseInt(arrey_lower_northeast_area), parseInt(arrey_eastern_area), parseInt(
                            arrey_southern_area)
                    ],

                },

            ]
        };

        var myoption = {
            tooltips: {
                enabled: true
            },
            hover: {
                animationDuration: 1
            },
            animation: {
                duration: 1,
                onComplete: function () {
                    var chartInstance = this.chart,
                        ctx = chartInstance.ctx;
                    ctx.textAlign = 'center';
                    ctx.fillStyle = "rgba(0, 0, 0, 1)";
                    ctx.textBaseline = 'bottom';

                    this.data.datasets.forEach(function (dataset, i) {
                        var meta = chartInstance.controller.getDatasetMeta(i);
                        meta.data.forEach(function (bar, index) {
                            var data = dataset.data[index];
                            ctx.fillText(data, bar._model.x, bar._model.y - 5);
                        });
                    });
                }
            },
            plugins: {
                datalabels: {
                    formatter: (value, ctx) => {
                        let sum = 0;
                        let dataArr = ctx.chart.data.datasets[0].data;
                        dataArr.map(data => {
                            sum += data;
                        });
                        // console.log(value)
                        let percentage = (value * 100 / sum).toFixed(2) + "%";
                        if (percentage == '0.00%') {
                            return ""
                        } else {
                            return "";
                        }
                    },
                    color: '#fff'
                }
            },
        };

        var myBarChart = new Chart(ctxx, {
            type: 'bar',
            data: data,
            options: myoption
        });
    </script>
</body>

</html>